This repository is for the Complex Game Systems assignment.

Welcome to Target Blast: An online simple break the targets game for up to 4 players I made. The objective of the game is to hit as many targets as you can by blasting them and get the highest score possible!

OPENING THE GAME:

To run a server, open [root folder]/bin/ServerApplication.exe.

To join a game as a player, open [root folder]/bin/ClientApplication.exe.

PLAYING THE GAME:

The goal of the game is to blast as many targets in 3 rounds of 1 minute 30 seconds each as you possibly can. You blast a target by controlling the crosshair of your colour around the game world and destroying the targets. There will be 8 targets at any one time to hit and they will keep randomly appearing. There are 3 different coloured targets:

BLUE - Worth 10 points. Will stay around for a while.

GREEN - Worth 20 points. Won't be around for too long.

RED - Worth 40 points. Better be quick!

These targets will need to be hit with a bullet to score the relevant points.

Controls:

W/^ - Move crosshair up

S/v - Move crosshair down

A/< - Move crosshair left

D/> - Move crosshair right

SPACE BAR - Blast target.

Notes:

You will see where the crosshairs of the other players throughout the gamem, so you can keep a close eye  on them...

When a target is blasted, the points scored for blasting that target will appear. The colour of the text that appears corresponds to the player that scored those points.

Thanks for checking it out and I hope you have fun.