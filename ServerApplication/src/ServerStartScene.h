/*
file:				ServerStartScene.h
author:				Brad Cook.
description:		This file contains the declaration of the ServerStartScene
					class: The scene that is shown at the start of the game.
date last modified:	11/01/2017
*/

#pragma once

#include "IScene.h"
#include <TimeManager.h>

#include <vector>
#include <RakNetTypes.h>

namespace RakNet {
	class RakPeerInterface;
}

/// <summary>
/// The scene that is used at the start. When the game is first started, this
/// is the scene that is set on the server. This scene is used as the waiting
/// area for players to join the game. It collates all the information on the
/// players that have joined. It also waits until a number of players have
/// arrived.
/// </summary>
class ServerStartScene : public IScene
{
public:
	/// <summary>
	/// Default Constructor. Returns a new ServerStartScene instance. To set up
	/// the scene, call the init() function.
	/// <returns>
	/// returns ServerStartScene instance.
	/// </returns>
	/// </summary>
	ServerStartScene() {}

	/// <summary>
	/// Sets up all the elements required by the scene. This function is called
	/// when the game is first loaded.
	/// <param name="a_p_PeerInter">
	/// @param a_p_PeerInter: The RakNet::RakPeerInterface object in use for
	/// the server.
	/// </param>
	/// </summary>
	bool init(RakNet::RakPeerInterface* a_p_PeerInter);

	/// <summary>
	/// Deletes and frees up up all the elements required by the scene. This
	/// function is called when the game is last closed.
	/// </summary>
	void destroy();

	/// <summary>
	/// Updates all the relevant information (elements) in the
	/// scene per frame.
	/// <param name="dt">
	/// @param dt: Delta time, the time (in seconds) between frames executing.
	/// </param>
	/// <returns>
	/// Returns the name of the scene to change to.
	/// </returns>
	/// </summary>
	std::string update(double dt);

	/// <summary>
	/// Sets up all the elements in the scene. This function is called everytime
	/// the scene is set as the active scene (i.e. When
	/// Application.setActiveScene is called). It is not called when the game is
	/// initially loaded.
	/// </summary>
	void activate();

	/// <summary>
	/// Cleans up all the elements in the scene. This function is called
	/// everytime the active scene is changed (i.e. When
	/// Application.setActiveScene is called). It is not called when the game is
	/// exited.
	/// </summary>
	void deactivate();

	/// <summary>
	/// Called when a player has disconnected from the server. All logic
	/// relating to handling a player disconnecting is done here.
	/// <param name="a_SystemAddr">
	/// @param a_SystemAddr: The system address of the player that has
	/// disconnected.
	/// </param>
	/// </summary>
	virtual void handlePlayerDisconnected(
		const RakNet::SystemAddress& a_SystemAddr);

private:

	/*-------------------------------------------------------------------------
							PRIVATE MEMBER VARIABLES
	-------------------------------------------------------------------------*/

	/// <summary>
	/// Determines whether the game can start or not.
	/// </summary>
	bool								m_bCanStartGame;

	/// <summary>
	/// Determines whether to actually start the game or not.
	/// </summary>
	bool								m_bStartGame;

	/// <summary>
	/// Collection of system addresses that were present when the countdown
	/// started.
	/// </summary>
	std::vector<RakNet::SystemAddress>	m_vConnectedOnCountdown;

	/// <summary>
	/// The <see cref="TimeManager"/> to manage <see cref="Timers"/>  used for
	/// time-based events.
	/// </summary>
	target::TimeManager*				m_p_TimeManager;

	/// <summary>
	/// Reference to the RakPeerInterface object;
	/// </summary>
	RakNet::RakPeerInterface*			m_p_PeerInter;

	/*-------------------------------------------------------------------------
								PRIVATE METHODS
	-------------------------------------------------------------------------*/

	/// <summary>
	/// Checks whether there are enough players to start the game.
	/// </summary>
	void checkEnoughPlayers();

	/// <summary>
	/// Check if additional players have connected if the game countdown has
	/// been started.
	/// </summary>
	void otherPlayersConnected();

	/// <summary>
	/// Records the RakNet system addresses of the players who will be playing
	/// the game.
	/// </summary>
	void recordPlayers();

	/// <summary>
	/// Starts the gameplay.
	/// </summary>
	void startGameplay();

	/// <summary>
	/// Starts the countdown to the game starting.
	/// </summary>
	void startCountdown();

	/// <summary>
	/// Stop the countdown to the game starting.
	/// </summary>
	void stopCountdown();

	/// <summary>
	/// Sends the ID_CLIENT_START_COUNTDOWN message, which informs all clients
	/// to start the countdown to the game beginning. 
	/// </summary>
	void sendStartCountdown();

	/// <summary>
	/// Sends the ID_CLIENT_START_COUNTDOWN_ONE message, which informs one
	/// particular client to start the countdown to the game beginning.
	/// <param name="a_AddressToSend">
	/// @param a_AddressToSend: The RakNet system address to send the message
	/// to.
	/// </param>
	/// </summary>
	void sendStartCountdownOne(const RakNet::SystemAddress& a_AddressToSend);

	/// <summary>
	/// Sends the ID_CLIENT_STOP_COUNTDOWN message, which informs all clients
	/// to stop the countdown to the game beginning. 
	/// </summary>
	void sendStopCountdown();
};
