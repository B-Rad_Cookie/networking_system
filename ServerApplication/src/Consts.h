/*
file:				Consts.h
author:				Brad Cook.
description:		This file contains the declaration of the Consts class:
					A utility class containing constant global variables used
					throughout the applications.
date last modified:	13/01/2017
*/

#pragma once

#include "Enums.h"
#include <map>

class Consts
{
public:

	/// <summary>
	/// Defines the radius of the target circle size for each of the target types.
	/// </summary>
	static std::map<TargetType, float>	TYPE_TO_SIZE;

	/// <summary>
	/// The maximum number of targets in a game.
	/// </summary>
	static const unsigned int			uiMAX_NUM_OF_TARGETS = 8;

	/// <summary>
	/// The number of rounds to play.
	/// </summary>
	static const unsigned int			uiNUM_OF_ROUNDS = 3;

	/// <summary>
	/// The time (in seconds) that each round lasts for.
	/// </summary>
	static const float					fGAMEPLAY_ROUND_TIME;
	
	/// <summary>
	/// The movement speed of the crosshairs
	/// </summary>
	static const float					fMOVEMENT_SPEED;
};
