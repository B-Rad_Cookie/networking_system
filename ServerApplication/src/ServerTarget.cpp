/*
file:				ServerTarget.cpp
author:				Brad Cook.
description:		This file contains the definition of the ServerTarget
					struct: The representation of the targets to destroy in the
					game on the server side.
date last modified:	15/01/2017
*/

#include "ServerTarget.h"
#include <Circle.h>

ServerTarget::ServerTarget() : NetworkObject()
{
	m_eType = TargetType::Blue;
	m_p_Circle = nullptr;
	m_fDuration = 0.0f;
}

ServerTarget::ServerTarget(unsigned int a_ID,
	Circle* a_p_Circle, TargetType a_eType)
	: NetworkObject(a_ID)
{
	m_eType = a_eType;
	m_p_Circle = a_p_Circle;

	// Set the duration based on the type
	switch (m_eType)
	{
	case TargetType::Blue:
	{
		m_fDuration = 20.0f;
		break;
	}
	case TargetType::Green:
	{
		m_fDuration = 10.0f;
		break;
	}
	case TargetType::Red:
	{
		m_fDuration = 5.0f;
		break;
	}
	default:  // Set to 0, similar to ServerTarget().
		m_fDuration = 0.0f;
		break;
	}
}

ServerTarget::~ServerTarget()
{
	if (m_p_Circle != nullptr)
	{
		delete m_p_Circle;
	}
}
