/*
file:				ServerStartScene.cpp
author:				Brad Cook.
description:		This file contains the definition of the ServerStartScene
					class: The scene that is shown at the start of the game.
date last modified:	11/01/2017
*/

#include "ServerStartScene.h"
#include "GameInfo.h"

#include <iostream>
#include <functional>
#include <algorithm>

#include <RakPeerInterface.h>
#include <MessageIdentifiers.h>
#include <BitStream.h>

bool ServerStartScene::init(RakNet::RakPeerInterface* a_p_PeerInter)
{
	// Setup the time manager.
	m_p_TimeManager = new target::TimeManager();

	// Get the peer interface reference.
	m_p_PeerInter = a_p_PeerInter;

	return true;
}

void ServerStartScene::destroy()
{
	// Remove the time manager.
	delete m_p_TimeManager;

	m_p_PeerInter = nullptr;
}

std::string ServerStartScene::update(double dt)
{
	// Update TimeManager.
	m_p_TimeManager->update(dt);

	// If the game has started, switch to the gameplay scene.
	if (m_bStartGame == true)
	{
		return "GameplayScene";
	}

	// If countdown has started, see if more players have connected so they can
	// be informed.
	if (m_bCanStartGame == true)
	{
		otherPlayersConnected();
	}

	// Keep a track on how many players are connected to the game. Once there
	// are two or more players, begin the countdown.
	checkEnoughPlayers();

	return "";
}

void ServerStartScene::activate()
{
	std::cout << "Setting up the players for the game" << std::endl << std::endl;

	// Reset the timer.
	m_p_TimeManager->reset();

	// Set can start game and game started to false
	m_bCanStartGame = false;
	m_bStartGame = false;
}

void ServerStartScene::deactivate()
{
	// Clear all the timers.
	m_p_TimeManager->clearTimers();

	// Clear all the connected address at countdown.
	m_vConnectedOnCountdown.clear();

	// Record the players who will be playing the game.
	recordPlayers();
}

void ServerStartScene::checkEnoughPlayers()
{
	// Get the number of players registered.
	int iNumOfPlayers = GameInfo::getInstance()->m_vRegistered.size();

	// Check if the countdown can be started, and start it if it can.
	if (iNumOfPlayers >= 2 && m_bCanStartGame == false)
	{
		startCountdown();
	}
}

void ServerStartScene::otherPlayersConnected()
{
	// If the number of registered players is the same when the countdown was
	// originally started, return
	if (GameInfo::getInstance()->m_vRegistered.size() ==
		m_vConnectedOnCountdown.size())
	{
		return;
	}

	// If there is a new player, add their system address to the list.
	for (auto connect = (
		GameInfo::getInstance()->m_vConnectedPlayers.begin());
		connect != GameInfo::getInstance()->m_vConnectedPlayers.end();
		connect++)
	{
		std::vector<RakNet::SystemAddress>::iterator SysAddress = std::find(
			m_vConnectedOnCountdown.begin(),
			m_vConnectedOnCountdown.end(),
			(*connect).m_SysAddress);

		if (SysAddress == m_vConnectedOnCountdown.end()) // Sys address was not connected
		{
			// Add sys address to connected on countdown.
			m_vConnectedOnCountdown.push_back((*connect).m_SysAddress);
			// Send the message.
			sendStartCountdownOne((*connect).m_SysAddress);
			break;
		}
	}
}

void ServerStartScene::recordPlayers()
{
	GameInfo::recordPlayersInGame();
}

void ServerStartScene::startGameplay()
{
	// Set the bool trigger to true.
	std::cout << "Switching to gameplay" << std::endl << std::endl;
	m_bStartGame = true;
}

void ServerStartScene::startCountdown()
{
	std::cout << "Starting game in 20 seconds" << std::endl << std::endl;

	// Set can start to true.
	m_bCanStartGame = true;

	// start the local countdown.
	m_p_TimeManager->addTimer("Countdown",
		std::bind(&ServerStartScene::startGameplay, this),
		20.0f, false);

	// Send the start countdown message.
	sendStartCountdown();

	// Record the sys addresses currently connected.
	for (auto connect = (
			GameInfo::getInstance()->m_vConnectedPlayers.begin());
		connect != GameInfo::getInstance()->m_vConnectedPlayers.end();
		connect++)
	{
		m_vConnectedOnCountdown.push_back((*connect).m_SysAddress);
	}
	GameInfo::getInstance()->m_vConnectedPlayers;
}

void ServerStartScene::stopCountdown()
{
	std::cout << "Stopping countdown: not enough players" << std::endl << std::endl;

	// Send the stop countdown message.
	sendStopCountdown();

	// Set can start to false
	m_bCanStartGame = false;

	// Remove the timer.
	m_p_TimeManager->stopTimer("Countdown");

	// Clear the connected sys addresses.
	m_vConnectedOnCountdown.clear();
}

void ServerStartScene::sendStartCountdown()
{
	// Need to send how long the countdown has remaining, just in case someone
	// joins while the countdown has started.
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_CLIENT_START_COUNTDOWN));
	bs.Write(unsigned int(m_p_TimeManager->getTimeRemaining("Countdown")));
	m_p_PeerInter->Send(&bs, HIGH_PRIORITY, RELIABLE, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void ServerStartScene::sendStartCountdownOne(
	const RakNet::SystemAddress& a_AddressToSend)
{
	// Need to send how long the countdown has remaining.
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_CLIENT_START_COUNTDOWN_ONE));
	bs.Write(unsigned int(m_p_TimeManager->getTimeRemaining("Countdown")));
	m_p_PeerInter->Send(&bs, HIGH_PRIORITY, RELIABLE, 0,
		a_AddressToSend, false);
}

void ServerStartScene::sendStopCountdown()
{
	// Write and send the message to all clients.
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_CLIENT_STOP_COUNTDOWN));
	m_p_PeerInter->Send(&bs, HIGH_PRIORITY, RELIABLE, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void ServerStartScene::handlePlayerDisconnected(
	const RakNet::SystemAddress& a_SystemAddr)
{
	// If the countdown has started and the player was connected, remove his
	// connection from that list.
	if (m_bCanStartGame == true)
	{
		// Use the remove erase idiom to remove the player.
		m_vConnectedOnCountdown.erase(std::remove(
			m_vConnectedOnCountdown.begin(),
			m_vConnectedOnCountdown.end(),
			a_SystemAddr));
	}

	int iNumOfPlayers = GameInfo::getInstance()->m_vRegistered.size();

	// If there's not enough players and it's set to true, set it to false and
	// send the stop countdown message. Additionally, stop the local countdown.
	if (iNumOfPlayers <= 1 && m_bCanStartGame == true)
	{
		stopCountdown();
	}
}
