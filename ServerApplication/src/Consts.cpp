/*
file:				Consts.cpp
author:				Brad Cook.
description:		This file contains the definition of the Consts class:
					A utility class containing constant global variables used
					throughout the applications.
date last modified:	14/01/2017
*/

#include "Consts.h"

std::map<TargetType, float> Consts::TYPE_TO_SIZE = std::map<TargetType, float>
{
	{ TargetType::Red, 20.0f },
	{ TargetType::Blue, 30.0f },
	{ TargetType::Green, 25.0f },
};

const float Consts::fGAMEPLAY_ROUND_TIME = 90.0f;

const float Consts::fMOVEMENT_SPEED = 250.0f;
