/*
file:				Server.h
author:				Brad Cook.
description:		This file contains the declaration for the class that
					handles the server-side of the game.
date last modified:	11/01/2017
*/

#pragma once

#include "CustomGameMessages.h"

#include <map>
#include <vector>

#include <RakNetTypes.h>
#include <RakPeerInterface.h>

class IScene;

namespace RakNet {
	class RakPeerInterface;
}

/// <summary>
/// Stores information about a client connecting to the server. It also stoes
/// some information about the players.
/// </summary>
struct ConnectionInfo
{
	/// <summary>
	/// The unique identifier, and player number, value given to the client
	/// </summary>
	unsigned int			m_uiConnectionID;

	/// <summary>
	/// The IP address of the client.
	/// </summary>
	RakNet::SystemAddress	m_SysAddress;
};

/// <summary>
/// The server-side	application for the game. It handles the logic of the
/// server-side of the game system. It also handles the messages received from
/// clients and sends messages to the clients.
/// <remarks>
/// This class has been designed so that many of it's functions can be extended
/// or overloaded.
/// </remarks>
/// </summary>
class Server
{
public:

	/// <summary>
	/// Default Constructor.
	/// <returns>
	/// returns Server instance.
	/// </returns>
	/// </summary>
	Server();

	/// <summary>
	/// Destructor. Inheriting classes should implement this if there are 
	/// things to clean up.
	/// </summary>
	virtual ~Server();

	/// <summary>
	/// Setups up the application. This function is called before the update
	/// loop begins.
	/// </summary>
	virtual bool startup();

	/// <summary>
	/// Starts running the server. Call this function to have the
	/// server begin it's update loop. It will calculate delta
	/// time, then call the update() in a loop until the server is exited.
	/// </summary>
	virtual void run();

	/// <summary>
	/// Tears down the application. This function is called after the update
	/// and draw loops have finished.
	/// </summary>
	virtual void shutdown();

	/// <summary>
	/// Update function. This function is called once every frame.
	/// <param name="deltaTime">@param deltaTime: the time (in seconds) between
	/// frames.</param>
	/// </summary>
	virtual void update(double deltaTime);

	/// <summary>
	/// Quits the server.
	/// </summary>
	void quit();

	/// <summary>
	/// Adds a new <see cref="IScene"/> to the collection of scenes used in the
	/// application. It also initialises the scene by calling its "init()"
	/// function.
	/// <param name="a_p_cName">
	/// @param a_p_cName: The name of the scene.
	/// </param>
	/// <param name="a_p_Scene">
	/// @param a_p_Scene: The <see cref="IScene"/> instance.
	/// </param>
	/// </summary>
	void addScene(const char* a_p_cName, IScene* a_p_Scene);

	/// <summary>
	/// Removes all the <see cref="IScene"/> scene objects stored. It also
	/// calls there "destroy()" function.
	/// </summary>
	void clearScenes();

	/// <summary>
	/// Changes the sctive scene to be the scene 'a_p_cSceneName'.
	/// 'a_p_cSceneName' should correspond to the name of a scene that was
	/// added through <see cref="Application.addScene(const char*, IScene*)/>.
	/// <param name="a_p_cSceneName">
	/// @param a_p_cSceneName: The name of the scene to set as active.
	/// </param>
	/// <returns>
	/// Returns true if the scene has been changed, returns false otherwise. 
	/// </returns>
	/// </summary>
	bool setActiveScene(const char* a_p_cSceneName);

protected:

	/*-------------------------------------------------------------------------
						PROTECTED MEMBER VARIABLES
	-------------------------------------------------------------------------*/

	/// <summary>
	/// The port on which the server is going to run on.
	/// </summary>
	const unsigned short PORT = 5456;

	/// <summary>
	/// Reference to the RakPeerinterface instance that will be handling all
	/// the messaging in the system.
	/// </summary>
	RakNet::RakPeerInterface*		m_p_PeerInter;

	/*
	unsigned int					m_connectionCounter;

	bool												m_hasWorldChanged;
	std::unordered_map<unsigned int, ConnectionInfo>	m_connectedClients;
	std::vector<GameObject>								m_gameObjects;
	std::vector<unsigned int>							m_gameObjectsUpdated;
	unsigned int										m_objectCounter;
	SyncType
	m_syncType = SyncType::POSITION_ONLY;
	*/

	/// <summary>
	/// The <see cref="IScene"/> scenes used on the application.
	/// <remarks>
	/// The first element of the std::map is a std::string representing the
	/// name of the scene. The second element of the std::map is the
	/// <see cref="IScene"/> object. 
	/// </remarks>
	/// </summary>
	std::map<std::string, IScene*>	m_mScenes;

	/// <summary>
	/// The current active scene.
	/// </summary>
	IScene*							m_p_ActiveScene;

	/// <summary>
	/// The ids available value to assign to players. The max number of players
	/// is 4. Player IDs will be taken from this pool of values and put back
	/// into the pool of values. This is so that IDs can be put back when a
	/// player disconnects, and used when another connects afterwards.
	/// <remarks>
	/// The data structure will work like a stack.
	/// </remarks>
	/// </summary>
	std::vector<unsigned int>		m_vPlayerIDs;

	/// <summary>
	/// The last player ID value that was assigned.
	/// </summary>
	unsigned int					m_uiAssignedPlayerID;

	/// <summary>
	/// The number of frames per second.
	/// </summary>
	unsigned int					m_uiFps;

	/// <summary>
	/// Whether or not the server should continue to run. Set this to false to
	/// stop the game loop from running.
	/// </summary>
	bool							m_bIsServerRunning;

	/*-------------------------------------------------------------------------
								PROTECTED METHODS
	-------------------------------------------------------------------------*/

	/// <summary>
	/// Creates and sets up the scenes used in the application.
	/// </summary>
	virtual void setupScenes();

	//#########################################################################
	//						Network message handling

	/// <summary>
	/// Handles messages that are to be handled at the server level. This
	/// mainly includes the connection and disconnection of clients. As a
	/// client and connect and disconnect at any time, this is handled here
	/// instead of within the individual <see cref="IScene"/> scenes. Otherwise
	/// the connection handling would've had to been done in every individual
	/// scene.
	/// </summary>
	virtual void handleNetworkMessages();

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//				Network Messages handled at Server level.

	/// <summary>
	/// Handles the logic for a new client connecting to the server.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The data sent with a network message.
	/// </param>
	/// </summary>
	void handleNewConnection(RakNet::Packet* a_p_Packet);

	/// <summary>
	/// Adds a new connection to the server.
	/// <param name="a_SystemAddr">
	/// @param a_SystemAddr: The system address of the connecting client
	/// </param>
	/// </summary>
	void addNewConnection(RakNet::SystemAddress a_SystemAddr);

	/// <summary>
	/// Handles the ID_DISCONNECTION_NOTIFICATION message, which is a client
	/// disconnecting from the server.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The data sent with a network message.
	/// </param>
	/// </summary>
	void handleDisconnection(RakNet::Packet* a_p_Packet);

	/// <summary>
	/// Handles the ID_CONNECTION_LOST message, which is a client
	/// unexpectedy losing connection to the server.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The data sent with a network message.
	/// </param>
	/// </summary>
	void handleConnectionLost(RakNet::Packet* a_p_Packet);

	/// <summary>
	/// Removes a player from the server after they have disconnected
	/// <param name="a_SystemAddr">
	/// @param a_SystemAddr: The system address of the player that has
	/// disconnected.
	/// </param>
	/// </summary>
	void disconnectPlayer(const RakNet::SystemAddress& a_SystemAddr);

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_SERVER_REGISTER'. The message is sent from a client registering
	/// that they can be part of a game.
	/// <param name="a_SystemAddr">
	/// @param a_SystemAddr: The system address of the player to add as
	/// registered.
	/// </param>
	/// <remarks>
	/// The message is sent from StartScene::sendRegister(). It is handled at
	/// this level so that the message registers regardless of the scene the
	/// server is at.
	/// </remarks>
	/// </summary>
	void handleRegister(const RakNet::SystemAddress& a_SystemAddr);

	/// <summary>
	/// Handles the ID_SERVER_READY_TO_START message, which is a client
	/// telling the server they are ready to start gameplay.
	/// <param name="a_SystemAddr">
	/// @param a_SystemAddr: The system address of the player that has
	/// disconnected.
	/// </param>
	/// <remarks>
	/// While this message is used within <see cref="ServerGameplayScene"/>, it
	/// is handled at the Server level to ensure the message gets through. This
	/// is an important message that must be registered. If it was handled at
	/// the scene level, there is a chance for it to be missed based on timings
	/// (i.e. the client and server aren't guaranteed to be at their respective
	/// gameplay scene at the same time, as the switching between the start and
	/// gameplay scenes is handled at the local level).
	/// This message is sent by Gameplay::sendReadyToStart().
	/// </remarks>
	/// </summary>
	void handleReadyToStart(const RakNet::SystemAddress& a_SystemAddr);

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//				Network Messages sent at Server level.

	/// <summary>
	/// Sends the connection information registered by the server back to the
	/// client. It also tells the client whether they can join a game or need
	/// to wait.
	/// <remarks>
	/// This message is handled in
	/// BasicNetworkingApplication::handleClientConnectInfo().
	/// </remarks>
	/// <param name="a_SystemAddr">
	/// @param a_SystemAddr: The system address of the connecting client
	/// </param>
	/// <param name="a_uiID">
	/// @param a_uiID: The connection ID (and player number) of the client.
	/// </param>
	/// <param name="a_bCanJoinGame">
	/// @param a_bCanJoinGame: Whether or not the client can join a game
	/// session.
	/// </param>
	/// </summary>
	void sendConnectionInfoToClient(RakNet::SystemAddress a_SystemAddr,
		unsigned int a_uiID, bool a_bCanJoinGame);
};
