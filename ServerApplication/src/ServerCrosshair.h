/*
file:				ServerCrosshair.h
author:				Brad Cook.
description:		This file contains the declaration of the ServerCrosshair
					struct: The representation of the crosshairs to destroy in
					the game on the server side.
date last modified:	15/01/2017
*/

#pragma once

#include "NetworkObject.h"
#include <glm\glm.hpp>
#include <RakNetTypes.h>

/// <summary>
/// Represents a player's crosshair that they use to destroy targets on the
/// server side of the application. Information on crosshairs is kept on the
/// server side so the server can complete it's required tasks. But the
/// information kept on the server is different than what is kept on the client
/// side (e.g. textures) as the requirements are different.
/// </summary>
struct ServerCrosshair : public NetworkObject
{
public:

	/// <summary>
	/// Default Constructor. Returns a new ServerCrosshair object with it's
	/// variables set to null or zero.
	/// <returns>
	/// Returns a new ServerCrosshair object.
	/// </returns>
	/// </summary>
	ServerCrosshair();

	/// <summary>
	/// Constructor. Returns a new ServerCrosshair object with it's variables
	/// set to the passed in values.
	/// <param name="a_PlayerSysAddr">
	/// @param a_PlayerSysAddr: The system address of the controlling player.
	/// </param>
	/// <param name="a_ID">
	/// @param a_ID: The ID value of the object.
	/// </param>
	/// <param name="a_Position">
	/// @param a_Position: The position of the crosshair
	/// </param>
	/// <returns>
	/// Returns a new ServerCrosshair object.
	/// </returns>
	/// </summary>
	ServerCrosshair(const RakNet::SystemAddress& a_PlayerSysAddr,
		unsigned int a_ID, const glm::vec3& a_Position);

	/// <summary>
	/// Destructor. Cleans up any necessary memory.
	/// </summary>
	~ServerCrosshair();
	
	/*-------------------------------------------------------------------------
							PUBLIC MEMBER VARIABLES
	-------------------------------------------------------------------------*/
	/// <summary>
	/// The RakNet system address of the player that controls the crosshair.
	/// </summary>
	RakNet::SystemAddress	m_SysAddress;

	/// <summary>
	/// The position of the crosshair
	/// </summary>
	glm::vec3				m_Position;

	/// <summary>
	/// The velocity of the crosshair
	/// </summary>
	glm::vec3				m_Velocity;
};
