/*
file:				GameInfo.h
author:				Brad Cook.
description:		This file contains the declaration of the GameInfo class:
					A class which contains all the information required by the
					game.
date last modified:	15/01/2017
*/

#pragma once

#include "Server.h"
#include <vector>
#include <RakNetTypes.h>

/// <summary>
/// Contains all information that is required throughout various parts of the
/// game. It collates information on the players, scores and other information
/// that is relevant to the game.
/// </summary>
class GameInfo
{
public:

	/*-------------------------------------------------------------------------
							PUBLIC MEMBER VARIABLES
	-------------------------------------------------------------------------*/
	
	/// <summary>
	/// The connection info, including player IDs, for the clients connected
	/// to the server.
	/// </summary>
	std::vector<ConnectionInfo>				m_vConnectedPlayers;

	/// <summary>
	/// The list of RakNet system addresses that are going to be playing the
	/// game.
	/// </summary>
	std::vector<RakNet::SystemAddress>		m_vPlayersPlaying;

	/// <summary>
	/// The list of RakNet system addresses from players who have sent the
	/// ID_SERVER_READY_TO_START message and are ready to play the game. This
	/// variable is used in <see cref="ServerGameplayScene"/> to determine
	/// whether all players are ready.
	/// </summary>
	std::vector<RakNet::SystemAddress>		m_vPlayersReady;

	/// <summary>
	/// The RakNet addresses of players that have registered to play a game.
	/// </summary>
	std::vector<RakNet::SystemAddress>		m_vRegistered;

	/*-------------------------------------------------------------------------
								PUBLIC METHODS
	-------------------------------------------------------------------------*/

	/// <summary>
	/// Creates the singleton instance.
	/// </summary>
	static void create();

	/// <summary>
	/// Deletes the singleton instance.
	/// </summary>
	static void destroy();

	/// <summary>
	/// Returns a reference to the singleton instance.
	/// <returns>
	/// Returns: const GameInfo* reference to the singleton.
	/// </returns>
	/// </summary>
	static const GameInfo* getInstance() { return p_Singleton; }

	/// <summary>
	/// Determines whether the RakNet::SystemAddress has already been recorded.
	/// <returns>
	/// Returns true if the address has already been registered, returns false
	/// otherwise.
	/// </returns>
	/// </summary>
	static bool hasSysAddress(const RakNet::SystemAddress& a_Add);

	/// <summary>
	/// Adds a new connection to the list of connections.
	/// <param name="a_Add">
	/// @param a_Add: The system address of the client to add.
	/// </param>
	/// <param name="a_ID">
	/// @param a_ID: The ID of the client.
	/// </param>
	/// </summary>
	static void addNewConnection(const RakNet::SystemAddress& a_Add, int a_ID);

	/// <summary>
	/// Adds a new connection to the list of players who are ready to start
	/// gameplay.
	/// <param name="a_Add">
	/// @param a_Add: The system address of the client to add.
	/// </param>
	/// </summary>
	static void addPlayerReady(const RakNet::SystemAddress& a_Add);

	/// <summary>
	/// Removes the connection from the list of connections.
	/// <param name="a_Remove">
	/// @param a_Remove: The system address of the client to remove.
	/// </param>
	/// </summary>
	static void removeConnection(const RakNet::SystemAddress& a_Remove);

	/// <summary>
	/// Takes a record of all the players who will be playing the game.
	/// </summary>
	static void recordPlayersInGame();

	/// <summary>
	/// Remove a player as being either ready or as playing.
	/// <param name="a_Remove">
	/// @param a_Remove: The system address of the client to remove.
	/// </param>
	/// </summary>
	static void removePlayer(const RakNet::SystemAddress& a_Remove);

	/// <summary>
	/// Clear the players currently playing a game information.
	/// </summary>
	static void clearPlayersPlaying();

	/// <summary>
	/// Clear the players currently ready to play a game information.
	/// </summary>
	static void clearPlayersReady();

	/// <summary>
	/// Adds a new system address to the list of registered players.
	/// </summary>
	/// <param name="a_Add">
	/// @param a_Add: The system address of the client to add.
	/// </param>
	static void addRegistered(const RakNet::SystemAddress& a_Add);

	/// <summary>
	/// Clears the registered players.
	/// </summary>
	static void clearRegistered();

private:
	/*-------------------------------------------------------------------------
								PRIVATE METHODS
	-------------------------------------------------------------------------*/

	/// <summary>
	/// Default constructor. Does nothing.
	/// </summary>
	GameInfo();

	/// <summary>
	/// Default destructor. Performs any necessary memory cleaning up
	/// procedures.
	/// </summary>
	~GameInfo();

	/*-------------------------------------------------------------------------
							PRIVATE MEMBER VARIABLES
	-------------------------------------------------------------------------*/

	/// <summary>
	/// The singleton instance.
	/// </summary>
	static GameInfo*	p_Singleton;
};

/// <summary>
/// A functor class used to determine whether a RakNet::SystemAddress matches
/// the RakNet::SystemAddress of a <see cref="ConnectionInfo"/> object.
/// <remarks>
/// This functor is designed to be used with the "find..." standard algorithm
/// functions in "algorithm" header file.
/// </remarks>
/// </summary>
class FindSysAddress
{
private:

	/// <summary>
	/// The system IP address to find.
	/// </summary>
	RakNet::SystemAddress m_SysAddress;

public:

	/// <summary>
	/// Constructor. Assigns the connection ID to find.
	/// </summary>
	FindSysAddress(const RakNet::SystemAddress& a_Add) : m_SysAddress(a_Add) {}

	/// <summary>
	/// The overload of the '()' function (to use as a functor). Determines
	/// whether a RakNet::SystemAddress matches
	/// the RakNet::SystemAddress of a <see cref="ConnectionInfo"/> object.
	/// <returns>
	/// Returns true if it does, returns false otherwise.
	/// </returns>
	/// </summary>
	bool operator() (const ConnectionInfo& a_connectInfo) const
	{
		return a_connectInfo.m_SysAddress == m_SysAddress;
	}
};
