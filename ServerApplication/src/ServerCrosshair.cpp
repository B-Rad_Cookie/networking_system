/*
file:				ServerCrosshair.cpp
author:				Brad Cook.
description:		This file contains the definition of the ServerCrosshair
					struct: The representation of the crosshairs to destroy in
					the game on the server side.
date last modified:	15/01/2017
*/

#include "ServerCrosshair.h"

ServerCrosshair::ServerCrosshair() : NetworkObject()
{
	m_Position = glm::vec3(0);
	m_Velocity = glm::vec3(0);
}

ServerCrosshair::ServerCrosshair(const RakNet::SystemAddress& a_PlayerSysAddr, 
	unsigned int a_ID, const glm::vec3& a_Position)
	: NetworkObject(a_ID)
{
	m_SysAddress = a_PlayerSysAddr;
	m_Position = a_Position;
	m_Velocity = glm::vec3(0);
}

ServerCrosshair::~ServerCrosshair()
{
	// Do nothing
}
