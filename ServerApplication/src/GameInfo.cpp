/*
file:				GameInfo.cpp
author:				Brad Cook.
description:		This file contains the definition of the GameInfo class:
					A class which contains all the information required by the
					game.
date last modified:	15/01/2017
*/

#include "GameInfo.h"
#include <algorithm>

GameInfo* GameInfo::p_Singleton = nullptr;

GameInfo::GameInfo()
{
	m_vConnectedPlayers = std::vector<ConnectionInfo>();
}

GameInfo::~GameInfo()
{
	m_vConnectedPlayers.clear();
}

void GameInfo::create()
{
	if (p_Singleton == nullptr)
	{
		p_Singleton = new GameInfo();
	}
}

void GameInfo::destroy()
{
	if (p_Singleton != nullptr)
	{
		delete p_Singleton;
		p_Singleton = nullptr;
	}
}

bool GameInfo::hasSysAddress(const RakNet::SystemAddress& a_Add)
{
	// Search the vector list.
	auto foundSysAddr = std::find_if(
		p_Singleton->m_vConnectedPlayers.begin(),
		p_Singleton->m_vConnectedPlayers.end(),
		FindSysAddress(a_Add));

	// If not found, return false. otherwise return true.
	if (foundSysAddr == p_Singleton->m_vConnectedPlayers.end())
	{
		return false;
	}
	else
	{
		return true;
	}
}

void GameInfo::addNewConnection(const RakNet::SystemAddress& a_Add,
	int a_ID)
{
	ConnectionInfo newClient;
	newClient.m_SysAddress = a_Add;
	newClient.m_uiConnectionID = a_ID;
	p_Singleton->m_vConnectedPlayers.push_back(newClient);
}

void GameInfo::addPlayerReady(const RakNet::SystemAddress& a_Add)
{
	p_Singleton->m_vPlayersReady.push_back(a_Add);
}

void GameInfo::removeConnection(const RakNet::SystemAddress& a_Remove)
{
	// Find the connection.
	std::vector<ConnectionInfo>::iterator Connection = std::find_if(
		p_Singleton->m_vConnectedPlayers.begin(),
		p_Singleton->m_vConnectedPlayers.end(),
		FindSysAddress(a_Remove));

	// Remove if found.
	if (Connection != p_Singleton->m_vConnectedPlayers.end())
	{
		p_Singleton->m_vConnectedPlayers.erase(Connection);
	}

	// Check if address was registered. If so, remove.
	std::vector<RakNet::SystemAddress>::iterator addr = std::find(
		p_Singleton->m_vRegistered.begin(),
		p_Singleton->m_vRegistered.end(),
		a_Remove);

	if (addr != p_Singleton->m_vRegistered.end())
	{
		p_Singleton->m_vRegistered.erase(addr);
	}
}

void GameInfo::recordPlayersInGame()
{
	// Clear what is there now.
	p_Singleton->m_vPlayersPlaying.clear();

	// Copy the values scross from the connection list.
	for (std::vector<ConnectionInfo>::iterator connect = p_Singleton->m_vConnectedPlayers.begin();
	connect != p_Singleton->m_vConnectedPlayers.end(); connect++)
	{
		p_Singleton->m_vPlayersPlaying.push_back((*connect).m_SysAddress);
	}
}

void GameInfo::removePlayer(const RakNet::SystemAddress& a_Remove)
{
	// Find the address in players playing and remove it.
	std::vector<RakNet::SystemAddress>::iterator player = std::find(
		p_Singleton->m_vPlayersPlaying.begin(),
		p_Singleton->m_vPlayersPlaying.end(),
		a_Remove);

	if (player != p_Singleton->m_vPlayersPlaying.end())
	{
		p_Singleton->m_vPlayersPlaying.erase(player);
	}

	// Find the address in players ready and remove it.
	player = std::find(
		p_Singleton->m_vPlayersReady.begin(),
		p_Singleton->m_vPlayersReady.end(),
		a_Remove);

	if (player != p_Singleton->m_vPlayersReady.end())
	{
		p_Singleton->m_vPlayersReady.erase(player);
	}
}

void GameInfo::clearPlayersPlaying()
{
	p_Singleton->m_vPlayersPlaying.clear();
}

void GameInfo::clearPlayersReady()
{
	p_Singleton->m_vPlayersReady.clear();
}

void GameInfo::addRegistered(const RakNet::SystemAddress& a_Add)
{
	// Check if already added.
	std::vector<RakNet::SystemAddress>::iterator current = std::find(
		p_Singleton->m_vRegistered.begin(),
		p_Singleton->m_vRegistered.end(),
		a_Add);

	if (current == p_Singleton->m_vRegistered.end())
	{
		p_Singleton->m_vRegistered.push_back(a_Add);
	}
}

void GameInfo::clearRegistered()
{
	p_Singleton->m_vRegistered.clear();
}
