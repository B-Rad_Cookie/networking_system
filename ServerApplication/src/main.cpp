/*
file:				main.cpp
author:				Conan Burke, Brad Cook.
description:		This file is the entry point of the server-side application.
date last modified:	09/01/2017
*/

#include "Server.h"
#include <Windows.h>

Server* app = nullptr;

/// <summary>
/// Ensures the server app memory is properly released.
/// </summary>
BOOL ctrlHandler( DWORD fdwCtrlType)
{
	// Called when window has been closed
	if (fdwCtrlType == CTRL_CLOSE_EVENT)
	{
		// Call quit on the server
		if (app != nullptr)
		{
			app->quit();
			delete app;
		}
		return (TRUE);
	}
	else
	{
		return (FALSE);
	}
}

int main() {
	SetConsoleCtrlHandler((PHANDLER_ROUTINE)ctrlHandler, TRUE);
	app = new Server();
	app->run();
	delete app;
	app = nullptr;

	return 0;
}
