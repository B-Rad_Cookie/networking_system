/*
file:				NetworkObject.h
author:				Brad Cook.
description:		This file contains the definition of the NetworkObject
					struct: A base struct for all objects that are to be sent
					over a network.
date last modified:	13/01/2017
*/

#pragma once

/// <summary>
/// Base struct for all objects that are to be sent over a network. It contains
/// variables important to infromation being sent over the network (e.g. object
/// IDs).
/// </summary>
struct NetworkObject
{
	/// <summary>
	/// Default Constructor. Returns a new NetworkObject object with it's
	/// variables set to null or zero.
	/// <returns>
	/// Returns a new NetworkObject.
	/// </returns>
	/// </summary>
	NetworkObject() { m_uiNetworkID = 0; }

	/// <summary>
	/// Constructor. Returns a new NetworkObject object with it's variables set to
	/// the passed in values.
	/// <param name="a_ID">
	/// @param a_ID: The ID value of the object.
	/// </param>
	/// <returns>
	/// Returns a new NetworkObject.
	/// </returns>
	/// </summary>
	NetworkObject(unsigned int a_ID) { m_uiNetworkID = a_ID; }

	/// <summary>
	/// The ID value of the object.
	/// </summary>
	unsigned int		m_uiNetworkID;
};
