/*
file:				ServerEndScene.cpp
author:				Brad Cook.
description:		This file contains the definition of the ServerEndScene
					class: The scene that is used at the end of the game.
date last modified:	15/01/2017
*/

#include "ServerEndScene.h"
#include "CustomGameMessages.h"
#include "GameInfo.h"

#include <RakPeerInterface.h>
#include <MessageIdentifiers.h>
#include <BitStream.h>

#include <iostream>
#include <functional>

bool ServerEndScene::init(RakNet::RakPeerInterface* a_p_PeerInter)
{
	// Get the peer interface reference.
	m_p_PeerInter = a_p_PeerInter;

	// Setup the time manager.
	m_p_TimeManager = new target::TimeManager();

	return true;
}

void ServerEndScene::destroy()
{
	// Clear timers.
	m_p_TimeManager->clearTimers();

	// Remove the timer.
	delete m_p_TimeManager;
}

std::string ServerEndScene::update(double dt)
{
	// if start next game, do so.
	if (m_bStartNextGame == true)
	{
		std::cout << "Starting setup of new game" << std::endl << std::endl;
		return "StartScene";
	}

	// Update the time manager.
	m_p_TimeManager->update(dt);

	// If received all the scores, send all scores to the clients.
	if (m_bWaitingForScores == true &&
		m_vPlayerScores.size() >= GameInfo::getInstance()->m_vPlayersPlaying.size())
	{
		m_bWaitingForScores = false;
		sendAllPlayerScores();

		// Start the timer to start the next round of games.
		m_p_TimeManager->addTimer("Countdown",
			std::bind(&ServerEndScene::startNextGame, this),
			15.0f, false);
	}
	return "";
}

void ServerEndScene::activate()
{
	// Set send scores to true.
	m_bWaitingForScores = true;

	// Set next game to false
	m_bStartNextGame = false;

	// Reset the time manager.
	m_p_TimeManager->reset();
}

void ServerEndScene::deactivate()
{
	// Stop all timers.
	m_p_TimeManager->clearTimers();

	// remove the player scores
	m_vPlayerScores.clear();

	// Remove the players playing list.
	GameInfo::clearPlayersPlaying();
}

//#########################################################################
//						Network message handling

std::string ServerEndScene::handleNetworkMessages(RakNet::Packet* a_p_Packet)
{
	// Set the return string to nothing ("")
	std::string sReturnString = "";

	switch (a_p_Packet->data[0])
	{
	case ID_SERVER_SEND_SCORE:
	{
		// Client sending their score
		handlePlayerScore(a_p_Packet);
		break;
	}
	}

	// Return the return string.
	return sReturnString;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//					Network Messages sent to client

void ServerEndScene::sendAllPlayerScores()
{
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_CLIENT_ALL_PLAYER_SCORES));
	// Also write the size of how many scores.
	bs.Write(m_vPlayerScores.size());

	// Go through and write all the values for each of the scores.
	for (int i = 0; i < m_vPlayerScores.size(); i++)
	{
		bs.Write(m_vPlayerScores[i].m_uiPlayerNumber);
		bs.Write(m_vPlayerScores[i].m_uiPlayerScore);
	}

	m_p_PeerInter->Send(&bs, IMMEDIATE_PRIORITY, RELIABLE, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);

	std::cout << "Sending game results." << std::endl << std::endl;
}

void ServerEndScene::sendNewGame()
{
	// Clear the number of players registered.
	GameInfo::clearRegistered();

	// Send the messages
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_CLIENT_NEW_GAME));
	m_p_PeerInter->Send(&bs, IMMEDIATE_PRIORITY, RELIABLE, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//					Network Messages received from client

void ServerEndScene::handlePlayerScore(RakNet::Packet* a_p_Packet)
{
	// Firstly read in the information.
	RakNet::BitStream bs(a_p_Packet->data, a_p_Packet->length, false);
	bs.IgnoreBytes(sizeof(RakNet::MessageID));
	unsigned int uiClientID;
	unsigned int uiPlayerScore;
	bs.Read(uiClientID);
	bs.Read(uiPlayerScore);

	// Add the information to the list of scores.
	PlayerScores PlayerScore;
	PlayerScore.m_uiPlayerNumber = uiClientID;
	PlayerScore.m_uiPlayerScore = uiPlayerScore;
	m_vPlayerScores.push_back(PlayerScore);
}

//#########################################################################
//						Time Events.

void ServerEndScene::startNextGame()
{
	// Send the new game message.
	sendNewGame();

	// Change the scene back to starting.
	m_bStartNextGame = true;
}
