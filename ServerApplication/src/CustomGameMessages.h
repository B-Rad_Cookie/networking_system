/*
file:				CustomGameMessages.h
author:				Brad Cook.
description:		This file contains the RakNet::MessageID enum values for
					custom messages sent in the game.
date last modified:	15/01/2017
*/

#pragma once

#include <MessageIdentifiers.h>

/// <summary>
/// The custom (i.e. ones not defined by RakNet) RakNet::MessageID enum values
/// for messages sent in the game.
/// </summary>
enum GameMessages
{
	// Messages sent to both.
	ID_SERVER_TEXT_MESSAGE = ID_USER_PACKET_ENUM + 1,

	//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	//						Messages sent to the client.

	/// <summary>
	/// Server sending connection and ID information to the client (e.g.
	/// Player ID/Number)
	/// </summary>
	ID_CLIENT_CONNECT_INFO,

	/// <summary>
	/// Server tells clients to begin countdown to game starting.
	/// </summary>
	ID_CLIENT_START_COUNTDOWN,

	/// <summary>
	/// Server tells one client to begin countdown to game starting.
	/// </summary>
	ID_CLIENT_START_COUNTDOWN_ONE,

	/// <summary>
	/// Server tells clients to stop countdown to game starting.
	/// </summary>
	ID_CLIENT_STOP_COUNTDOWN,

	/// <summary>
	/// Server tells clients that the agme is being set up.
	/// </summary>
	ID_CLIENT_SETUP_GAME,

	/// <summary>
	/// Server sends clients the intial targets for a round of gameplay.
	/// </summary>
	ID_CLIENT_INITIAL_TARGETS,

	/// <summary>
	/// Server sends clients the initial crosshairs.
	/// </summary>
	ID_CLIENT_CROSSHAIRS,

	/// <summary>
	/// Server tells clients gameplay round has started.
	/// </summary>
	ID_CLIENT_ROUND_START,

	/// <summary>
	/// Server sends clients the crosshair updates.
	/// </summary>
	ID_CLIENT_UPDATE_CROSSHAIRS,

	/// <summary>
	/// Server sends clients info on the destruction of a target.
	/// </summary>
	ID_CLIENT_TARGET_DESTROYED,

	/// <summary>
	/// Server sends clients info on removal of a target (i.e. time ran out).
	/// </summary>
	ID_CLIENT_TARGET_REMOVED,

	/// <summary>
	/// Server sends clients info on new targets added.
	/// </summary>
	ID_CLIENT_NEW_TARGETS,

	/// <summary>
	/// Server sends clients info on a player who has left the game.
	/// </summary>
	ID_CLIENT_REMOVE_PLAYER,

	/// <summary>
	/// Server informs clients round of gameplay has ended.
	/// </summary>
	ID_CLIENT_END_ROUND,

	/// <summary>
	/// Server informs clients a new round of gameplay is going to be done.
	/// </summary>
	ID_CLIENT_RESTART_ROUND,

	/// <summary>
	/// Server informs clients game has ended.
	/// </summary>
	ID_CLIENT_END_GAME,

	/// <summary>
	/// Server sends clients all player's scores.
	/// </summary>
	ID_CLIENT_ALL_PLAYER_SCORES,

	/// <summary>
	/// Server informs clients a new game is going to start.
	/// </summary>
	ID_CLIENT_NEW_GAME,

	//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	//						Messages sent to the Server.

	/// <summary>
	/// Client tells server they have registed to start a game.
	/// </summary>
	ID_SERVER_REGISTER,

	/// <summary>
	/// Client tells server they are ready to start game.
	/// </summary>
	ID_SERVER_READY_TO_START,
	
	/// <summary>
	/// Client tells server they have loaded the intial data.
	/// </summary>
	ID_SERVER_INITIAL_DATA_LOADED,

	/// <summary>
	/// Client sends the crosshair movement info to server.
	/// </summary>
	ID_SERVER_MOVEMENT_INFO,

	/// <summary>
	/// Client inquiries server as to whether they have destroyed a target.
	/// </summary>
	ID_SERVER_CHECK_DESTROY_TARGET,

	/// <summary>
	/// Client tells server they are at the eend of the round.
	/// </summary>
	ID_SERVER_AT_END_ROUND,

	/// <summary>
	/// Client tells server the score they achieved.
	/// </summary>
	ID_SERVER_SEND_SCORE
};
