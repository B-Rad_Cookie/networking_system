/*
file:				ServerGameplayScene.cpp
author:				Brad Cook.
description:		This file contains the definition of the ServerGameplayScene
					class: The scene that controls the main gameplay on the
					server side.
date last modified:	14/01/2017
*/

#include "ServerGameplayScene.h"

#include "GameInfo.h"
#include "ServerTarget.h"
#include "ServerCrosshair.h"
#include <ColDetect.h>
#include <Circle.h>
#include "Consts.h"

#include <RakPeerInterface.h>
#include <MessageIdentifiers.h>
#include <BitStream.h>

#include <iostream>
#include <time.h>
#include <random>
#include <algorithm>
#include <functional>

bool ServerGameplayScene::init(RakNet::RakPeerInterface* a_p_PeerInter)
{
	// Setup the time manager.
	m_p_TimeManager = new target::TimeManager();

	// Get the peer interface reference.
	m_p_PeerInter = a_p_PeerInter;

	// Set the random generator
	srand(time(NULL));

	return true;
}

void ServerGameplayScene::destroy()
{
	// Clear Entities
	clearEntities();

	// Clear the timers.
	m_p_TimeManager->clearTimers();

	// Remove the time manager.
	delete m_p_TimeManager;
}

std::string ServerGameplayScene::update(double dt)
{
	// If there's only one player left, end the game.
	if (GameInfo::getInstance()->m_vPlayersPlaying.size() <= 1)
	{
		m_bGameEnd = true;
		std::cout << "Game finished" << std::endl << std::endl;
		return "EndScene";
	}

	// Update TimeManager.
	m_p_TimeManager->update(dt);

	// If the game has ended, end immediately.
	if (m_bGameEnd == true)
	{
		std::cout << "Game finished" << std::endl << std::endl;
		return "EndScene";
	}

	// Split the update based on the gameplay state.
	std::string sReturnString = "";
	switch (m_eGameplayStatus)
	{
	case GameplayStatus::Waiting:
	{
		sReturnString = updateWaiting(dt);
		break;
	}
	case GameplayStatus::Setup:
	{
		sReturnString = updateSetup(dt);
		break;
	}
	case GameplayStatus::Starting:
	{
		sReturnString = updateStarting(dt);
		break;
	}
	case GameplayStatus::Playing:
	{
		sReturnString = updatePlaying(dt);
		break;
	}
	case GameplayStatus::EndRound:
	{
		sReturnString = updateEndRound(dt);
		break;
	}
	default:
		break;
	}	

	// Return if the return string for changing scene is set to something.
	if (sReturnString != "")
	{
		return sReturnString;
	}

	return "";
}

std::string ServerGameplayScene::updateWaiting(double dt)
{
	// Check to see if all players are connected.
	if (GameInfo::getInstance()->m_vPlayersReady.size() >=
		GameInfo::getInstance()->m_vPlayersPlaying.size())
	{
		// All players are ready.
		setupGame();
	}

	return "";
}

std::string ServerGameplayScene::updateSetup(double dt)
{
	// Wait to hear whether all the players have loaded the initial stuff.
	if (m_uiPlayersLoadedData ==
		GameInfo::getInstance()->m_vPlayersPlaying.size())
	{
		gameStarting();
	}
	return "";
}

std::string ServerGameplayScene::updateStarting(double dt)
{
	// Do nothing.
	return "";
}

std::string ServerGameplayScene::updatePlaying(double dt)
{
	// If all player's have updated movement, send all crosshairs to players.
	// Waiting till everyopne has finished avoids sending the message multiple
	// times.
	if (m_uiPlayersUpdatedMovement ==
		GameInfo::getInstance()->m_vPlayersPlaying.size())
	{
		// Send the update info.
		sendUpdateCrosshairs();

		// Reset the updated info counter
		m_uiPlayersUpdatedMovement = 0;
	}

	// If targets have been removed, replace them.
	if (m_vTargets.size() < Consts::uiMAX_NUM_OF_TARGETS)
	{
		int iNumTargetsToAdd = (
			Consts::uiMAX_NUM_OF_TARGETS - m_vTargets.size());
		addTargets(iNumTargetsToAdd);
		sendNewTargets(iNumTargetsToAdd);
	}

	return "";
}

std::string ServerGameplayScene::updateEndRound(double dt)
{
	// Wait to hear whether all  players have loaded stopped playing.
	if (m_uiPlayersRoundEnd ==
		GameInfo::getInstance()->m_vPlayersPlaying.size())
	{
		// Set gameplay status to waiting.
		m_eGameplayStatus = GameplayStatus::Waiting;

		// Send the restart round message.
		sendRoundRestart();
	}

	return "";
}

void ServerGameplayScene::activate()
{
	// Reset the timer.
	m_p_TimeManager->reset();

	// Set the round number to 0.
	m_uiRoundNumber = 0;
	
	// Start of as waiting for a game to occur.
	m_eGameplayStatus = GameplayStatus::Waiting;

	// Set the target ID to 0.
	m_uiTargetID = 0;

	// Set game end to false
	m_bGameEnd = false;
}

void ServerGameplayScene::deactivate()
{
	// Clear entities
	clearEntities();

	// Clear the timers.
	m_p_TimeManager->clearTimers();

	// Send the game end message.
	sendGameEnd();

	// Clear the players ready.
	GameInfo::clearPlayersReady();
}

//#########################################################################
//								Game setup

void ServerGameplayScene::setupGame()
{
	// Send the game setup message, set the gameplay status to setup, and 
	// increment the round number
	std::cout << "All players ready to start" << std::endl;
	sendGameSetup();
	m_eGameplayStatus = GameplayStatus::Setup;
	m_uiRoundNumber++;
	m_uiPlayersLoadedData = 0;
	m_uiPlayersUpdatedMovement = 0;
	m_uiPlayersRoundEnd = 0;
	std::cout << "Setting up round " << m_uiRoundNumber << std::endl << std::endl;

	// Setup the creation of the crosshairs
	setupCrosshairs();

	// Send the crosshairs
	sendCrosshairs();

	// Setup the creation of the targets.
	addTargets(Consts::uiMAX_NUM_OF_TARGETS);

	// Send the targets.
	sendInitialTargets();
}

void ServerGameplayScene::gameStarting()
{
	std::cout << "Starting round " << m_uiRoundNumber << std::endl << std::endl;

	// Set the gameplay status to starting.
	m_eGameplayStatus = GameplayStatus::Starting;

	// Start the countdown timer.
	m_p_TimeManager->addTimer("Countdown",
		std::bind(&ServerGameplayScene::startRound, this), 5.0f, false);
}

//#########################################################################
//								Entities

void ServerGameplayScene::addTargets(unsigned int a_uiNum)
{
	for (int i = 0; i < a_uiNum; i++)
	{
		// generate a random type (based on percentages).
		int randNum = (rand() % 100) + 1;
		TargetType eNewType;
		if (randNum < 50)
		{
			eNewType = TargetType::Blue;
		}
		else if (randNum >= 80)  // Red
		{
			eNewType = TargetType::Red;
		}
		else  // Green
		{
			eNewType = TargetType::Green;
		}

		// Set the window widths and heights so that the target will fit
		int iWidth = 1280;
		int iHeight = 720;

		bool bPositionOverlap = true;
		Circle* p_Circle = nullptr;
		do
		{ 
			// Remove the circle memory for a new circle.
			if (p_Circle != nullptr)
			{
				delete p_Circle;
			}

			// generate a random position (two floats) so that it fits
			// in the screen.
			float x_coord = (float)(rand() % (int)
				(iWidth - (2 * Consts::TYPE_TO_SIZE[eNewType])) + Consts::TYPE_TO_SIZE[eNewType]);
			float y_coord = (float)(rand() % (int)
				(iHeight - (2 * Consts::TYPE_TO_SIZE[eNewType])) + Consts::TYPE_TO_SIZE[eNewType]);
			glm::vec3 Position = glm::vec3(x_coord, y_coord, 1.0f);

			// Check to see if it doesn't overlap with any other target.
			// Create a circle out of the position and type for testing.
			p_Circle = new Circle(Position, Consts::TYPE_TO_SIZE[eNewType]);
			bPositionOverlap = isOverlapping(p_Circle);

		} while(bPositionOverlap == true);

		// Add target to the list and start it's removal timer.
		ServerTarget* p_newTarget = new ServerTarget(
			m_uiTargetID++, p_Circle, eNewType);
		m_vTargets.push_back(p_newTarget);

		// Set the name of the timer to be "TargetRemove[ID]".
		using std::placeholders::_1;
		m_p_TimeManager->addTimer(
			std::string("TargetRemove" + std::to_string(m_uiTargetID)),
			std::bind(&ServerGameplayScene::removeTarget, this, _1),
			p_newTarget->m_uiNetworkID,
			p_newTarget->m_fDuration, false);
	}
}

bool ServerGameplayScene::isOverlapping(const Circle* a_p_Circle)
{
	for (std::vector<ServerTarget*>::iterator target = m_vTargets.begin();
		 target != m_vTargets.end(); target++)
	{
		if (ColDetect::AreColliding(a_p_Circle,
			(*target)->m_p_Circle) == true)
		{
			return true;
		}
	}
	return false;
}

void ServerGameplayScene::setupCrosshairs()
{
	// Set the window widths and heights to use to place the crosshairs.
	int iWidth = 1280;
	int iHeight = 720;

	// Keep a record of the number of crosshairs added.
	int iPlayersAdded = 1;

	// Get the number of players actually playing.
	for (auto connection = GameInfo::getInstance()->m_vConnectedPlayers.begin();
		 connection != GameInfo::getInstance()->m_vConnectedPlayers.end(); connection++)
	{
		// See i the connection is a conenction that is playing.
		auto addr = std::find(
			GameInfo::getInstance()->m_vPlayersPlaying.begin(),
			GameInfo::getInstance()->m_vPlayersPlaying.end(),
			(*connection).m_SysAddress);

		// if so, then create a crosshair for it. Set the Network ID to be the
		// ID of the player.
		if (addr != GameInfo::getInstance()->m_vPlayersPlaying.end())
		{
			// The Crosshairs are going to start in a square pattern around
			// the centre, based on the number of players.

			// The 1st and 3rd players will be just to the left of centre.
			float fFromCentreX;
			if (iPlayersAdded % 2 != 0)
			{ 
				fFromCentreX = -30.0f;
			}
			else  // 2nd and 4th or just to the right of centre
			{
				fFromCentreX = 30.0f;
			}
			
			float fFromCentreY;
			// The 1st and 2nd players will start from just above centre.
			if (iPlayersAdded <= 2)
			{
				fFromCentreY = 30.0f;
			}
			else // The 3rd and 4th players will start just below centre.
			{
				fFromCentreY = -30.0f;
			}

			// Set the position of the crosshair
			glm::vec3 Pos = glm::vec3(((float)iWidth / 2.0f) + fFromCentreX,
				((float)iHeight / 2.0f) + fFromCentreY,
				1.0f);

			// Create the Crosshair and add it. Set the Network ID to be the
			// ID of the player.
			m_vCrosshairs.push_back(new ServerCrosshair(
				(*connection).m_SysAddress,
				(*connection).m_uiConnectionID, Pos));
			iPlayersAdded++;
		}
	}

}

void ServerGameplayScene::removeTarget(int a_iID)
{
	// Find the target to remove.
	int iTargetIndex = -1;
	for (int i = 0; i < m_vTargets.size(); i++)
	{
		if (m_vTargets[i]->m_uiNetworkID == a_iID)
		{
			iTargetIndex = i;
			break;
		}
	}

	// Remove the target.
	if (iTargetIndex != -1)
	{
		delete m_vTargets[iTargetIndex];
		m_vTargets.erase(m_vTargets.begin() + iTargetIndex);
		sendTargetRemoved(a_iID);
	}
}

void ServerGameplayScene::clearEntities()
{
	// Go through and delete all the targets.
	for (std::vector<ServerTarget*>::iterator target = m_vTargets.begin();
		 target != m_vTargets.end(); target++)
	{
		delete (*target);
	}
	m_vTargets.clear();

	// Go through and delete all the crosshairs.
	for (std::vector<ServerCrosshair*>::iterator cross = m_vCrosshairs.begin();
	cross != m_vCrosshairs.end(); cross++)
	{
		delete (*cross);
	}
	m_vCrosshairs.clear();
}

//#########################################################################
//						Network message handling

void ServerGameplayScene::handlePlayerDisconnected(
	const RakNet::SystemAddress& a_SystemAddr)
{
	// Remove the player from the information.
	GameInfo::removePlayer(a_SystemAddr);

	// Remove the player's crosshair.
	int iCrossIndex = -1;
	for (int i = 0; i < m_vCrosshairs.size(); i++)
	{
		if (m_vCrosshairs[i]->m_SysAddress == a_SystemAddr)
		{
			iCrossIndex = i;
			break;
		}
	}
	if (iCrossIndex != -1)
	{
		unsigned int uiPlayerID = m_vCrosshairs[iCrossIndex]->m_uiNetworkID;
		delete m_vCrosshairs[iCrossIndex];
		m_vCrosshairs.erase(m_vCrosshairs.begin() + iCrossIndex);

		// Send the id of the player that disconnected.
		sendRemovePlayer(uiPlayerID);
	}
}

std::string ServerGameplayScene::handleNetworkMessages(RakNet::Packet* a_p_Packet)
{
	// Set the return string to nothing ("")
	std::string sReturnString = "";

	switch (a_p_Packet->data[0])
	{
	case ID_SERVER_INITIAL_DATA_LOADED:
	{
		// Client finished loading
		handleDataLoaded();
		break;
	}
	case ID_SERVER_MOVEMENT_INFO:
	{
		// Client updating movement info
		handleMovementInfo(a_p_Packet);
		break;
	}
	case ID_SERVER_CHECK_DESTROY_TARGET:
	{
		// Client inquiring about target destroy.
		handleCheckDestroyTarget(a_p_Packet);
		break;
	}
	case ID_SERVER_AT_END_ROUND:
	{
		// Client at end of round
		handleAtEndRound();
		break;
	}
	}

	// Return the return string.
	return sReturnString;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//					Network Messages sent to clients

void ServerGameplayScene::sendGameSetup()
{
	// Send the ID_CLIENT_SETUP_GAME message.
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_CLIENT_SETUP_GAME));
	m_p_PeerInter->Send(&bs, IMMEDIATE_PRIORITY, RELIABLE, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void ServerGameplayScene::sendInitialTargets()
{
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_CLIENT_INITIAL_TARGETS));

	// Go through and write all the values for each of the targets.
	for (int i = 0; i < m_vTargets.size(); i++)
	{
		writeTargetToMessage(bs, m_vTargets[i]);
	}

	m_p_PeerInter->Send(&bs, IMMEDIATE_PRIORITY, RELIABLE, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void ServerGameplayScene::writeTargetToMessage(RakNet::BitStream& a_bs,
	const ServerTarget* a_p_Target)
{
	// Only need to send the ID, target type and the x and y co-ordinate.
	// Need to send the native types. Also need to send the enum as a byte.
	a_bs.Write(a_p_Target->m_uiNetworkID);
	a_bs.Write((int)a_p_Target->m_eType);
	a_bs.Write(a_p_Target->m_p_Circle->m_centre.x);
	a_bs.Write(a_p_Target->m_p_Circle->m_centre.y);
}

void ServerGameplayScene::sendCrosshairs()
{
	// As well as the Crosshair information, need to send the number of
	// crosshairs as well.
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_CLIENT_CROSSHAIRS));
	bs.Write(m_vCrosshairs.size());  // Number of crosshairs
	for (int i = 0; i < m_vCrosshairs.size(); i++)
	{
		bs.Write(m_vCrosshairs[i]->m_uiNetworkID);
		bs.Write(m_vCrosshairs[i]->m_Position.x);
		bs.Write(m_vCrosshairs[i]->m_Position.y);
	}
	m_p_PeerInter->Send(&bs, IMMEDIATE_PRIORITY, RELIABLE, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void ServerGameplayScene::sendRoundStart()
{
	// Send the ID_CLIENT_ROUND_START message.
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_CLIENT_ROUND_START));
	m_p_PeerInter->Send(&bs, IMMEDIATE_PRIORITY, RELIABLE, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void ServerGameplayScene::sendUpdateCrosshairs()
{
	// As well as the Crosshair information, need to send the number of
	// crosshairs as well.
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_CLIENT_UPDATE_CROSSHAIRS));
	bs.Write(m_vCrosshairs.size());  // Number of crosshairs
	for (int i = 0; i < m_vCrosshairs.size(); i++)
	{
		bs.Write(m_vCrosshairs[i]->m_uiNetworkID);
		bs.Write(m_vCrosshairs[i]->m_Position.x);
		bs.Write(m_vCrosshairs[i]->m_Position.y);
		bs.Write(m_vCrosshairs[i]->m_Velocity.x);
		bs.Write(m_vCrosshairs[i]->m_Velocity.y);
	}

	// Send with low priority
	m_p_PeerInter->Send(&bs, LOW_PRIORITY, RELIABLE, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void ServerGameplayScene::sendTargetDestroyed(unsigned int a_uiTargetID,
	unsigned int a_uiClientID)
{
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_CLIENT_TARGET_DESTROYED));

	// Send the passed in information.
	bs.Write(a_uiTargetID);
	bs.Write(a_uiClientID);

	m_p_PeerInter->Send(&bs, IMMEDIATE_PRIORITY, RELIABLE, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void ServerGameplayScene::sendTargetRemoved(unsigned int a_uiTargetID)
{
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_CLIENT_TARGET_REMOVED));

	// Send the passed in information.
	bs.Write(a_uiTargetID);

	m_p_PeerInter->Send(&bs, IMMEDIATE_PRIORITY, RELIABLE, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void ServerGameplayScene::sendNewTargets(int a_iNumAdded)
{
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_CLIENT_NEW_TARGETS));
	bs.Write(a_iNumAdded);

	// As all new targets are added to the back, start from the back when
	// adding targets.
	for (int i = m_vTargets.size() - a_iNumAdded; i < m_vTargets.size(); i++)
	{
		writeTargetToMessage(bs, m_vTargets[i]);
	}

	m_p_PeerInter->Send(&bs, IMMEDIATE_PRIORITY, RELIABLE, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void ServerGameplayScene::sendRemovePlayer(unsigned int a_uiClientID)
{
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_CLIENT_REMOVE_PLAYER));
	bs.Write(a_uiClientID);
	m_p_PeerInter->Send(&bs, IMMEDIATE_PRIORITY, RELIABLE, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void ServerGameplayScene::sendRoundEnd()
{
	// Send the ID_CLIENT_END_ROUND message.
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_CLIENT_END_ROUND));
	m_p_PeerInter->Send(&bs, IMMEDIATE_PRIORITY, RELIABLE, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void ServerGameplayScene::sendRoundRestart()
{
	// Send the ID_CLIENT_RESTART_ROUND message.
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_CLIENT_RESTART_ROUND));
	m_p_PeerInter->Send(&bs, IMMEDIATE_PRIORITY, RELIABLE, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void ServerGameplayScene::sendGameEnd()
{
	// Send the ID_CLIENT_END_GAME message.
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_CLIENT_END_GAME));
	m_p_PeerInter->Send(&bs, IMMEDIATE_PRIORITY, RELIABLE, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//				Network Messages received from clients

void ServerGameplayScene::handleDataLoaded()
{
	// Increment they number of players that have loaded the data.
	m_uiPlayersLoadedData++;
}

void ServerGameplayScene::handleMovementInfo(RakNet::Packet* a_p_Packet)
{
	// Firstly read in the information.
	RakNet::BitStream bs(a_p_Packet->data, a_p_Packet->length, false);
	bs.IgnoreBytes(sizeof(RakNet::MessageID));
	unsigned int ID;
	float fPositionX;
	float fPositionY;
	float fVelocityX;
	float fVelocityY;
	bs.Read(ID);
	bs.Read(fPositionX);
	bs.Read(fPositionY);
	bs.Read(fVelocityX);
	bs.Read(fVelocityY);

	// Find the crosshair this information is referring to, and update it's
	// info.
	bool bFound = false;
	for (int i = 0; i < m_vCrosshairs.size(); i++)
	{
		if (m_vCrosshairs[i]->m_uiNetworkID == ID)
		{
			bFound = true;

			m_vCrosshairs[i]->m_Position = glm::vec3(fPositionX, fPositionY, 1);
			m_vCrosshairs[i]->m_Velocity = glm::vec3(fVelocityX, fVelocityY, 1);

			// Increment the number of players who have updated their info.
			m_uiPlayersUpdatedMovement++;
			break;
		}
	}

	// If the ID couldn't be found, report to server.
	if (bFound = false)
	{
		std::cerr << "Could not find Client ID " << ID;
		std::cerr << std::endl << std::endl;
	}
}

void ServerGameplayScene::handleCheckDestroyTarget(RakNet::Packet* a_p_Packet)
{
	// Read the information.
	RakNet::BitStream bs(a_p_Packet->data, a_p_Packet->length, false);
	bs.IgnoreBytes(sizeof(RakNet::MessageID));
	unsigned int uiTargetID;
	unsigned int uiClientID;
	bs.Read(uiTargetID);
	bs.Read(uiClientID);

	// Check if the target is there. If it is, send the information to the
	// clients, informing which client got the target.
	int iTargetIndex = -1;
	for (int i = 0; i < m_vTargets.size(); i++)
	{
		if (m_vTargets[i]->m_uiNetworkID == uiTargetID)  // success.
		{
			iTargetIndex = i;
			sendTargetDestroyed(uiTargetID, uiClientID);
			break;
		}
	}

	// Remove the target
	if (iTargetIndex != -1)
	{
		// Stop it's timer.
		m_p_TimeManager->stopTimer(
			std::string("RemoveTarget" + std::to_string(
				m_vTargets[iTargetIndex]->m_uiNetworkID)));

		delete m_vTargets[iTargetIndex];
		m_vTargets.erase(m_vTargets.begin() + iTargetIndex);
	}
}

void ServerGameplayScene::handleAtEndRound()
{
	// Increment they number of players that are at round end.
	m_uiPlayersRoundEnd++;
}

//#########################################################################
//						Time based events handling

void ServerGameplayScene::startRound()
{
	std::cout << "Round " << m_uiRoundNumber << " started.";
	std::cout << std::endl << std::endl;
	// Set the gameplay status to playing.
	m_eGameplayStatus = GameplayStatus::Playing;

	// Start the round timer.
	m_p_TimeManager->addTimer("EndRound",
		std::bind(&ServerGameplayScene::endRound, this),
		Consts::fGAMEPLAY_ROUND_TIME, false);

	// Send the start round message to the clients.
	sendRoundStart();
}

void ServerGameplayScene::endRound()
{
	// Check to see if the number of rounds has reached the end.
	if (m_uiRoundNumber >= Consts::uiNUM_OF_ROUNDS)
	{
		// Game finished. Dont worry about clearing entities here. There will
		// be a change to the EndScene, and the deactivate function here
		// clears the entities.
		m_bGameEnd = true;	

		std::cout << "Game finished" << std::endl << std::endl;
	}
	else // Round finished.
	{  
		std::cout << "Round " << m_uiRoundNumber << " finished.";
		std::cout << std::endl << std::endl;
	    // Send the end round message
		sendRoundEnd();

		// Clear the players ready.
		GameInfo::clearPlayersReady();

		// Set the gameplay status to end.
		m_eGameplayStatus = GameplayStatus::EndRound;

		// Clear all entities.
		clearEntities();
	}
}
