/*
file:				ServerEndScene.h
author:				Brad Cook.
description:		This file contains the declaration of the ServerEndScene
					class: The scene that is used at the end of the game.
date last modified:	15/01/2017
*/

#pragma once

#include "IScene.h"
#include "TimeManager.h"
#include <vector>

/// <summary>
/// A struct representing the information required for player scores.
/// </summary>
struct PlayerScores
{
	/// <summary>
	/// The player number assigned to the player.
	/// </summary>
	unsigned int	m_uiPlayerNumber;

	/// <summary>
	/// The game score the player has.
	/// </summary>
	unsigned int	m_uiPlayerScore;
};

/// <summary>
/// The scene that is used at the end. This scene is used when the main
/// gameplay finishes. It figures out the player scores and sends the
/// information to the players.
/// </summary>
class ServerEndScene : public IScene
{
public:
	/// <summary>
	/// Default Constructor. Returns a new ServerEndScene instance. To set up
	/// the scene, call the init() function.
	/// <returns>
	/// returns ServerEndScene instance.
	/// </returns>
	/// </summary>
	ServerEndScene() {}

	/// <summary>
	/// Sets up all the elements required by the scene. This function is called
	/// when the game is first loaded.
	/// <param name="a_p_PeerInter">
	/// @param a_p_PeerInter: The RakNet::RakPeerInterface object in use for
	/// the server.
	/// </param>
	/// </summary>
	bool init(RakNet::RakPeerInterface* a_p_PeerInter);

	/// <summary>
	/// Deletes and frees up up all the elements required by the scene. This
	/// function is called when the game is last closed.
	/// </summary>
	void destroy();

	/// <summary>
	/// Updates all the relevant information (elements) in the
	/// scene per frame.
	/// <param name="dt">
	/// @param dt: Delta time, the time (in seconds) between frames executing.
	/// </param>
	/// <returns>
	/// Returns the name of the scene to change to.
	/// </returns>
	/// </summary>
	std::string update(double dt);

	/// <summary>
	/// Sets up all the elements in the scene. This function is called everytime
	/// the scene is set as the active scene (i.e. When
	/// Application.setActiveScene is called). It is not called when the game is
	/// initially loaded.
	/// </summary>
	void activate();

	/// <summary>
	/// Cleans up all the elements in the scene. This function is called
	/// everytime the active scene is changed (i.e. When
	/// Application.setActiveScene is called). It is not called when the game is
	/// exited.
	/// </summary>
	void deactivate();

	/// <summary>
	/// Handles messages that are sent from the clients.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The data sent with a network message.
	/// </param>
	/// </summary>
	std::string handleNetworkMessages(RakNet::Packet* a_p_Packet);

private:

	/*-------------------------------------------------------------------------
							PRIVATE MEMBER VARIABLES
	-------------------------------------------------------------------------*/

	/// <summary>
	/// The player scores for the game.
	/// </summary>
	std::vector<PlayerScores>		m_vPlayerScores;

	/// <summary>
	/// Determines whether all server is waiting for the clients to send their
	/// scores.
	/// </summary>
	bool							m_bWaitingForScores;

	/// <summary>
	/// Determines whether a new game should be started.
	/// </summary>
	bool							m_bStartNextGame;

	/// <summary>
	/// The <see cref="TimeManager"/> to manage <see cref="Timers"/>  used for
	/// time-based events.
	/// </summary>
	target::TimeManager*			m_p_TimeManager;

	/// <summary>
	/// Reference to the RakPeerInterface object;
	/// </summary>
	RakNet::RakPeerInterface*		m_p_PeerInter;

	/*-------------------------------------------------------------------------
								PRIVATE METHODS
	-------------------------------------------------------------------------*/

	//#########################################################################
	//						Network message handling

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//					Network Messages sent to client

	/// <summary>
	/// Sends the ID_CLIENT_ALL_PLAYER_SCORES message, which sends all clients
	/// all players scores.
	/// <remarks>
	/// This message is handled in EndScene::handleAllPlayerScores().
	/// </remarks>
	/// </summary>
	void sendAllPlayerScores();

	/// <summary>
	/// Sends the ID_CLIENT_NEW_GAME message, which informs all clients
	/// a new game is going to start.
	/// <remarks>
	/// This message is handled in EndScene::handleNewGame() and
	/// WaitingScene::handleNewGame().
	/// </remarks>
	/// </summary>
	void sendNewGame();

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//					Network Messages received from client

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_SERVER_SEND_SCORE'. The message is sent from a client
	/// providing it's game score info.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The packet of data associated with the message.
	/// </param>
	/// <remarks>
	/// The message is sent from EndScene::sendPlayerScore().
	/// </remarks>
	/// </summary>
	void handlePlayerScore(RakNet::Packet* a_p_Packet);

	//#########################################################################
	//						Time Events.

	/// <summary>
	/// Starts the next game round.
	/// </summary>
	void startNextGame();
};
