/*
file:				Server.h
author:				Brad Cook.
description:		This file contains the definition for the class that
					handles the server-side of the game.
date last modified:	13/01/2017
*/

#include "Server.h"
#include <chrono>

#include "CustomGameMessages.h"
#include "GameInfo.h"

#include <IScene.h>
#include "ServerStartScene.h"
#include "ServerGameplayScene.h"
#include "ServerEndScene.h"

#include <BitStream.h>

#include <iostream>
#include <string>
#include <algorithm>

typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::duration<double> ClockDouble;

Server::Server()
{
	m_p_PeerInter = RakNet::RakPeerInterface::GetInstance();
	// Enter in the numbers 1 to 4 into the IDs. Enter them in backwards (i.e.
	// from 4 to 1) so that 1 is the first one off the rank.
	m_vPlayerIDs.push_back(4);
	m_vPlayerIDs.push_back(3);
	m_vPlayerIDs.push_back(2);
	m_vPlayerIDs.push_back(1);
	m_p_ActiveScene = nullptr;
}

Server::~Server()
{
	shutdown();
}

bool Server::startup()
{
	// Set server to running.
	m_bIsServerRunning = true;

	// Setup the GameInfo.
	GameInfo::create();

	// Startup the server network.

	// Create a networksocket descriptor.
	RakNet::SocketDescriptor sd(PORT, 0);

	// Startup the network.
	m_p_PeerInter->Startup(4, &sd, 1);
	m_p_PeerInter->SetMaximumIncomingConnections(4);

	// Startup the scenes.
	setupScenes();
	setActiveScene("StartScene");

	return true;
}

void Server::run()
{
	// start game loop if successfully initialised
	if (startup()) {

		// variables for timing
		auto prevTime = Clock::now();
		auto currTime = Clock::now();
		double deltaTime = 0;
		unsigned int frames = 0;
		double fpsInterval = 0;

		// loop while game is running
		while (m_bIsServerRunning) {

			// update delta time
			currTime = Clock::now();
			ClockDouble cdDoubleSecs = currTime - prevTime;
			auto dsec = std::chrono::duration_cast<std::chrono::nanoseconds>(currTime - prevTime).count();
			deltaTime = cdDoubleSecs.count();
			prevTime = currTime;

			// update fps every second
			frames++;
			fpsInterval += deltaTime;
			if (fpsInterval >= 1.0f) {
				m_uiFps = frames;
				frames = 0;
				fpsInterval -= 1.0f;
			}

			// Call update
			update(deltaTime);
		}
	}

	// cleanup
	shutdown();
}

void Server::shutdown()
{
	// Shutdown the server.
	if (m_p_PeerInter->IsActive())
	{
		m_p_PeerInter->Shutdown(30, '\000', IMMEDIATE_PRIORITY);
	}

	// Remove the scenes.
	clearScenes();

	// End the GameInfo.
	GameInfo::destroy();
}

void Server::update(double deltaTime)
{
	// Handle network messages first.
	handleNetworkMessages();

	// Run the active scene's update
	std::string sChangeToScene = "";
	sChangeToScene = m_p_ActiveScene->update(deltaTime);

	// If there's a change of scene, do so.
	if (sChangeToScene != "")
	{
		setActiveScene(sChangeToScene.c_str());
	}
}

void Server::quit()
{
	// Set the running to false.
	m_bIsServerRunning = false;
}

void Server::addScene(const char* a_p_cName, IScene* a_p_Scene)
{
	if (m_mScenes.find(a_p_cName) == m_mScenes.end())
	{
		m_mScenes[a_p_cName] = a_p_Scene;
		m_mScenes[a_p_cName]->init(m_p_PeerInter);
	}
	else
	{
		std::cout << "Scene " << a_p_cName << " already exists" << std::endl;
	}
}

void Server::clearScenes()
{
	// Go through and call the destroy functions, then remove the IScene
	// object. Then clear the map.
	for (std::map<std::string, IScene*>::iterator sceneIt = m_mScenes.begin();
	sceneIt != m_mScenes.end(); sceneIt++)
	{
		(*sceneIt).second->destroy();
		delete (*sceneIt).second;
	}
	m_mScenes.clear();
}

bool Server::setActiveScene(const char* a_p_cSceneName)
{
	auto setScene = m_mScenes.find(a_p_cSceneName);

	if (setScene == m_mScenes.end())
		return false;

	if (m_p_ActiveScene != nullptr)
	{
		m_p_ActiveScene->deactivate();
	}
	m_p_ActiveScene = setScene->second;
	m_p_ActiveScene->activate();

	return true;
}

void Server::setupScenes()
{
	addScene("StartScene", new ServerStartScene());
	addScene("GameplayScene", new ServerGameplayScene());
	addScene("EndScene", new ServerEndScene());
}

void Server::handleNetworkMessages()
{
	// Get and go through all packets.
	RakNet::Packet* p_Packet = nullptr;

	for (p_Packet = m_p_PeerInter->Receive(); p_Packet;
	m_p_PeerInter->DeallocatePacket(p_Packet),
		p_Packet = m_p_PeerInter->Receive())
	{
		// Read the RakNet message id and  performs actions based on that.
		switch (p_Packet->data[0])
		{
		//#####################################################################
		// Messages handled at the Server level.
		case ID_NEW_INCOMING_CONNECTION:
		{
			// New client joining.
			handleNewConnection(p_Packet);
			break;
		}
		case ID_DISCONNECTION_NOTIFICATION:
		{
			// Client disconnecting.
			handleDisconnection(p_Packet);
			break;
		}
		case ID_CONNECTION_LOST:
		{
			// Client connection unexpectedly lost.
			handleConnectionLost(p_Packet);
			break;
		}
		case ID_SERVER_REGISTER:
		{
			// Client registering to play
			handleRegister(p_Packet->systemAddress);
			break;
		}
		case ID_SERVER_READY_TO_START:
		{
			// Client ready to start gameplay
			handleReadyToStart(p_Packet->systemAddress);
			break;
		}
		default:
		{
			// Check the active scene's handle network messages
			m_p_ActiveScene->handleNetworkMessages(p_Packet);
			return;
		}
		}
	}
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//				Network Messages handled at Server level.

void Server::handleNewConnection(RakNet::Packet* a_p_Packet)
{
	// Add the connection to the list of connections if the IP address doesn't
	// exist already.
	if (!GameInfo::hasSysAddress(a_p_Packet->systemAddress))
	{
		addNewConnection(a_p_Packet->systemAddress);
	}
	else
	{
		// Already connected, so return.
		return;
	}

	// Now, need to determine whether this client can play in the game or not.
	// If the server is still in the start scene, then they can, otherwise they
	// can't.
	if (m_p_ActiveScene == m_mScenes["StartScene"])  // can join game
	{
		sendConnectionInfoToClient(a_p_Packet->systemAddress,
			m_uiAssignedPlayerID, true);
	}
	else  // can't join game.
	{
		sendConnectionInfoToClient(a_p_Packet->systemAddress,
			m_uiAssignedPlayerID, false);
	}
}

void Server::addNewConnection(RakNet::SystemAddress a_SystemAddr)
{
	// Get the next available player ID.
	m_uiAssignedPlayerID = m_vPlayerIDs.back();
	m_vPlayerIDs.pop_back();
	GameInfo::addNewConnection(a_SystemAddr, m_uiAssignedPlayerID);

	// Send info to screen.
	std::cout << "New player has joined the server. Player number: ";
	std::cout << m_uiAssignedPlayerID << std::endl << std::endl;
}

void Server::handleDisconnection(RakNet::Packet* a_p_Packet)
{
	// Remove the player form the list.
	disconnectPlayer(a_p_Packet->systemAddress);
}

void Server::handleConnectionLost(RakNet::Packet* a_p_Packet)
{
	// Remove the player form the list.
	disconnectPlayer(a_p_Packet->systemAddress);
}

void Server::disconnectPlayer(const RakNet::SystemAddress& a_SystemAddr)
{
	// Add the disconnecting player's ID value back to the pool of available 
	// IDs.
	// Find the connection.
	auto Connection = std::find_if(
		GameInfo::getInstance()->m_vConnectedPlayers.begin(),
		GameInfo::getInstance()->m_vConnectedPlayers.end(),
		FindSysAddress(a_SystemAddr));

	// If found, get it's connection ID and add it back to the pool of available
	// values. Then, sort the vector so that the lowest value is at the end of
	// the vector.
	if (Connection != GameInfo::getInstance()->m_vConnectedPlayers.end())
	{
		// Get ID
		unsigned int uiConnectID = (*Connection).m_uiConnectionID;
		m_vPlayerIDs.push_back(uiConnectID);

		// Print info to the screen.
		std::cout << "Player " << uiConnectID << " has disconnected.";
		std::cout << std::endl << std::endl;

		// Sort
		std::sort(m_vPlayerIDs.begin(), m_vPlayerIDs.end());
		std::reverse(m_vPlayerIDs.begin(), m_vPlayerIDs.end());
	}

	// Remove the connection.
	GameInfo::removeConnection(a_SystemAddr);

	// Call the active scene's handlePlayerDisconnected
	m_p_ActiveScene->handlePlayerDisconnected(a_SystemAddr);
}

void Server::handleRegister(const RakNet::SystemAddress& a_SystemAddr)
{
	// Increment the registered players.
	GameInfo::addRegistered(a_SystemAddr);
}

void Server::handleReadyToStart(const RakNet::SystemAddress& a_SystemAddr)
{
	// Record the system address of the sender.
	GameInfo::addPlayerReady(a_SystemAddr);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//				Network Messages sent at Server level.

void Server::sendConnectionInfoToClient(RakNet::SystemAddress a_SystemAddr,
	unsigned int a_uiID,
	bool a_bCanJoinGame)
{
	// Write the network message and send it.
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_CLIENT_CONNECT_INFO));
	bs.Write(a_uiID);
	bs.Write(a_bCanJoinGame);
	m_p_PeerInter->Send(&bs, HIGH_PRIORITY, RELIABLE, 0, a_SystemAddr, false);
}