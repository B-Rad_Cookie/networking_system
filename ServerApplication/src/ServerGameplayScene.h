/*
file:				ServerGameplayScene.h
author:				Brad Cook.
description:		This file contains the declaration of the ServerGameplayScene
					class: The scene that controls the main gameplay on the
					server side.
date last modified:	15/01/2017
*/

#pragma once

#include "IScene.h"
#include "Server.h"
#include "Enums.h"
#include <TimeManager.h>

#include <vector>

struct ServerTarget;
struct ServerCrosshair;
struct Circle;

/// <summary>
/// The scene that is used during the main gameplay. It controls the creation,
/// locations, types and  destruction of the targets in the game. On the first
/// round before starting a game, it first checks that all players are ready.
/// Only when all players are ready will a game actually start.
/// </summary>
class ServerGameplayScene : public IScene
{
public:
	/// <summary>
	/// Default Constructor. Returns a new ServerGameplayScene instance. To set up
	/// the scene, call the init() function.
	/// <returns>
	/// returns ServerGameplayScene instance.
	/// </returns>
	/// </summary>
	ServerGameplayScene() {}

	/// <summary>
	/// Sets up all the elements required by the scene. This function is called
	/// when the game is first loaded.
	/// <param name="a_p_PeerInter">
	/// @param a_p_PeerInter: The RakNet::RakPeerInterface object in use for
	/// the server.
	/// </param>
	/// </summary>
	bool init(RakNet::RakPeerInterface* a_p_PeerInter);

	/// <summary>
	/// Deletes and frees up up all the elements required by the scene. This
	/// function is called when the game is last closed.
	/// </summary>
	void destroy();

	/// <summary>
	/// Updates all the relevant information (elements) in the
	/// scene per frame.
	/// <param name="dt">
	/// @param dt: Delta time, the time (in seconds) between frames executing.
	/// </param>
	/// <returns>
	/// Returns the name of the scene to change to.
	/// </returns>
	/// </summary>
	std::string update(double dt);

	/// <summary>
	/// The update function that is run when the gameplay status is in the
	/// Waiting gameplay state.
	/// <param name="dt">
	/// @param dt: Delta time, the time (in seconds) between frames executing.
	/// </param>
	/// <returns>
	/// Returns the name of the scene to change to.
	/// </returns>
	/// </summary>
	std::string updateWaiting(double dt);

	/// <summary>
	/// The update function that is run when the gameplay status is in the
	/// Setup gameplay state.
	/// <param name="dt">
	/// @param dt: Delta time, the time (in seconds) between frames executing.
	/// </param>
	/// <returns>
	/// Returns the name of the scene to change to.
	/// </returns>
	/// </summary>
	std::string updateSetup(double dt);

	/// <summary>
	/// The update function that is run when the gameplay status is in the
	/// Starting gameplay state.
	/// <param name="dt">
	/// @param dt: Delta time, the time (in seconds) between frames executing.
	/// </param>
	/// <returns>
	/// Returns the name of the scene to change to.
	/// </returns>
	/// </summary>
	std::string updateStarting(double dt);

	/// <summary>
	/// The update function that is run when the gameplay status is in the
	/// Playing gameplay state.
	/// <param name="dt">
	/// @param dt: Delta time, the time (in seconds) between frames executing.
	/// </param>
	/// <returns>
	/// Returns the name of the scene to change to.
	/// </returns>
	/// </summary>
	std::string updatePlaying(double dt);

	/// <summary>
	/// The update function that is run when the gameplay status is in the
	/// EndRound gameplay state.
	/// <param name="dt">
	/// @param dt: Delta time, the time (in seconds) between frames executing.
	/// </param>
	/// <returns>
	/// Returns the name of the scene to change to.
	/// </returns>
	/// </summary>
	std::string updateEndRound(double dt);

	/// <summary>
	/// Sets up all the elements in the scene. This function is called everytime
	/// the scene is set as the active scene (i.e. When
	/// Application.setActiveScene is called). It is not called when the game is
	/// initially loaded.
	/// </summary>
	void activate();

	/// <summary>
	/// Cleans up all the elements in the scene. This function is called
	/// everytime the active scene is changed (i.e. When
	/// Application.setActiveScene is called). It is not called when the game is
	/// exited.
	/// </summary>
	void deactivate();

	/// <summary>
	/// Handles network messages. Receives the network messages and sends the
	/// information to the relevant function.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The data sent with a network message.
	/// </param>
	/// <returns>
	/// Resturns: the name of the scene to change to if any. Returns "" if
	/// no scene is required.
	/// </returns>
	/// </summary>
	std::string handleNetworkMessages(RakNet::Packet* a_p_Packet);

	/// <summary>
	/// Called when a player has disconnected from the server. All logic
	/// relating to handling a player disconnecting is done here.
	/// <param name="a_SystemAddr">
	/// @param a_SystemAddr: The system address of the player that has
	/// disconnected.
	/// </param>
	/// </summary>
	void handlePlayerDisconnected(const RakNet::SystemAddress& a_SystemAddr);

private:
	
	/*-------------------------------------------------------------------------
							PRIVATE MEMBER VARIABLES
	-------------------------------------------------------------------------*/

	/// <summary>
	/// The targets that are currently in play.
	/// </summary>
	std::vector<ServerTarget*>			m_vTargets;

	/// <summary>
	/// Records the system addresses of the players who have been registered.
	/// </summary>
	std::vector<RakNet::SystemAddress>	m_vPlayersRegistered;

	/// <summary>
	/// The crosshairs of all the players.
	/// </summary>
	std::vector<ServerCrosshair*>		m_vCrosshairs;

	/// <summary>
	/// The ID to give the next target. The ID  assigned to every target will
	/// be unique. Therefore any logic can assume that the target ID will be
	/// unique.
	/// </summary>
	unsigned int						m_uiTargetID;
	
	/// <summary>
	/// The state of gameplay
	/// </summary>
	GameplayStatus						m_eGameplayStatus;

	/// <summary>
	/// The current gameplay round number
	/// </summary>
	unsigned int						m_uiRoundNumber;

	/// <summary>
	/// How many players have loaded all the initial setup
	/// </summary>
	unsigned int						m_uiPlayersLoadedData;

	/// <summary>
	/// How many players have loaded all the initial setup
	/// </summary>
	unsigned int						m_uiPlayersUpdatedMovement;

	/// <summary>
	/// How many players have successfully got to the end of the round.
	/// </summary>
	unsigned int						m_uiPlayersRoundEnd;

	/// <summary>
	/// Signfies that the game has ended.
	/// </summary>
	bool								m_bGameEnd;

	/// <summary>
	/// The <see cref="TimeManager"/> to manage <see cref="Timers"/>  used for
	/// time-based events.
	/// </summary>
	target::TimeManager*				m_p_TimeManager;

	/// <summary>
	/// Reference to the RakPeerInterface object;
	/// </summary>
	RakNet::RakPeerInterface*			m_p_PeerInter;

	/*-------------------------------------------------------------------------
								PRIVATE METHODS
	-------------------------------------------------------------------------*/

	//#########################################################################
	//								Game setup

	/// <summary>
	/// Performs all the neccessary setup for the game. This includes creating
	/// the initial targets and setting the crosshairs into place.
	/// </summary>
	void setupGame();

	/// <summary>
	/// Performs all the tasks required when the server changes to preparing to
	/// start a game.
	/// </summary>
	void gameStarting();

	//#########################################################################
	//								Entities

	/// <summary>
	/// Adds the given number of new random targets to the scene.
	/// <param name="a_uiNum">
	/// @param a_uiNum: The number of targets to create.
	/// </param>
	/// </summary>
	void addTargets(unsigned int a_uiNum);

	/// <summary>
	/// Determines whether the given circle overlaps with any of the other
	/// targets.
	/// <param name="a_p_Circle">
	/// @param a_p_Circle: The circle to test.
	/// </param>
	/// <returns>
	/// Returns true if the circle does overlap with any other target, false
	/// otherwise.
	/// </returns>
	/// </summary>
	bool isOverlapping(const Circle* a_p_Circle);

	/// <summary>
	/// Sets up the cross hairs at the start of the match.
	/// </summary>
	void setupCrosshairs();

	/// <summary>
	/// Removes a target from the targets when it's time has finished up.
	/// <param name="a_iID">
	/// @param a_iID: The ID of the target to remove.
	/// </param>
	/// </summary>
	void removeTarget(int a_iID);

	/// <summary>
	/// Clears all the game entities.
	/// </summary>
	void clearEntities();

	//#########################################################################
	//						Network message handling

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//					Network Messages sent to clients

	/// <summary>
	/// Sends the ID_CLIENT_SETUP_GAME message, which informs all clients
	/// that the gameplay is being set up.
	/// <remarks>
	/// This message is handled in Gameplay::handleGameSetup().
	/// </remarks>
	/// </summary>
	void sendGameSetup();

	/// <summary>
	/// Sends the ID_CLIENT_INITIAL_TARGETS message, which sends all clients
	/// the initial targets available to destroy.
	/// <remarks>
	/// This message is handled in Gameplay::handleInitialTargets().
	/// </remarks>
	/// </summary>
	void sendInitialTargets();

	/// <summary>
	/// Writes a <see cref="ServerTarget"/> to a RakNet Bitstream so to send as
	/// a network message.
	/// <param name="a_bs>
	/// @param a_bs: The RakNet::Bitstream object to write the values to.
	/// </param>
	/// <param name="a_p_Target>
	/// @param a_p_Target: The <see cref="ServerTarget"/> object to write to
	/// the bitstream.
	/// </param>
	/// <remarks>
	/// It sends the information to Gameplay::readTargetFromsMessage()
	/// </remarks>
	/// </summary>
	void writeTargetToMessage(RakNet::BitStream& a_bs,
		const ServerTarget* a_p_Target);

	/// <summary>
	/// Sends the ID_CLIENT_CROSSHAIRS message, which sends all clients
	/// the crosshairs
	/// <remarks>
	/// This message is handled in Gameplay::handleCrosshairs().
	/// </remarks>
	/// </summary>
	void sendCrosshairs();

	/// <summary>
	/// Sends the ID_CLIENT_ROUND_START message, which informs all clients
	/// a round of gameplay has started.
	/// <remarks>
	/// This message is handled in Gameplay::handleRoundStart().
	/// </remarks>
	/// </summary>
	void sendRoundStart();

	/// <summary>
	/// Sends the ID_CLIENT_UPDATE_CROSSHAIRS message, which sends all clients
	/// updated crosshair movement info.
	/// <remarks>
	/// This message is handled in Gameplay::handleUpdateCrosshairs().
	/// </remarks>
	/// </summary>
	void sendUpdateCrosshairs();

	/// <summary>
	/// Sends the ID_CLIENT_TARGET_DESTROYED message, which sends all clients
	/// info on a target that has been destroyed.
	/// <param name="a_uiTargetID">
	/// @param a_uiTargetID: The target that got destroyed.
	/// </param>
	/// <param name="a_uiClientID">
	/// @param a_uiClientID: The player who destroyed it.
	/// </param>
	/// <remarks>
	/// This message is handled in Gameplay::handleTargetDestroyed().
	/// </remarks>
	/// </summary>
	void sendTargetDestroyed(unsigned int a_uiTargetID,
		unsigned int a_uiClientID);

	/// <summary>
	/// Sends the ID_CLIENT_TARGET_REMOVED message, which sends all clients
	/// info on a target that has been removed due to it's timer running out.
	/// <param name="a_uiTargetID">
	/// @param a_uiTargetID: The target that got removed.
	/// </param>
	/// <remarks>
	/// This message is handled in Gameplay::handleTargetRemoved().
	/// </remarks>
	/// </summary>
	void sendTargetRemoved(unsigned int a_uiTargetID);

	/// <summary>
	/// Sends the ID_CLIENT_NEW_TARGETS message, which sends all clients
	/// new targets.
	/// <param name="a_iNumAdded">
	/// @param a_iNumAdded: The number of targets that were added.
	/// </param>
	/// <remarks>
	/// This message is handled in Gameplay::handleNewTargets().
	/// </remarks>
	/// </summary>
	void sendNewTargets(int a_iNumAdded);

	/// <summary>
	/// Sends the ID_CLIENT_REMOVE_PLAYER message, which sends all clients
	/// info on a player who has left the game.
	/// <param name="a_uiClientID">
	/// @param a_uiClientID: The player who left.
	/// </param>
	/// <remarks>
	/// This message is handled in Gameplay::handleRemovePlayer().
	/// </remarks>
	/// </summary>
	void sendRemovePlayer(unsigned int a_uiClientID);

	/// <summary>
	/// Sends the ID_CLIENT_RESTART_ROUND message, which informs all clients
	/// a new round of gameplay is going to be played.
	/// <remarks>
	/// This message is handled in Gameplay::handleRoundRestart().
	/// </remarks>
	/// </summary>
	void sendRoundRestart();

	/// <summary>
	/// Sends the ID_CLIENT_END_ROUND message, which informs all clients
	/// a round of gameplay has ended.
	/// <remarks>
	/// This message is handled in Gameplay::handleRoundEnd().
	/// </remarks>
	/// </summary>
	void sendRoundEnd();

	/// <summary>
	/// Sends the ID_CLIENT_END_GAME message, which informs all clients
	/// the game has ended.
	/// <remarks>
	/// This message is handled in Gameplay::handleGameEnd().
	/// </remarks>
	/// </summary>
	void sendGameEnd();

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//				Network Messages received from clients

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_SERVER_INITIAL_DATA_LOADED'. The message is sent from a client
	/// informaing the server they have loaded all the initial data and
	/// completed setup.
	/// <remarks>
	/// The message is sent from Gameplay::sendDataLoaded().
	/// </remarks>
	/// </summary>
	void handleDataLoaded();

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_SERVER_MOVEMENT_INFO'. The message is sent from a client
	/// sending an update on their movement info.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The packet of data associated with the message.
	/// </param>
	/// <remarks>
	/// The message is sent from Gameplay::sendMovementInfo().
	/// </remarks>
	/// </summary>
	void handleMovementInfo(RakNet::Packet* a_p_Packet);

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_SERVER_CHECK_DESTROY_TARGET'. The message is sent from a client
	/// inquiring as to whether they have hit a target or not.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The packet of data associated with the message.
	/// </param>
	/// <remarks>
	/// The message is sent from Gameplay::sendCheckDestroyTarget().
	/// </remarks>
	/// </summary>
	void handleCheckDestroyTarget(RakNet::Packet* a_p_Packet);

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_SERVER_AT_END_ROUND'. The message is sent from a client
	/// informaing the server they have reached the end of round.
	/// <remarks>
	/// The message is sent from Gameplay::sendAtEndRound().
	/// </remarks>
	/// </summary>
	void handleAtEndRound();

	//#########################################################################
	//						Time based events handling

	/// <summary>
	/// Starts a round of gameplay.
	/// </summary>
	void startRound();

	/// <summary>
	/// Ends a round of gameplay.
	/// </summary>
	void endRound();
};
