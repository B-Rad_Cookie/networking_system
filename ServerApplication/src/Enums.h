/*
file:				Consts.h
author:				Brad Cook.
description:		This file contains the definitions of enum types used
					throughout the applications.
date last modified:	15/01/2017
*/

#pragma once

/// <summary>
/// Defines the types of targets that will appear in the game. They are defined
/// by their colour type.
/// </summary>
enum TargetType
{
	Red,
	Blue,
	Green
};

/// <summary>
/// Defines enum values based on whether the game is going to start or not.
/// When the client opens a new game, the server will not start a game until
/// there are at least 2 players. If a client joins without there being any
/// other players, then they will need to wait until other player join. These
/// enum values can be used to determine between these two states.
/// </summary>
enum GameStartStatus
{
	GameWaiting,
	GameStarting,
	GameStarted
};

/// <summary>
/// An enum defining the gameplay status of the game. These values can be used
/// to distinguish between the various states of gameplay.
/// </summary>
enum GameplayStatus
{
	Waiting,
	Setup,
	Starting,
	Playing,
	EndRound
};
