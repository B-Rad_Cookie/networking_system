/*
file:				ServerTarget.h
author:				Brad Cook.
description:		This file contains the declaration of the ServerTarget
					struct: The representation of the targets to destroy in the
					game on the server side.
date last modified:	15/01/2017
*/

#pragma once

#include "NetworkObject.h"
#include "Enums.h"
#include <glm\glm.hpp>


struct Circle;

/// <summary>
/// Represent a descructible target on the server side of the application.
/// Information on targets is kept on the server side so the server can
/// complete it's required tasks. But the information kept on the server is
/// different than what is kept on the client side (e.g. textures) as the
/// requirements are different.
/// </summary>
struct ServerTarget : public NetworkObject
{
	/// <summary>
	/// Default Constructor. Returns a new ServerTarget object with it's
	/// variables set to null or zero.
	/// <returns>
	/// Returns a new ServerTarget object.
	/// </returns>
	/// </summary>
	ServerTarget();

	/// <summary>
	/// Constructor. Returns a new ServerTarget object with it's variables set to
	/// the passed in values.
	/// <param name="a_ID">
	/// @param a_ID: The ID value of the object.
	/// </param>
	/// <param name="a_p_Circle">
	/// @param a_p_Circle: The <see cref="Circle"/> object.
	/// </param>
	/// <param name="a_eType">
	/// @param a_eType: The target type of the target
	/// </param>
	/// <returns>
	/// Returns a new ServerTargetObject.
	/// </returns>
	/// </summary>
	ServerTarget(unsigned int a_ID, Circle* a_p_Circle, TargetType a_eType);

	/// <summary>
	/// Destructor. Cleans up any necessary memory.
	/// </summary>
	~ServerTarget();

	/*-------------------------------------------------------------------------
							PUBLIC MEMBER VARIABLES
	-------------------------------------------------------------------------*/

	/// <summary>
	/// The type of the target.
	/// </summary>
	TargetType				m_eType;

	/// <summary>
	/// The circle geometry for the target.
	/// </summary>
	Circle*					m_p_Circle;

	/// <summary>
	/// The time, in seconds, that the target will remain on the screen before
	/// disappearing.
	/// </summary>
	float					m_fDuration;
};
