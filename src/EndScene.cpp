/*
file:				EndScene.cpp
author:				Brad Cook.
description:		This file contains the definition of the EndScene class:
					The scene that is shown at the end of the game.
date last modified:	15/01/2017
*/

#include "EndScene.h"

#include "ClientInfo.h"
#include <CustomGameMessages.h>

#include <RakPeerInterface.h>
#include <MessageIdentifiers.h>
#include <BitStream.h>

#include <algorithm>

bool EndScene::init(RakNet::RakPeerInterface* a_p_PeerInter)
{
	// Get the peer interface reference.
	m_p_PeerInter = a_p_PeerInter;

	// Setup the font.
	m_p_DisplayFont = new aie::Font("./font/consolas.ttf", 24);

	return true;
}

void EndScene::destroy()
{
	// delete the font.
	delete m_p_DisplayFont;
}

std::string EndScene::update(double dt)
{
	// If new game starting, switch to beginning.
	if (m_bStartNewGame == true)
	{
		return "StartScene";
	}
	return "";
}

void EndScene::draw()
{
	// Get the renderer and window widths and height.
	aie::Renderer2D* p_Renderer = ClientInfo::getRenderer();
	int iWidth = ClientInfo::getWindowWidth();
	int iHeight = ClientInfo::getWindowHeight();

	// Draw the Results header.
	p_Renderer->drawText(m_p_DisplayFont,
		"Results",
		((float)iWidth / 2.0f) - 20.0f,
		(float)iHeight - 100.0f);

	// Draw the player's number
	// Set the render colour to the player's colour
	glm::vec4 Colour = ClientInfo::getPlayerColour();
	p_Renderer->setRenderColour(Colour.r, Colour.g, Colour.b, Colour.a);

	std::string sPlayerNum = std::string(
		"You are Player " + std::to_string(ClientInfo::getClientID()));
	p_Renderer->drawText(m_p_DisplayFont,
		sPlayerNum.c_str(),
		((float)iWidth / 2.0f) - 70.0f,
		(float)iHeight - 200.0f);

	// Set renderer back to default (white)
	p_Renderer->setRenderColour(1.0f, 1.0f, 1.0f);

	// If you have the scores, print them.
	if (m_vPlayerScores.size() > 0)  // scores loaded.
	{
		float fGapBetweenScores = 60.0f;
		float fHeightPosition = (float)iHeight - 350.0f;
		for (int i = 0; i < m_vPlayerScores.size(); i++)
		{
			// Set the render colour based on the player number.
			unsigned int uiPlayerNum = m_vPlayerScores[i].m_uiPlayerNumber;
			switch (uiPlayerNum)
			{
			case 1:  // Red
			{
				p_Renderer->setRenderColour(1.0f, 0.0f, 0.0f);
				break;
			}
			case 2:  // Blue
			{
				p_Renderer->setRenderColour(0.01f, 0.2f, 1.0f);
				break;
			}
			case 3:  // Green
			{
				p_Renderer->setRenderColour(0.0f, 0.7f, 0.0f);
				break;
			}
			case 4:  // Yellow
			{
				p_Renderer->setRenderColour(1.0f, 0.95f, 0.0f);
				break;
			}
			}

			// Print the info.
			std::string sPlayerInfo = std::string(
				"Player " + std::to_string(uiPlayerNum) +
				"                          " +
				std::to_string(m_vPlayerScores[i].m_uiPlayerScore));

			p_Renderer->drawText(m_p_DisplayFont,
				sPlayerInfo.c_str(),
				((float)iWidth / 2.0f) - 250.0f,
				fHeightPosition);

			// Set the position for the next score.
			fHeightPosition -= fGapBetweenScores;
		}

		// Set renderer back to default (white)
		p_Renderer->setRenderColour(1.0f, 1.0f, 1.0f);
	}
	else  // No scores yet.
	{
		// A waiting for scores.
		p_Renderer->drawText(m_p_DisplayFont,
			"Waiting for scores...",
			((float)iWidth / 2.0f) - 100.0f,
			(float)iHeight/ 2.0f);
	}

	p_Renderer->drawText(m_p_DisplayFont,
		"New game starting soon",
		((float)iWidth / 2.0f) - 110.0f, 70.0f);
}

void EndScene::activate()
{
	// Send the player's score info.
	sendPlayerScore();

	// Set new game to false
	m_bStartNewGame = false;
}

void EndScene::deactivate()
{
	// remove the player scores
	m_vPlayerScores.clear();
}

//#########################################################################
//						Network message handling

std::string EndScene::handleNetworkMessages(RakNet::Packet* a_p_Packet)
{
	// Set the return string to nothing ("")
	std::string sReturnString = "";

	switch (a_p_Packet->data[0])
	{
	case ID_CLIENT_ALL_PLAYER_SCORES:
	{
		// Client sending all scores
		handleAllPlayerScores(a_p_Packet);
		break;
	}
	case ID_CLIENT_NEW_GAME:
	{
		// New Game starting.
		handleNewGame();
		break;
	}
	}

	// Return the return string.
	return sReturnString;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//					Network Messages received from server

void EndScene::handleAllPlayerScores(RakNet::Packet* a_p_Packet)
{
	// Firstly read in the information.
	RakNet::BitStream bs(a_p_Packet->data, a_p_Packet->length, false);
	bs.IgnoreBytes(sizeof(RakNet::MessageID));
	int iNumOfScores;
	bs.Read(iNumOfScores);

	// Read all the scores and add them to the list.
	for (int i = 0; i < iNumOfScores; i++)
	{
		unsigned int uiPlayerNum;
		unsigned int uiPlayerScore;
		bs.Read(uiPlayerNum);
		bs.Read(uiPlayerScore);

		PlayerScores player;
		player.m_uiPlayerNumber = uiPlayerNum;
		player.m_uiPlayerScore = uiPlayerScore;
		m_vPlayerScores.push_back(player);
	}

	// sort the list.
	std::sort(m_vPlayerScores.begin(), m_vPlayerScores.end(), ScoreSorter());
}

void EndScene::handleNewGame()
{
	m_bStartNewGame = true;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//					Network Messages sent to server

void EndScene::sendPlayerScore()
{
	// Send the ID_CLIENT_END_ROUND message.
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_SERVER_SEND_SCORE));

	// Get the score info and the player ID and send the info.
	bs.Write(ClientInfo::getClientID());
	bs.Write(ClientInfo::getPlayerScore());

	m_p_PeerInter->Send(&bs, IMMEDIATE_PRIORITY, RELIABLE, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}
