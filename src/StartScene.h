/*
file:				StartScene.h
author:				Brad Cook.
description:		This file contains the declaration of the StartScene class:
					The scene that is shown at the start of the game.
date last modified:	15/01/2017
*/

#pragma once

#include "IScene.h"
#include <vector>
#include <string>

#include <Font.h>
#include "TimeManager.h"
#include <Enums.h>

#include <RakPeerInterface.h>
#include <MessageIdentifiers.h>
#include <BitStream.h>

/// <summary>
/// The scene that is used at the start of the game. When the game is first
/// opened, this is the scene that is set. This scene is used as the waiting
/// area for players to join the game. A message is sent to the server
/// notifying that a new player has joined. It will then wait to start the game
/// when the server tells it.
/// </summary>
class StartScene : public IScene
{
public:
	/// <summary>
	/// Default Constructor. Returns a new StartScene instance. To set up the
	/// scene, call the init() function.
	/// <returns>
	/// returns StartScene instance.
	/// </returns>
	/// </summary>
	StartScene() {}

	/*-------------------------------------------------------------------------
								PUBLIC METHODS
	-------------------------------------------------------------------------*/

	/// <summary>
	/// Sets up all the elements required by the scene. This function is called
	/// when the game is first loaded.
	/// <param name="a_p_PeerInter">
	/// @param a_p_PeerInter: The RakNet::RakPeerInterface object in use for
	/// the server.
	/// </param>
	/// </summary>
	bool init(RakNet::RakPeerInterface* a_p_PeerInter);

	/// <summary>
	/// Deletes and frees up up all the elements required by the scene. This
	/// function is called when the game is last closed.
	/// </summary>
	void destroy();

	/// <summary>
	/// Updates all the relevant information (elements) in the
	/// scene per frame.
	/// <param name="dt">
	/// @param dt: Delta time, the time (in seconds) between frames executing.
	/// </param>
	/// <returns>
	/// Returns the name of the scene to change to.
	/// </returns>
	/// </summary>
	std::string update(double dt);

	/// <summary>
	/// Draws the entities and information in the scene (i.e. sends the
	/// graphics to GPU).
	/// </summary>
	void draw();

	/// <summary>
	/// Sets up all the elements in the scene. This function is called everytime
	/// the scene is set as the active scene (i.e. When
	/// Application.setActiveScene is called). It is not called when the game is
	/// initially loaded.
	/// </summary>
	void activate();

	/// <summary>
	/// Cleans up all the elements in the scene. This function is called
	/// everytime the active scene is changed (i.e. When
	/// Application.setActiveScene is called). It is not called when the game is
	/// exited.
	/// </summary>
	void deactivate();

	/// <summary>
	/// Handles network messages. Receives the network messages and sends the
	/// information to the relevant function.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The data sent with a network message.
	/// </param>
	/// <returns>
	/// Resturns: the name of the scene to change to if any. Returns "" if
	/// no scene is required.
	/// </returns>
	/// </summary>
	std::string handleNetworkMessages(RakNet::Packet* a_p_Packet);

private:

	/*-------------------------------------------------------------------------
							PRIVATE MEMBER VARIABLES
	-------------------------------------------------------------------------*/

	/// <summary>
	/// The <see cref="aie::Font"/> object font used to display text on the
	/// screen.
	/// </summary>
	aie::Font*					m_p_DisplayFont;

	/// <summary>
	/// Determines whether the game is going to start or not (see
	/// <see cref="GameStartStatus"/>).
	/// <remarks>
	/// An enum was chosen over another IScene because it would be excessive to
	/// have two separate IScenes for something this trivial.
	/// </remarks>
	/// </summary>
	GameStartStatus				m_eStartStatus;

	/// <summary>
	/// Determines whether the client has registered to play or not.
	/// </summary>
	bool						m_bHasRegistered;

	/// <summary>
	/// The <see cref="TimeManager"/> to manage <see cref="Timers"/>  used for
	/// time-based events.
	/// </summary>
	target::TimeManager*		m_p_TimeManager;

	/// <summary>
	/// Reference to the RakPeerInterface object;
	/// </summary>
	RakNet::RakPeerInterface*	m_p_PeerInter;

	/*-------------------------------------------------------------------------
								PRIVATE METHODS
	-------------------------------------------------------------------------*/

	/// <summary>
	/// Draws all the relevant information when the player has successfully
	/// connected to the server to the screen.
	/// <param name="a_p_2dRenderer">
	/// @param a_p_2dRenderer: The <see cref="aie::2dRenderer"/> object used to
	/// draw the information to the screen.
	/// </param>
	/// <param name="a_iWidth">
	/// @param a_iWidth: The width of the window.
	/// </param>
	/// <param name="a_iHeight">
	/// @param a_iHeight: The height of the window.
	/// </param>
	/// </summary>
	void drawConnectedInfo(aie::Renderer2D* a_p_2dRenderer,
		int a_iWidth, int a_iHeight);

	/// <summary>
	/// Starts the game.
	/// </summary>
	void startGame();

	//#########################################################################
	//						Network message handling

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//					Network Messages received from Server

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_CLIENT_START_COUNTDOWN'. The message is sent from the server
	/// informing to start the countdown to the game starting.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The packet of data associated with the message.
	/// </param>
	/// <remarks>
	/// The message is sent from ServerStartScene::sendStartCountdown().
	/// </remarks>
	/// </summary>
	void handleStartCountdown(RakNet::Packet* a_p_Packet);

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_CLIENT_START_COUNTDOWN_ONE'. The message is sent from the server
	/// informing a particular client to start the countdown to the game
	/// starting.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The packet of data associated with the message.
	/// </param>
	/// <remarks>
	/// The message is sent from ServerStartScene::sendStartCountdownOne().
	/// </remarks>
	/// </summary>
	void handleStartCountdownOne(RakNet::Packet* a_p_Packet);

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_CLIENT_STOP_COUNTDOWN'. The message is sent from the server
	/// informing to start the countdown to the game starting.
	/// <remarks>
	/// The message is sent from ServerStartScene::sendStopCountdown().
	/// </remarks>
	/// </summary>
	void handleStopCountdown();

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//					Network Messages sent to Server

	/// <summary>
	/// Sends the ID_SERVER_REGISTER message to the server, which informs
	/// the server that this client wants to register for a game.
	/// <remarks>
	/// This message is handled by Server::handleRegister().
	/// </remarks>
	/// </summary>
	void sendRegister();
};
