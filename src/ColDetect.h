/*
file:				ColDetect.h
author:				Brad Cook.
description:		This file contains the declaration of the ColDetect class:
					The utility class which handles collision detection of
					geometry objects.
date last modified:	20/12/2016
*/

#pragma once

#include <glm\glm.hpp>
#include <vector>

struct Circle;

/// <summary>
/// The Collision Detection utility class. Provides methods which determines
/// whether geometries have collided.
/// </summary>
class ColDetect
{
public:
	/// <summary>
	/// Default constructor. As this is a utility class, does nothing.
	/// </summary>
	ColDetect() {};

	/// <summary>
	/// Destructor. As this is a utility class, does nothing.
	/// </summary>
	~ColDetect() {};

	/// <summary>
	/// Determines whether two circles intersect.
	/// <param name="a_p_Circle1">
	/// @param a_p_Circle1: The first circle to test.
	/// </param>
	/// <param name="a_p_Circle2">
	/// @param a_p_Circle2: The second circle to test.
	/// </param>
	/// <returns>
	/// Returns true if the point falls within the circle, returns false if the
	/// point falls outside of the circle.
	/// </returns>
	/// </summary>
	static bool AreColliding(const Circle* a_p_Circle1,
		const Circle* a_p_Circle2);

	/// <summary>
	/// Determines whether a point falls within a circle.
	/// <param name="a_Point">
	/// @param a_Point: The point (represented as a glm::vec3) to test.
	/// </param>
	/// <param name="a_p_Circle">
	/// @param a_p_Circle: The circle to test.
	/// </param>
	/// <returns>
	/// Returns true if the point falls within the circle, returns false if the
	/// point falls outside of the circle.
	/// </returns>
	/// </summary>
	static bool AreColliding(const glm::vec3 &a_Point,
		const Circle* a_p_Circle);
};
