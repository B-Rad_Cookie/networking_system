/*
file:				ClientInfo.h
author:				Brad Cook.
description:		This file contains the declaration of the ClientInfo class:
					A class which contains all the information required by the
					client for the game.
date last modified:	11/01/2017
*/

#pragma once

#include <Renderer2D.h>
#include <glm\glm.hpp>

/// <summary>
/// Represents the different connection statuses of a client.
/// </summary>
enum ConnectionStatus
{
	AttemptingConnection,
	Connected,
	ConnectionFailed,
	ServerFull,
	Disconnected
};

/// <summary>
/// Contains all information that is required throughout various parts of the
/// game by the client. It acts similarly to a blackboard.
/// <remarks>
/// This class is setup as a singleton.
/// </remarks>
/// </summary>
class ClientInfo
{
public:

	/*-------------------------------------------------------------------------
								PUBLIC METHODS
	-------------------------------------------------------------------------*/

	/// <summary>
	/// Creates the singleton instance.
	/// </summary>
	static void create();

	/// <summary>
	/// Deletes the singleton instance.
	/// </summary>
	static void destroy();

	/// <summary>
	/// Returns a reference to the singleton instance.
	/// <returns>
	/// Returns: const GameInfo* reference to the singleton.
	/// </returns>
	/// </summary>
	static const ClientInfo* getInstance() { return p_Singleton; }

	/// <summary>
	/// Set the client ID value.
	/// <param name="a_uiID">
	/// @param a_uiID: The new value to set the client ID to.
	/// </param>
	/// </summary>
	static void setClientID(unsigned int a_uiID)
	{
		p_Singleton->m_uiClientID = a_uiID;
	}
	
	/// <summary>
	/// Get the client ID value.
	/// </summary>
	static unsigned int getClientID()
	{
		return p_Singleton->m_uiClientID;
	}

	/// <summary>
	/// Set the player's colour. Colour represented in RGBA form.
	/// <param name="a_PlayerColour">
	/// @param a_PlayerColour: The new value to set the colour to.
	/// </param>
	/// </summary>
	static void setPlayerColour(const glm::vec4& a_PlayerColour)
	{
		p_Singleton->m_PlayerColour = a_PlayerColour;
	}

	/// <summary>
	/// Get the player's colour. Colour represented in RGBA form.
	/// </summary>
	static const glm::vec4 getPlayerColour()
	{
		return p_Singleton->m_PlayerColour;
	}

	/// <summary>
	/// Set the player score value.
	/// <param name="a_uiScore">
	/// @param a_uiScore: The new value to set the player score to.
	/// </param>
	/// </summary>
	static void setPlayerScore(unsigned int a_uiScore)
	{
		p_Singleton->m_uiPlayerScore = a_uiScore;
	}

	/// <summary>
	/// Adds the value passed in to the player score value.
	/// <param name="a_uiScore">
	/// @param a_uiScore: The score to add to the player score.
	/// </param>
	/// </summary>
	static void addToScore(unsigned int a_uiScore)
	{
		p_Singleton->m_uiPlayerScore += a_uiScore;
	}

	/// <summary>
	/// Get the player score value.
	/// </summary>
	static unsigned int getPlayerScore()
	{
		return p_Singleton->m_uiPlayerScore;
	}

	/// <summary>
	/// Set the window width.
	/// <param name="a_iWindowWidth">
	/// @param a_iWindowWidth: The new value to set window width to.
	/// </param>
	/// </summary>
	static void setWindowWidth(int a_iWindowWidth)
	{
		p_Singleton->m_iWindowWidth = a_iWindowWidth;
	}

	/// <summary>
	/// Get the window width value.
	/// </summary>
	static int getWindowWidth()
	{
		return p_Singleton->m_iWindowWidth;
	}

	/// <summary>
	/// Set the window height.
	/// <param name="a_iWindowHeight">
	/// @param a_iWindowHeight: The new value to set window height to.
	/// </param>
	/// </summary>
	static void setWindowHeight(int a_iWindowHeight)
	{
		p_Singleton->m_iWindowHeight = a_iWindowHeight;
	}

	/// <summary>
	/// Get the window height value.
	/// </summary>
	static int getWindowHeight()
	{
		return p_Singleton->m_iWindowHeight;
	}

	/// <summary>
	/// Get the connection status.
	/// </summary>
	static ConnectionStatus getConnectStatus()
	{
		return p_Singleton->m_eConnectionStatus;
	}

	/// <summary>
	/// Set the connection status
	/// <param name="a_eConnectStatus">
	/// @param a_eConnectStatus: The new value to set the connection status to.
	/// </param>
	/// </summary>
	static void setConnectStatus(ConnectionStatus a_eConnectStatus)
	{
		p_Singleton->m_eConnectionStatus = a_eConnectStatus;
	}

	/// <summary>
	/// Set the renderer object. To set the renderer to nullptr, pass in
	/// nullptr;
	/// <param name="a_p_Renderer">
	/// @param a_p_Renderer: The new object to set the renderer to.
	/// </param>
	/// </summary>
	static void setRenderer(aie::Renderer2D* a_p_Renderer);

	/// <summary>
	/// Get the renderer object.
	/// </summary>
	static aie::Renderer2D* getRenderer()
	{
		return p_Singleton->m_p_2dRenderer;
	}

private:

	/*-------------------------------------------------------------------------
							PRIVATE MEMBER VARIABLES
	-------------------------------------------------------------------------*/

	/// <summary>
	/// The singleton instance.
	/// </summary>
	static ClientInfo*	p_Singleton;

	/// <summary>
	/// The ID value for the client.
	/// <summary>
	unsigned int		m_uiClientID;

	/// <summary>
	/// The colour the player (client) crosshair will be. Colour represented in
	/// RGBA form.
	/// </summary>
	glm::vec4			m_PlayerColour;

	/// <summary>
	/// The score of the player.
	/// </summary>
	unsigned int		m_uiPlayerScore;

	/// <summary>
	/// The width of the client window.
	/// </summary>
	int					m_iWindowWidth;

	/// <summary>
	/// The height of the client window.
	/// </summary>
	int					m_iWindowHeight;

	/// <summary>
	/// The connection status of the client to the server.
	/// </summary>
	ConnectionStatus	m_eConnectionStatus;

	/// <summary>
	/// The <see cref="aie::Renderer2D"/> object used for handling the drawing
	/// of objects to the screen. This object is used throughout the draw()
	/// functions of <see cref="Iscene"/> scenes to draw their entities to the
	/// screen.
	/// <remarks>
	/// This object is placed here because this object will be used throughout
	/// the client application. Therefore putting it here means that not all
	/// scenes have to have their own individual ones. It also means that this
	/// can be accessed in BasicNetworkApplication as well so that all
	/// the draw handling functions (e.g. begin() and end()) can be called in
	/// BasicNetworkApplication::draw(), leaving the scenes to only drawing
	/// their entities
	/// </remarks>
	/// </summary>
	aie::Renderer2D*	m_p_2dRenderer;

	/*-------------------------------------------------------------------------
								PRIVATE METHODS
	-------------------------------------------------------------------------*/

	/// <summary>
	/// Default constructor. Creates the <see cref="aie::Renderer2D/> object
	/// and sets the other pointer values to null
	/// </summary>
	ClientInfo();

	/// <summary>
	/// Default destructor. Performs any necessary memory cleaning up
	/// procedures.
	/// </summary>
	~ClientInfo();
};
