/*
file:				Crosshair.h
author:				Brad Cook.
description:		This file contains the declaration of the Crosshair
					struct: The representation of the crosshair the players
					control to destroy targets.
date last modified:	14/01/2017
*/

#pragma once

#include <NetworkObject.h>
#include <Texture.h>

#include <glm\glm.hpp>

/// <summary>
/// The crosshairs players control and move around the game to destroy targets. 
/// </summary>
struct Crosshair : public NetworkObject
{
public:
	/// <summary>
	/// Default constructor. Returns a new Crosshair instance with the member
	/// variables set to 0 or null.
	/// <returns>
	/// returns Crosshair instance.
	/// </returns>
	/// </summary>
	Crosshair();

	/// <summary>
	/// Set constructor. Returns a new Crosshair instance with the member
	/// variables set to the passed in argument values.
	/// <param name="a_uiID">
	/// @param a_uiID: The ID value of the object.
	/// </param>
	/// <param name="a_Pos">
	/// @param a_Pos: The centre point of the crosshair.
	/// </param>
	/// <returns>
	/// returns Crosshair instance.
	/// </returns>
	/// </summary>
	Crosshair(unsigned int a_uiID, const glm::vec3 &a_Pos);

	/// <summary>
	/// Destructor. Cleans up used memory.
	/// </summary>
	~Crosshair();

	/*-------------------------------------------------------------------------
							PUBLIC MEMBER VARIABLES
	-------------------------------------------------------------------------*/

	/// <summary>
	/// The centre-point position of the crosshair.
	/// </summary>
	glm::vec3			m_Position;

	/// <summary>
	/// The velocity of the crosshair
	/// </summary>
	glm::vec3			m_Velocity;

	/*-------------------------------------------------------------------------
								PUBLIC METHODS
	-------------------------------------------------------------------------*/
	/// <summary>
	/// Draws the target to the display (i.e. sends the graphics to GPU).
	/// </summary>
	void draw();

	/// <summary>
	/// Sets up all the target textures  to be used for drawing.
	/// </summary>
	static void setupTextures();

	/// <summary>
	/// Removesall the target textures.
	/// </summary>
	static void destroyTextures();

protected:

	/*-------------------------------------------------------------------------
	PROTECTED MEMBER VARIABLES
	-------------------------------------------------------------------------*/

	/// <summary>
	/// The texture to draw the target with.
	/// </summary>
	aie::Texture*	m_p_Texture;

	/// <summary>
	/// The texture for the red crosshair.
	/// </summary>
	static aie::Texture*		s_p_RedCrosshairTexture;

	/// <summary>
	/// The texture for the blue crosshair.
	/// </summary>
	static aie::Texture*		s_p_BlueCrosshairTexture;

	/// <summary>
	/// The texture for the green crosshair.
	/// </summary>
	static aie::Texture*		s_p_GreenCrosshairTexture;

	/// <summary>
	/// The texture for the yellow crosshair.
	/// </summary>
	static aie::Texture*		s_p_YellowCrosshairTexture;
};

