/*
file:				TimeManager.cpp
author:				Brad Cook.
description:		This file contains the definition for the TimeManager class: which
					handles time and timers in the game.
date last modified:	15/01/2017
*/

#include "TimeManager.h"
#include <math.h>

#pragma once
target::TimeManager::TimeManager()
{
	reset();
}

target::TimeManager::~TimeManager()
{
	clearTimers();
}

void target::TimeManager::reset()
{
	// Set the time back to 0.
	m_dElapsedTime = 0.0f;
}

void target::TimeManager::update(double dt)
{
	m_dElapsedTime += dt;

	// Determine whether any timers have elapsed. If so, run their function.
	// If the timer is a repeatable one, then reset it. If the timer is not a
	// repeating one, remove it from the list.
	std::map<std::string, target::Timer*>::iterator timerIt = m_mTimers.begin();
	while (timerIt != m_mTimers.end())
	{
		if ((*timerIt).second->hasTimeElapsed(m_dElapsedTime))
		{
			// Call the timer function.
			(*timerIt).second->invokeFunction();

			if ((*timerIt).second->isRepeat())
			{
				(*timerIt).second->restart(m_dElapsedTime);
				timerIt++;
			}
			else
			{
				delete (*timerIt).second;
				timerIt = m_mTimers.erase(timerIt);
			}
		}
		else
		{
			timerIt++;
		}
	}
}

const target::Timer* target::TimeManager::addTimer(std::string a_sTimerName,
	const std::function<void(void)>& a_FunctionBinding,
	float a_fTimerDuration, bool a_bRepeat)
{
	// First, see if the timer name already exists. If it does, return nullptr
	// and don't add.
	if (m_mTimers.count(a_sTimerName) == 1)
	{
		return nullptr;
	}

	// Add the new timer.
	target::Timer* p_NewTimer = new target::Timer(
		a_FunctionBinding, (float)m_dElapsedTime, a_fTimerDuration, a_bRepeat);
	m_mTimers[a_sTimerName] = p_NewTimer;
	return p_NewTimer;
}

const target::Timer* target::TimeManager::addTimer(std::string a_sTimerName,
	const std::function<void(int)>& a_FunctionBinding, int a_iArgumentValue,
	float a_fTimerDuration, bool a_bRepeat)
{
	// First, see if the timer name already exists. If it does, return nullptr
	// and don't add.
	if (m_mTimers.count(a_sTimerName) == 1)
	{
		return nullptr;
	}

	// Add the new timer.
	target::Timer* p_NewTimer = new target::Timer(
		a_FunctionBinding, a_iArgumentValue,
		(float)m_dElapsedTime, a_fTimerDuration, a_bRepeat);
	m_mTimers[a_sTimerName] = p_NewTimer;
	return p_NewTimer;
}

bool target::TimeManager::stopTimer(const std::string& a_sTimerName)
{
	// Check if the timer exists. If it doesn't, then return false;
	if (m_mTimers.count(a_sTimerName) == 0)
	{
		return false;
	}

	// remove the timer.
	delete m_mTimers[a_sTimerName];
	m_mTimers.erase(a_sTimerName);
	return true;
}

float target::TimeManager::getTimeRemaining(const std::string& a_sTimerName)
{
	// Check if the timer exists. If it doesn't, then return -1;
	if (m_mTimers.count(a_sTimerName) == 0)
	{
		return -1.0f;
	}

	// Return the time remaining, which is the end time minus the current time.
	return m_mTimers[a_sTimerName]->getEndTime() - m_dElapsedTime;
}

std::string target::TimeManager::getPrintTimeRemaining(
	const std::string& a_sTimerName)
{
	// Check if the timer exists. If it doesn't, then return "";
	if (m_mTimers.count(a_sTimerName) == 0)
	{
		return "";
	}

	// Get the time remaining.
	float fTimeRemaining = getTimeRemaining(a_sTimerName);

	// Convert to the string format.

	// First, find the hours, minutes and seconds.
	int iHours = int(fTimeRemaining / 1200.0f);
	int iMinutes = int(fTimeRemaining / 60.0f);
	float fRemainingSeconds = (
		fTimeRemaining - (iHours * 1200.0f) - (iMinutes * 60.0f));
	int iSeconds = round(fRemainingSeconds);

	// When the remaining seconds is 60, set the seconds to 0 and add one to
	// the minutes so that it appears as 4:00 and instead of 3:60 for example.
	if (iSeconds == 60)
	{
		iSeconds = 0;
		iMinutes++;
	}

	// Build up the string.
	std::string sReturnString = "";
	// Hours
	if (iHours > 0)
	{
		sReturnString.append(std::to_string(iHours));
		sReturnString.append(":");
	}
	// Minutes
	if (iMinutes > 0)
	{
		sReturnString.append(std::to_string(iMinutes));
		sReturnString.append(":");
	}
	// Seconds
	if (iSeconds < 10)  // pad with a 0 if below 10.
	{
		sReturnString.append("0");
	}
	sReturnString.append(std::to_string(iSeconds));
	return sReturnString;
}

void target::TimeManager::clearTimers()
{
	// Go through and remove the memory for the timers, then clear the vector.
	for (std::map<std::string, target::Timer*>::iterator timerIt = m_mTimers.begin();
		timerIt != m_mTimers.end(); timerIt++)
	{
		delete (*timerIt).second;
	}
	m_mTimers.clear();
}
