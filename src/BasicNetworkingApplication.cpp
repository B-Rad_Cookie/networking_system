/*
file:				BasicNetworkingApplication.cpp
author:				Conan Burke, Brad Cook.
description:		This file contains the definition of the client
					application for the game. It handles the logic of the
					client-side of the game system.
date last modified:	18/01/2017
*/

#include "BasicNetworkingApplication.h"

#include "IScene.h"
#include "StagingScene.h"
#include "StartScene.h"
#include "Gameplay.h"
#include "EndScene.h"
#include "WaitingScene.h"

#include "ClientInfo.h"

#include <CustomGameMessages.h>

#include <iostream>
#include <string>

#include <glm\glm.hpp>

#include <RakPeerInterface.h>
#include <MessageIdentifiers.h>
#include <BitStream.h>

BasicNetworkingApplication::BasicNetworkingApplication() {
	m_p_ActiveScene = nullptr;
	m_p_PeerInter = RakNet::RakPeerInterface::GetInstance();
}

BasicNetworkingApplication::~BasicNetworkingApplication() {
	shutdown();
}

bool BasicNetworkingApplication::startup()
{	
	// Setup the clientinfo.
	ClientInfo::create();

	// Set some ClientInfo variables.
	ClientInfo::setWindowWidth(getWindowWidth());
	ClientInfo::setWindowHeight(getWindowHeight());

	// Connect up to the network.
	m_p_PeerInter = RakNet::RakPeerInterface::GetInstance();
	RakNet::SocketDescriptor sd;
	m_p_PeerInter->Startup(1, &sd, 1);
	m_p_PeerInter->Connect("127.0.0.1", PORT, nullptr, 0);

	// Startup the scenes.
	setupScenes();

	return true;
}

void BasicNetworkingApplication::shutdown() {
	// Disconnect from the server.
	if (m_p_PeerInter->IsActive())
	{
		m_p_PeerInter->Shutdown(30, '\000', IMMEDIATE_PRIORITY);
	}

	// Remove the scenes.
	clearScenes();

	// Remove the client info.
	ClientInfo::destroy();
}

void BasicNetworkingApplication::update(double deltaTime) {
	// Handle network messages.
	std::string sChangeToScene = handleNetworkMessages();

	// If there's a change of scene, do so.
	if (sChangeToScene != "")
	{
		setActiveScene(sChangeToScene.c_str());
		return;
	}

	sChangeToScene = m_p_ActiveScene->update(deltaTime);
	// If there's a change of scene, do so.
	if (sChangeToScene != "")
	{
		setActiveScene(sChangeToScene.c_str());
		return;
	}
}

void BasicNetworkingApplication::draw() {
	// Clear the screen.
	clearScreen();

	// Set the renderer to beging drawing.
	aie::Renderer2D* p_Renderer = ClientInfo::getRenderer();
	p_Renderer->begin();

	// call the active scene's draw to add entities.
	m_p_ActiveScene->draw();

	//  End the renderer from drawing.
	p_Renderer->end();
}

void BasicNetworkingApplication::addScene(const char* a_p_cName, IScene* a_p_Scene)
{
	if (m_mScenes.find(a_p_cName) == m_mScenes.end())
	{
		m_mScenes[a_p_cName] = a_p_Scene;
		m_mScenes[a_p_cName]->init(m_p_PeerInter);
	}
	else
	{
		std::cout << "Scene " << a_p_cName << " already exists" << std::endl;
	}
}

void BasicNetworkingApplication::clearScenes()
{
	// Go through and call the destroy functions, then remove the IScene
	// object. Then clear the map.
	for (std::map<std::string, IScene*>::iterator sceneIt = m_mScenes.begin();
	sceneIt != m_mScenes.end(); sceneIt++)
	{
		(*sceneIt).second->destroy();
		delete (*sceneIt).second;
	}
	m_mScenes.clear();
}

bool BasicNetworkingApplication::setActiveScene(const char* a_p_cSceneName)
{
	auto setScene = m_mScenes.find(a_p_cSceneName);

	if (setScene == m_mScenes.end())
		return false;

	if (m_p_ActiveScene != nullptr)
	{
		m_p_ActiveScene->deactivate();
	}
	m_p_ActiveScene = setScene->second;
	m_p_ActiveScene->activate();

	return true;
}


void BasicNetworkingApplication::setupScenes()
{
	addScene("StagingScene", new StagingScene());
	addScene("StartScene", new StartScene());
	addScene("GameplayScene", new Gameplay());
	addScene("EndScene", new EndScene());
	addScene("WaitingScene", new WaitingScene());
	setActiveScene("StagingScene");
}

void BasicNetworkingApplication::quit()
{
	// end the game loop
	m_gameOver = true;
}

std::string BasicNetworkingApplication::handleNetworkMessages()
{
	// Set the return string.
	std::string sReturnString = "";

	// Get and go through all packets.
	RakNet::Packet* p_Packet = nullptr;

	for (p_Packet = m_p_PeerInter->Receive(); p_Packet;
	m_p_PeerInter->DeallocatePacket(p_Packet),
		p_Packet = m_p_PeerInter->Receive())
	{
		// Read the RakNet message id and  performs actions based on that.
		switch (p_Packet->data[0])
		{
			//#####################################################################
			// Messages handled at the Application level.
		case ID_CONNECTION_REQUEST_ACCEPTED:
		{
			// Successfully connected to the server.
			handleConnectionAccepted();
			break;
		}
		case ID_CONNECTION_ATTEMPT_FAILED:
		{
			// Failed to connect to the server.
			handleConnectionFailed();
			break;
		}
		case ID_NO_FREE_INCOMING_CONNECTIONS:
		{
			// Failed to connect to the server because it is full.
			handleNoFreeConnections();
			break;
		}
		case ID_DISCONNECTION_NOTIFICATION:
		{
			// Server shutdown.
			handleDisconnection(p_Packet);
			break;
		}
		case ID_CONNECTION_LOST:
		{
			// Server error
			handleConnectionLost(p_Packet);
			break;
		}
		case ID_CLIENT_CONNECT_INFO:
		{
			//Server sending ID and can participate in a game info.
			return handleClientConnectInfo(p_Packet);
			break;
		}
		default:
		{
			// Check the active scene's handle network messages.
			sReturnString = m_p_ActiveScene->handleNetworkMessages(p_Packet);
		}
		}
	}

	// Don't change the scene if nothing happens
	return sReturnString;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//		Network Messages handled at BaseNetworkingApplication level.

void BasicNetworkingApplication::handleConnectionAccepted()
{
	// Set the connection status to connected.
	ClientInfo::setConnectStatus(ConnectionStatus::Connected);	
}

void BasicNetworkingApplication::handleConnectionFailed()
{
	// Set the connection status to failed.
	ClientInfo::setConnectStatus(ConnectionStatus::ConnectionFailed);
}

void BasicNetworkingApplication::handleNoFreeConnections()
{
	// Set the connection status to full.
	ClientInfo::setConnectStatus(ConnectionStatus::ServerFull);
}

void BasicNetworkingApplication::handleDisconnection(
	RakNet::Packet* a_p_Packet)
{
	// Deal with the server shutting down.
	serverShutdown();
}

void BasicNetworkingApplication::handleConnectionLost(
	RakNet::Packet* a_p_Packet)
{
	// Deal with the server shutting down.
	serverShutdown();
}

void BasicNetworkingApplication::serverShutdown()
{
	// Just quit for now.
	quit();
}

std::string BasicNetworkingApplication::handleClientConnectInfo(
	RakNet::Packet* a_p_Packet)
{
	// Firstly, read the message.
	unsigned int uiID;
	bool bCanJoinGame;
	RakNet::BitStream bs(a_p_Packet->data, a_p_Packet->length, false);
	bs.IgnoreBytes(sizeof(RakNet::MessageID));
	bs.Read(uiID);
	bs.Read(bCanJoinGame);

	// Set the client ID value.
	ClientInfo::setClientID(uiID);

	// Set the colour based on the ID. The colour is assigned as follows:
	// Player 1: Red
	// Player 2: Blue
	// Player 3: Green
	// Player 4: Yellow.
	switch (uiID)
	{
	case 1:  // Red
	{
		ClientInfo::setPlayerColour(glm::vec4(1, 0, 0, 1));
		break; 
	}
	case 2:  // Blue
	{
		ClientInfo::setPlayerColour(glm::vec4(0.01f, 0.2f, 1, 1));
		break;
	}
	case 3:  // Green
	{
		ClientInfo::setPlayerColour(glm::vec4(0, 0.7f, 0, 1));
		break;
	}
	case 4:  // Yellow
	{
		ClientInfo::setPlayerColour(glm::vec4(1, 0.95f, 0, 1));
		break;
	}
	default:
	{
		// Something has gone wrong here.
		break;
	}
	}

	// If the player must wait, switch to the waiting scene.
	if (!bCanJoinGame)
	{
		return "WaitingScene";
	}
	else  // Switch to the StartScene scene
	{
		return "StartScene";
	}
}
