/*
file:				Timer.h
author:				Brad Cook.
description:		This file contains the declaration for the Timer class: 
					which handles and event based on time elapsing.
date last modified:	08/01/2017
*/

#pragma once

#include "NetworkObject.h"
#include <functional>
#include "IScene.h"

namespace target
{

/// <summary>
/// Handles the triggering of events that are based on a certain amount of time
/// elapsing. It will execute a function that is passed into it when a given
/// amount of time has passed. They can also be set to repeat so that the
/// function is executed when the give amount of time elapses repeatedly. The
/// member function must be from an <see cref="IScene"/> object  and must be of
/// the form "void function()" or "void function(int)".
/// <remarks>
/// This class is designed to work in tandem with <see cref="TimeManager"/>. If
/// you are using a <see cref="TimeManager"/> in your application, then you should
/// interface with that class, not this class.
/// </remarks>
/// </summary>
class Timer
{
public:

	/// <summary>
	/// Default Constructor. Creates a new Timer instance. Sets all variables
	/// to null/0/false and does not start the timer.
	/// <returns>
	/// Returns: <see cref="Timer"/> object.
	/// </returns>
	/// </summary>
	Timer();

	/// <summary>
	/// void func() Constructor. Creates a new Timer instance with a
	/// function of the form "void function()".
	/// <param name="a_FunctionBinding">
	/// @param a_FunctionBinding: The std::function binding to call when the
	/// timer finishes. The function must be of the kind void func(). To create
	/// this, you will need to call the std::bind function. For more
	/// information on how to do this, see the examples section in the link 
	/// <see href="http://en.cppreference.com/w/cpp/utility/functional/function">
	/// HERE</see>
	/// </param>
	/// <param name="a_fCurrentTime">
	/// @param a_fCurrentTime: The current time.
	/// </param>
	/// <param name="a_fTimerDuration">
	/// @param a_fTimerDuration: The time (in seconds) to elapse before the
	/// function is called.
	/// </param>
	/// <param name="a_bRepeat">
	/// @param a_bRepeat: Whether the timer should be repeated when time runs
	/// out.
	/// </param>
	/// <returns>
	/// Returns: <see cref="Timer"/> object.
	/// </returns>
	/// </summary>
	Timer(const std::function<void(void)>& a_FunctionBinding,
		float a_fCurrentTime, float a_fTimerDuration, bool a_bRepeat);

	/// <summary>
	/// void func(int) Constructor. Creates a new Timer instance with a
	/// function of the form "void function(int)".
	/// <param name="a_FunctionBinding">
	/// @param a_FunctionBinding: The std::function binding to call when the
	/// timer finishes. The function must be of the kind void func(int). To create
	/// this, you will need to call the std::bind function. For more
	/// information on how to do this, see the examples section in the link 
	/// <see href="http://en.cppreference.com/w/cpp/utility/functional/function">
	/// HERE</see>
	/// </param>
	/// <param name="a_iArgumentValue">
	/// @param a_iArgumentValue: The value of the int argument to pass in when
	/// calling the function.
	/// </param>
	/// <param name="a_fCurrentTime">
	/// @param a_fCurrentTime: The current time.
	/// </param>
	/// <param name="a_fTimerDuration">
	/// @param a_fTimerDuration: The time (in seconds) to elapse before the
	/// function is called.
	/// </param>
	/// <param name="a_bRepeat">
	/// @param a_bRepeat: Whether the timer should be repeated when time runs
	/// out.
	/// </param>
	/// <returns>
	/// Returns: <see cref="Timer"/> object.
	/// </returns>
	/// </summary>
	Timer(const std::function<void(int)>& a_FunctionBinding, int a_iArgumentValue,
		float a_fCurrentTime, float a_fTimerDuration, bool a_bRepeat);

	/// <summary>
	/// Destructor. Performs necessary clean up functionality on destruction of
	/// an instance.
	/// </summary>
	~Timer();

	/*-------------------------------------------------------------------------
								PUBLIC METHODS
	-------------------------------------------------------------------------*/

	/// <summary>
	/// Determines whether the timer has elapsed based on the passed time.
	/// <param name="a_fCurrentTime">@param a_fCurrentTime: The time to compare
	/// whether time has passed against.</param>
	/// <returns>Returns true if the time has elapsed, returns false otherwise.
	/// </returns>
	/// </summary>
	bool hasTimeElapsed(float a_fCurrentTime);

	/// <summary>
	/// Calls the function.
	/// </summary>
	void invokeFunction();

	/// <summary>
	/// Determines whether the timer is to be repeated.
	/// <returns>Returns true if the timer is to be repeated, returns false
	/// otherwise.
	/// </returns>
	/// </summary>
	bool isRepeat();

	/// <summary>
	/// Restarts the timer.
	/// <param name="a_fCurrentTime">
	/// @param a_fCurrentTime: The current time.
	/// </param>
	/// </summary>
	void restart(float a_fCurrentTime);

	/// <summary>
	/// Gets the end time of the timer.
	/// <returns>
	/// Returns the end time.
	/// </returns>
	/// </summary>
	float getEndTime() { return m_fEndTime; }

private:

	/*-------------------------------------------------------------------------
							PRIVATE MEMBER VARIABLES
	-------------------------------------------------------------------------*/

	/// <summary>
	/// The std::function object, of the form void func(), to call when the
	/// timer runs out.
	/// </summary>
	std::function<void(void)>	m_p_VoidFuncBind;

	/// <summary>
	/// The std::function object, of the form void func(int), to call when the
	/// timer runs out.
	/// </summary>
	std::function<void(int)>	m_p_IntFuncBind;

	/// <summary>
	/// The value of the argument to pass into the function when it is called.
	/// </summary>
	int							m_iArgumentValue;

	/// <summary>
	/// The duration of time (in seconds) to pass for the timer to finish.
	/// <summary>
	float						m_fTimeDuration;

	/// <summary>
	/// The duration of time (in seconds) to pass for the timer to finish.
	/// <summary>
	float						m_fEndTime;

	/// <summary>
	/// Whether or not the timer is to be repeated.
	/// </summary>
	bool						m_bToRepeat;

	/*-------------------------------------------------------------------------
								PRIVATE METHODS
	-------------------------------------------------------------------------*/

	/// <summary>
	/// Initialises and sets the timer.
	/// <param name="a_fCurrentTime">
	/// @param a_fCurrentTime: The current time.
	/// </param>
	/// </summary>
	void setTimer(float a_fCurrentTime);
};

} // namespace target
