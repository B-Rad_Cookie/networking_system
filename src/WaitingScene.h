/*
file:				WaitingScene.h
author:				Brad Cook.
description:		This file contains the declaration of the WaitingScene class:
					The scene where players are made to wait for the next game.
date last modified:	15/01/2017
*/

#pragma once

#include "IScene.h"
#include <Font.h>

/// <summary>
/// The scene where players are made to wait for the next game. This is a
/// waiting area for players. If gameplay has already started and a player
/// connects during this, they do not join the game, but instead wait for
/// the next game.
/// </summary>
class WaitingScene : public IScene
{
public:
	/// <summary>
	/// Default Constructor. Returns a new WaitingScene instance. To set up the
	/// scene, call the init() function.
	/// <returns>
	/// returns WaitingScene instance.
	/// </returns>
	/// </summary>
	WaitingScene() {}

	/*-------------------------------------------------------------------------
								PUBLIC METHODS
	-------------------------------------------------------------------------*/

	/// <summary>
	/// Sets up all the elements required by the scene. This function is called
	/// when the game is first loaded.
	/// <param name="a_p_PeerInter">
	/// @param a_p_PeerInter: The RakNet::RakPeerInterface object in use for
	/// the server.
	/// </param>
	/// </summary>
	bool init(RakNet::RakPeerInterface* a_p_PeerInter);

	/// <summary>
	/// Deletes and frees up up all the elements required by the scene. This
	/// function is called when the game is last closed.
	/// </summary>
	void destroy();

	/// <summary>
	/// Updates all the relevant information (elements) in the
	/// scene per frame.
	/// <param name="dt">
	/// @param dt: Delta time, the time (in seconds) between frames executing.
	/// </param>
	/// <returns>
	/// Returns the name of the scene to change to.
	/// </returns>
	/// </summary>
	std::string update(double dt);

	/// <summary>
	/// Draws the entities and information in the scene (i.e. sends the
	/// graphics to GPU).
	/// </summary>
	void draw();

	/// <summary>
	/// Sets up all the elements in the scene. This function is called everytime
	/// the scene is set as the active scene (i.e. When
	/// Application.setActiveScene is called). It is not called when the game is
	/// initially loaded.
	/// </summary>
	void activate();

	/// <summary>
	/// Cleans up all the elements in the scene. This function is called
	/// everytime the active scene is changed (i.e. When
	/// Application.setActiveScene is called). It is not called when the game is
	/// exited.
	/// </summary>
	void deactivate();

	/// <summary>
	/// Handles network messages. Receives the network messages and sends the
	/// information to the relevant function.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The data sent with a network message.
	/// </param>
	/// <returns>
	/// Resturns: the name of the scene to change to if any. Returns "" if
	/// no scene is required.
	/// </returns>
	/// </summary>
	std::string handleNetworkMessages(RakNet::Packet* a_p_Packet);

private:

	/*-------------------------------------------------------------------------
							PRIVATE MEMBER VARIABLES
	-------------------------------------------------------------------------*/

	/// <summary>
	/// The <see cref="aie::Font"/> object font used to display text on the
	/// screen.
	/// </summary>
	aie::Font*					m_p_DisplayFont;

	/// <summary>
	/// Determines whether a new game has started.
	/// </summary>
	bool						m_bNewGameStarted;

	/// <summary>
	/// Reference to the RakPeerInterface object;
	/// </summary>
	RakNet::RakPeerInterface*	m_p_PeerInter;

	/*-------------------------------------------------------------------------
								PRIVATE METHODS
	-------------------------------------------------------------------------*/

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_CLIENT_NEW_GAME'. The message is sent from the server
	/// informing a new game is going to begin.
	/// <remarks>
	/// The message is sent from ServerEndScene::sendNewGame().
	/// </remarks>
	/// </summary>
	void handleNewGame();
};
