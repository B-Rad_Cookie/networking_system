/*
file:				Crosshair.cpp
author:				Brad Cook.
description:		This file contains the definition of the Crosshair
					struct: The representation of the crosshair the players
					control to destroy targets.
date last modified:	02/01/2017
*/

#include "Crosshair.h"
#include <Renderer2D.h>
#include "ClientInfo.h"

aie::Texture* Crosshair::s_p_RedCrosshairTexture = nullptr;
aie::Texture* Crosshair::s_p_BlueCrosshairTexture = nullptr;
aie::Texture* Crosshair::s_p_GreenCrosshairTexture = nullptr;
aie::Texture* Crosshair::s_p_YellowCrosshairTexture = nullptr;

Crosshair::Crosshair() : NetworkObject()
{
	m_Position = glm::vec3(0);
	m_Velocity = glm::vec3(0);
	m_p_Texture = nullptr;
}

Crosshair::Crosshair(unsigned int a_uiID, const glm::vec3 &a_Pos)
	: NetworkObject(a_uiID)
{
	m_Position = a_Pos;
	m_Velocity = glm::vec3(0);

	// Set the texture based on the Id/Player number.
	switch (m_uiNetworkID)
	{
	case 1:  // Player 1 : Red
	{
		m_p_Texture = Crosshair::s_p_RedCrosshairTexture;
		break;
	}
	case 2:  // Player 2 : Blue
	{
		m_p_Texture = Crosshair::s_p_BlueCrosshairTexture;
		break;
	}
	case 3:  // Player 3 : Green
	{
		m_p_Texture = Crosshair::s_p_GreenCrosshairTexture;
		break;
	}
	case 4:  // Player 4: Yellow.
	{
		m_p_Texture = Crosshair::s_p_YellowCrosshairTexture;
		break;
	}
	default:
	{  // Set to white
		m_p_Texture = nullptr;
		break;
	}
	}
}

Crosshair::~Crosshair()
{
	// No allocated heap memory, so do nothing
}

void Crosshair::draw()
{
	aie::Renderer2D* p_Renderer = ClientInfo::getRenderer();
	p_Renderer->drawSprite(m_p_Texture, m_Position.x, m_Position.y);
}

void Crosshair::setupTextures()
{
	s_p_RedCrosshairTexture = new aie::Texture(
		"./images/red_crosshair.png");
	s_p_BlueCrosshairTexture = new aie::Texture(
		"./images/blue_crosshair.png");
	s_p_GreenCrosshairTexture = new aie::Texture(
		"./images/green_crosshair.png");
	s_p_YellowCrosshairTexture = new aie::Texture(
		"./images/yellow_crosshair.png");
}

void Crosshair::destroyTextures()
{
	delete s_p_RedCrosshairTexture;
	delete s_p_BlueCrosshairTexture;
	delete s_p_GreenCrosshairTexture;
	delete s_p_YellowCrosshairTexture;
}
