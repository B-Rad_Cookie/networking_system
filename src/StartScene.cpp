/*
file:				StartScene.cpp
author:				Brad Cook.
description:		This file contains the definition of the StartScene class:
					The scene that is shown at the start of the game.
date last modified:	18/01/2017
*/

#include "gl_core_4_4.h"
#include <gl\GLU.h>
#include "StartScene.h"

#include "ClientInfo.h"
#include <CustomGameMessages.h>

#include <Renderer2D.h>

#include <iostream>
#include <string>
#include <functional>

bool StartScene::init(RakNet::RakPeerInterface* a_p_PeerInter)
{
	// Setup the font.
	m_p_DisplayFont = new aie::Font("./font/consolas.ttf", 24);

	// Setup the time manager.
	m_p_TimeManager = new target::TimeManager();

	// Get the peer interface reference.
	m_p_PeerInter = a_p_PeerInter;

	return true;
}

void StartScene::destroy()
{
	// delete the font.
	delete m_p_DisplayFont;

	// delete the time manager.
	delete m_p_TimeManager;

	m_p_PeerInter = nullptr;
}

std::string StartScene::update(double dt)
{
	// If client is connected, send register notice.
	if (m_bHasRegistered == false &&
		ClientInfo::getConnectStatus() == ConnectionStatus::Connected)
	{
		sendRegister();
		m_bHasRegistered = true;
	}

	// Update the Time Manager.
	m_p_TimeManager->update(dt);

	// Check if the game has started. if so, change scene to the gameplay
	// scene.
	if (m_eStartStatus == GameStartStatus::GameStarted)
	{
		return "GameplayScene";
	}

	return "";
}

void StartScene::draw()
{
	// Get the renderer and window widths and height.
	aie::Renderer2D* p_Renderer = ClientInfo::getRenderer();
	int iWidth = ClientInfo::getWindowWidth();
	int iHeight = ClientInfo::getWindowHeight();

	p_Renderer->drawText(m_p_DisplayFont,
		"Welcome to Target Blast!",
		((float)iWidth / 2.0f) - 170.0f,
		((float)iHeight / 2.0f) + 150.0f);

	// Draw the connection info.
	drawConnectedInfo(p_Renderer, iWidth, iHeight);
}

void StartScene::activate()
{
	// Reset the time manager.
	m_p_TimeManager->reset();

	// Set the waiting status to GameWaiting.
	m_eStartStatus = GameStartStatus::GameWaiting;

	// Set has registered to false;
	m_bHasRegistered = false;
}

void StartScene::deactivate()
{
	// Clear all the timers.
	m_p_TimeManager->clearTimers();
}

void StartScene::drawConnectedInfo(aie::Renderer2D* a_p_2dRenderer,
	int a_iWidth, int a_iHeight)
{
	// Print the player information and inform them of waiting for a game
	// to start.
	a_p_2dRenderer->drawText(m_p_DisplayFont, "You are",
		((float)a_iWidth / 2.0f) - 170.0f,
		((float)a_iHeight / 2.0f) + 50.0f);

	// Get the player's number and colour to use to print the information.
	unsigned int uiPlayerNumber = ClientInfo::getClientID();
	glm::vec4 Colour = ClientInfo::getPlayerColour();
	std::string sPrintString(
		"Player Number " + std::to_string(uiPlayerNumber));

	a_p_2dRenderer->setRenderColour(
		Colour.r, Colour.g, Colour.b, Colour.a);

	a_p_2dRenderer->drawText(m_p_DisplayFont, sPrintString.c_str(),
		((float)a_iWidth / 2.0f) - 63.0f, ((float)a_iHeight / 2.0f) + 50.0f);

	// Set render colour back to white.
	a_p_2dRenderer->setRenderColour(1, 1, 1, 1);

	// Print the information regarding as to whether the player needs to
	// wait for enough players to connect to start a game or not.
	if (m_eStartStatus == GameStartStatus::GameWaiting)
	{
		a_p_2dRenderer->drawText(m_p_DisplayFont,
			"Waiting for more players to join the game...",
			((float)a_iWidth / 2.0f) - 290.0f,
			((float)a_iHeight / 2.0f) - 50.0f);
	}
	else if (m_eStartStatus == GameStartStatus::GameStarting)
	{
		std::string sRenderText = std::string("Game will start in ");
		sRenderText.append
			(m_p_TimeManager->getPrintTimeRemaining("Countdown").c_str());
		sRenderText.append(" seconds...");
		a_p_2dRenderer->drawText(m_p_DisplayFont,
			sRenderText.c_str(),
			((float)a_iWidth / 2.0f) - 220.0f,
			((float)a_iHeight / 2.0f) - 50.0f);
	}
}

void StartScene::startGame()
{
	// Set the game status to started.
	m_eStartStatus = GameStartStatus::GameStarted;
}

std::string StartScene::handleNetworkMessages(RakNet::Packet* a_p_Packet)
{
	switch (a_p_Packet->data[0])
	{
	case ID_CLIENT_START_COUNTDOWN:
	{
		// Start countdown
		handleStartCountdown(a_p_Packet);
		break;
	}
	case ID_CLIENT_START_COUNTDOWN_ONE:
	{
		// Start countdown
		handleStartCountdownOne(a_p_Packet);
		break;
	}
	case ID_CLIENT_STOP_COUNTDOWN:
	{
		// Stop countdown.
		handleStopCountdown();
		break;
	}
	}

	// There is no change of scene driven by a network message, so return ""
	return "";
}

void StartScene::handleStartCountdown(RakNet::Packet* a_p_Packet)
{
	// First read the data (the amount fo time remaining before the game starts).
	unsigned int uiTimeRemaining;
	RakNet::BitStream bs(a_p_Packet->data, a_p_Packet->length, false);
	bs.IgnoreBytes(sizeof(RakNet::MessageID));
	bs.Read(uiTimeRemaining);

	// Set the status to starting and start the timer.
	m_eStartStatus = GameStartStatus::GameStarting;

	m_p_TimeManager->addTimer("Countdown",
		std::bind(&StartScene::startGame, this), uiTimeRemaining, false);
}

void StartScene::handleStartCountdownOne(RakNet::Packet* a_p_Packet)
{
	// First read the data (the amount fo time remaining before the game starts).
	unsigned int uiTimeRemaining;
	RakNet::BitStream bs(a_p_Packet->data, a_p_Packet->length, false);
	bs.IgnoreBytes(sizeof(RakNet::MessageID));
	bs.Read(uiTimeRemaining);

	// Set the status to starting and start the timer.
	m_eStartStatus = GameStartStatus::GameStarting;

	m_p_TimeManager->addTimer("Countdown",
		std::bind(&StartScene::startGame, this), uiTimeRemaining, false);
}

void StartScene::handleStopCountdown()
{
	// No data is sent with this message, so the packet is not required.
	// Stop the countdown timer and set the game status to waiting.
	m_p_TimeManager->stopTimer("Countdown");
	m_eStartStatus = GameStartStatus::GameWaiting;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//					Network Messages sent to Server

void StartScene::sendRegister()
{
	// Send the ID_SERVER_READY_TO_START message.
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_SERVER_REGISTER));
	m_p_PeerInter->Send(&bs, IMMEDIATE_PRIORITY, RELIABLE, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}
