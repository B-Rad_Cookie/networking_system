/*
file:				Gameplay.cpp
author:				Brad Cook.
description:		This file contains the definition of the Gameplay class:
					The main gameplay scene within the game.
date last modified:	15/01/2017
*/

#include "Gameplay.h"
#include "Target.h"
#include "Crosshair.h"
#include "Circle.h"

#include "ClientInfo.h"
#include <Renderer2D.h>
#include <Consts.h>
#include "ColDetect.h"

#include <RakPeerInterface.h>
#include <MessageIdentifiers.h>
#include <BitStream.h>

#include <CustomGameMessages.h>
#include <Enums.h>
#include <Input.h>

#include <string>
#include <functional>

bool Gameplay::init(RakNet::RakPeerInterface* a_p_PeerInter)
{
	// Setup the time manager.
	m_p_TimeManager = new target::TimeManager();

	// Setup the font.
	m_p_DisplayFont = new aie::Font("./font/consolas.ttf", 24);

	// Get the peer interface reference.
	m_p_PeerInter = a_p_PeerInter;

	// Setup the texture for targets and crosshairs.
	Target::setupTextures();
	Crosshair::setupTextures();

	return true;
}

void Gameplay::destroy()
{
	// Remove the entities
	destroyEntities();

	// destroy the timer.
	delete m_p_TimeManager;

	// delete the font.
	delete m_p_DisplayFont;

	// Delete the target and crosshair textures
	Target::destroyTextures();
	Crosshair::destroyTextures();
}

std::string Gameplay::update(double dt)
{
	// Update the time manager
	m_p_TimeManager->update(dt);

	// If the game has ended, move onto the end scene.
	if (m_bGameEnded == true)
	{
		return "EndScene";
	}

	// Split the update based on the gameplay state.
	std::string sReturnString = "";
	switch (m_eGameplayStatus)
	{
	case GameplayStatus::Waiting:
	{
		sReturnString = updateWaiting(dt);
		break;
	}
	case GameplayStatus::Setup:
	{
		sReturnString = updateSetup(dt);
		break;
	}
	case GameplayStatus::Starting:
	{
		sReturnString = updateStarting(dt);
		break;
	}
	case GameplayStatus::Playing:
	{
		sReturnString = updatePlaying(dt);
		break;
	}
	default:
		break;
	}

	// Return if the return string for changing scene is set to something.
	if (sReturnString != "")
	{
		return sReturnString;
	}
	return "";
}

std::string Gameplay::updateWaiting(double dt)
{
	// Nothing to do: wait for messages from server.
	return "";
}

std::string Gameplay::updateSetup(double dt)
{
	// When the data has all been loaded, send the load data ready message.
	if (m_vTargets.size() >= Consts::uiMAX_NUM_OF_TARGETS)
	{
		// Set the gameplay status type to starting.
		m_eGameplayStatus = GameplayStatus::Starting;

		// Send the data loaded message to the server.
		sendDataLoaded();
	}
	return "";
}

std::string Gameplay::updateStarting(double dt)
{
	return "";
}

std::string Gameplay::updatePlaying(double dt)
{
	// handle Input.
	handleInput(dt);

	// Update the crosshairs
	updateCrosshairs(dt);

	return "";
}

void Gameplay::draw()
{
	// Get the renderer and window widths and height.
	aie::Renderer2D* p_Renderer = ClientInfo::getRenderer();
	int iWidth = ClientInfo::getWindowWidth();
	int iHeight = ClientInfo::getWindowHeight();

	// What gets drawn to the screen depends on the gameplay status
	switch (m_eGameplayStatus)
	{
	case Waiting:
	{
		p_Renderer->drawText(m_p_DisplayFont,
			"Waiting for other players to join...",
			((float)iWidth / 2.0f) - 170.0f,
			((float)iHeight / 2.0f));
		break;
	}
	case Setup:
	{
		std::string sPrintString = std::string(
			"Round " + std::to_string(m_uiRoundNumber) +
			" will start when this message disappears");

		p_Renderer->drawText(m_p_DisplayFont,
			sPrintString.c_str(),
			((float)iWidth / 2.0f) - 350.0f,
			((float)iHeight / 2.0f));
		break;
	}
	case Starting:
	{
		std::string sPrintString = std::string(
			"Round " + std::to_string(m_uiRoundNumber) +
			" will start when this message disappears");

		p_Renderer->drawText(m_p_DisplayFont,
			sPrintString.c_str(),
			((float)iWidth / 2.0f) - 350.0f,
			((float)iHeight / 2.0f));

		break;
	}
	case Playing:
	{
		// Print the time.
		std::string sTimeString = std::string("Time Remaining: ");
		sTimeString.append
			(m_p_TimeManager->getPrintTimeRemaining("GameRound").c_str());
		p_Renderer->drawText(m_p_DisplayFont,
			sTimeString.c_str(),
			((float)iWidth / 2.0f) - 130.0f,
			(float)iHeight - 30.0f);

		// Go through and print any target destroy display messages.
		for (std::vector<DisplayTargetDestroy>::iterator info = m_vDisplayInfo.begin();
			 info != m_vDisplayInfo.end(); info++)
		{
			// Change the render colour based on the player who destroyed the
			// target.
			switch ((*info).m_uiPlayerID)
			{
			case 1:  // Red
			{
				p_Renderer->setRenderColour(1.0f, 0.0f, 0.0f);
				break;
			}
			case 2:  // Blue
			{
				p_Renderer->setRenderColour(0.01f, 0.2f, 1.0f);
				break;
			}
			case 3:  // Green
			{
				p_Renderer->setRenderColour(0.0f, 0.7f, 0.0f);
				break;
			}
			case 4:  // Yellow
			{
				p_Renderer->setRenderColour(1.0f, 0.95f, 0.0f);
				break;
			}
			}
			// Draw the text
			p_Renderer->drawText(m_p_DisplayFont,
				std::to_string((*info).m_iPointsScored).c_str(),
				(*info).m_Position.x, (*info).m_Position.y);
		}
		
		// Set the renderer back to default colour (white).
		p_Renderer->setRenderColour(1.0f, 1.0f, 1.0f);

		break;
	}
	default:
	{
		break;
	}
	}

	// Print player's score.
	std::string sPlayerScore = std::string(
		"Score: " + std::to_string(ClientInfo::getPlayerScore()));

	p_Renderer->drawText(m_p_DisplayFont,
		sPlayerScore.c_str(),
		20.0f, (float)iHeight - 30.0f);

	// Draw the crosshairs.
	for (Crosshair* p_cross : m_vCrosshairs)
	{
		p_cross->draw();
	}

	// Draw the targets.
	for (Target* p_target : m_vTargets)
	{
		p_target->draw();
	}
}

void Gameplay::activate()
{
	// Reset the timer.
	m_p_TimeManager->reset();

	// Set the game ended to false
	m_bGameEnded = false;

	// Set the round number to 0.
	m_uiRoundNumber = 0;

	// Set player score to 0
	ClientInfo::setPlayerScore(0);

	// Start of as waiting for a game to occur.
	m_eGameplayStatus = GameplayStatus::Waiting;

	// Send the ready message.
	sendReadyToStart();
}

void Gameplay::deactivate()
{
	// Remove the entities.
	destroyEntities();
}

void Gameplay::handleInput(double dt)
{
	// Only update the info if the player's index is the correct one.
	if (m_iCrosshairIndex < m_vCrosshairs.size() &&
		m_vCrosshairs[m_iCrosshairIndex]->m_uiNetworkID == ClientInfo::getClientID())
	{
		// Get the input handler
		aie::Input* p_Input = aie::Input::getInstance();

		// Setup a temp velocity.
		glm::vec3 velocity = glm::vec3(0);

		// Use the arrow keys to add velocity to the crosshair.
		if (p_Input->isKeyDown(aie::INPUT_KEY_UP) ||
			p_Input->isKeyDown(aie::INPUT_KEY_W))
			velocity.y += Consts::fMOVEMENT_SPEED;

		if (p_Input->isKeyDown(aie::INPUT_KEY_DOWN) ||
			p_Input->isKeyDown(aie::INPUT_KEY_S))
			velocity.y -= Consts::fMOVEMENT_SPEED;

		if (p_Input->isKeyDown(aie::INPUT_KEY_LEFT) ||
			p_Input->isKeyDown(aie::INPUT_KEY_A))
			velocity.x -= Consts::fMOVEMENT_SPEED;

		if (p_Input->isKeyDown(aie::INPUT_KEY_RIGHT) ||
			p_Input->isKeyDown(aie::INPUT_KEY_D))
			velocity.x += Consts::fMOVEMENT_SPEED;

		// Create a temp position.
		glm::vec3 position = m_vCrosshairs[m_iCrosshairIndex]->m_Position;
		position += velocity * (float)dt;

		// Check to see if the proposed position will take the crosshair out of
		// bounds. If so, freeze the position to the screen.
		if (position.x < 0 || position.x >(float)ClientInfo::getWindowWidth())
		{
			position.x = m_vCrosshairs[m_iCrosshairIndex]->m_Position.x;
			velocity.x = 0.0f;
		}
		if (position.y < 0 || position.y >(float)ClientInfo::getWindowHeight())
		{
			position.y = m_vCrosshairs[m_iCrosshairIndex]->m_Position.y;
			velocity.y = 0.0f;
		}

		// Set the velocity and the position on the player's crosshair.
		m_vCrosshairs[m_iCrosshairIndex]->m_Velocity = velocity;
		m_vCrosshairs[m_iCrosshairIndex]->m_Position = position;

		// Handle destroying a target. When the destroy button is pressed,
		// first check that it catches a target.
		if (p_Input->wasKeyPressed(aie::INPUT_KEY_SPACE))
		{
			// Check if it intersects with any targets.
			for (std::vector<Target*>::iterator target = m_vTargets.begin();
			target != m_vTargets.end(); target++)
			{
				if (ColDetect::AreColliding(
					m_vCrosshairs[m_iCrosshairIndex]->m_Position,
					(*target)->m_p_Circle) == true)
				{
					// If it does, then send the information to the server to
					// verify whether the client has destroyed this target.
					sendCheckDestroyTarget((*target));
					break;
				}
			}
		}
	}
}

void Gameplay::destroyEntities()
{
	// Go through and remove the memory, then clear the vector.
	for (std::vector<Target*>::iterator targetIt = m_vTargets.begin();
	targetIt != m_vTargets.end(); targetIt++)
	{
		delete (*targetIt);
	}
	m_vTargets.clear();

	for (std::vector<Crosshair*>::iterator crossIt = m_vCrosshairs.begin();
	crossIt != m_vCrosshairs.end(); crossIt++)
	{
		delete (*crossIt);
	}
	m_vCrosshairs.clear();

	// Remove the display info.
	m_vDisplayInfo.clear();

	// Stop all timers.
	m_p_TimeManager->clearTimers();
}

void Gameplay::addDisplayTargetDestroyedInfo(unsigned int a_uiTargetID,
	unsigned int a_uiPlayerID, int a_iPointsScored,
	const glm::vec3& a_Position)
{
	// Create the new info.
	DisplayTargetDestroy NewInfo;
	NewInfo.m_uiTargetID = a_uiTargetID;
	NewInfo.m_uiPlayerID = a_uiPlayerID;
	NewInfo.m_iPointsScored = a_iPointsScored;
	NewInfo.m_Position = a_Position;
	m_vDisplayInfo.push_back(NewInfo);

	// Start the timer for how long the message should be displayed.
	// Name the timer "DisplayInfo[TargetID]".
	std::string sName = std::string(
		"DisplayInfo" + std::to_string(a_uiTargetID));
	using std::placeholders::_1;
	m_p_TimeManager->addTimer(
		sName, std::bind(&Gameplay::removeDisplayInfo, this, _1),
		a_uiTargetID, 2.0f, false);
}

//#########################################################################
//							Entities

void Gameplay::updateCrosshairs(double dt)
{
	// Go through each crosshair.
	for (int i = 0; i < m_vCrosshairs.size(); i++)
	{
		// If the crosshair is the player's own crosshair, ignore.
		if (i == m_iCrosshairIndex)
		{
			continue;
		}

		// Update the position based on the last known velocity;
		m_vCrosshairs[i]->m_Position += (
			m_vCrosshairs[i]->m_Velocity * (float)dt);
	}
}

//#########################################################################
//						Network message handling

std::string Gameplay::handleNetworkMessages(RakNet::Packet* a_p_Packet)
{
	// Set the return string to nothing ("")
	std::string sReturnString = "";

	switch (a_p_Packet->data[0])
	{
	case ID_CLIENT_SETUP_GAME:
	{
		// Game being set up
		handleGameSetup();
		break;
	}
	case ID_CLIENT_INITIAL_TARGETS:
	{
		// Initial Targets
		handleInitialTargets(a_p_Packet);
		break;
	}
	case ID_CLIENT_CROSSHAIRS:
	{
		// Initial Crosshairs
		handleCrosshairs(a_p_Packet);
		break;
	}
	case ID_CLIENT_ROUND_START:
	{
		// Initial Crosshairs
		handleRoundStart();
		break;
	}
	case ID_CLIENT_UPDATE_CROSSHAIRS:
	{
		// updated Crosshairs
		handleUpdateCrosshairs(a_p_Packet);
		break;
	}
	case ID_CLIENT_TARGET_DESTROYED:
	{
		// target destroyed
		handleTargetDestroyed(a_p_Packet);
		break;
	}
	case ID_CLIENT_TARGET_REMOVED:
	{
		// target removed
		handleTargetRemoved(a_p_Packet);
		break;
	}
	case ID_CLIENT_NEW_TARGETS:
	{
		// targets added
		handleNewTargets(a_p_Packet);
		break;
	}
	case ID_CLIENT_REMOVE_PLAYER:
	{
		// Player left
		handleRemovePlayer(a_p_Packet);
		break;
	}
	case ID_CLIENT_END_ROUND:
	{
		// Round ended
		handleRoundEnd();
		break;
	}
	case ID_CLIENT_RESTART_ROUND:
	{
		// new round restarting
		handleRoundRestart();
		break;
	}
	case ID_CLIENT_END_GAME:
	{
		// Game ended
		handleGameEnd();
		break;
	}
	}

	// Return the return string.
	return sReturnString;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//					Network Messages received from server

void Gameplay::handleGameSetup()
{
	// Set the gameplay status and increment the round number.
	m_eGameplayStatus = GameplayStatus::Setup;
	m_uiRoundNumber++;
}

void Gameplay::handleInitialTargets(RakNet::Packet* a_p_Packet)
{
	// Create a BitStream from the data.
	RakNet::BitStream bs(a_p_Packet->data, a_p_Packet->length, false);
	bs.IgnoreBytes(sizeof(RakNet::MessageID));

	// Go through all the targets and add them.
	for (int i = 0; i < Consts::uiMAX_NUM_OF_TARGETS; i++)
	{
		readTargetFromMessage(bs);
	}
}

void Gameplay::readTargetFromMessage(RakNet::BitStream& a_bs)
{
	// Firstly, read all the native data from the bitstream.
	unsigned int uiNetworkID = 0;
	TargetType eType = Blue;
	float fX = 0.0f;
	float fY = 0.0f;
	a_bs.Read(uiNetworkID);
	a_bs.Read((TargetType)eType);
	a_bs.Read(fX);
	a_bs.Read(fY);

	// Construct and add a Target object from this information.
	glm::vec3 Centre = glm::vec3(fX, fY, 1.0f);
	m_vTargets.push_back(new Target(uiNetworkID, eType, Centre));
}

void Gameplay::handleCrosshairs(RakNet::Packet* a_p_Packet)
{
	// Firstly read the information.
	RakNet::BitStream bs(a_p_Packet->data, a_p_Packet->length, false);
	bs.IgnoreBytes(sizeof(RakNet::MessageID));
	int iNumOfCrosshairs;
	bs.Read(iNumOfCrosshairs);

	// Create and add a crosshair for each crosshair
	for (int i = 0; i < iNumOfCrosshairs; i++)
	{
		unsigned int uiID;
		float fX;
		float fY;
		bs.Read(uiID);
		bs.Read(fX);
		bs.Read(fY);

		m_vCrosshairs.push_back(new Crosshair(uiID, glm::vec3(fX, fY, 1)));
	}

	// Find the crosshair that is this players and save that info.
	int i = 0;
	for (auto crosshair = m_vCrosshairs.begin(); crosshair != m_vCrosshairs.end();
		 crosshair++)
	{
		if ((*crosshair)->m_uiNetworkID == ClientInfo::getClientID())
		{
			m_iCrosshairIndex = i;
			break;
		}
		else
		{
			i++;
		}
	}
}

void Gameplay::handleRoundStart()
{
	// Set the gameplay status to playing.
	m_eGameplayStatus = GameplayStatus::Playing;

	// Start the round timer.
	m_p_TimeManager->addTimer("GameRound", 
		std::bind(&Gameplay::dummy, this),
		Consts::fGAMEPLAY_ROUND_TIME, false);

	// Start the server update timer.
	m_p_TimeManager->addTimer("UpdateInfo",
		std::bind(&Gameplay::sendMovementInfo, this),
		m_fMOVEMENT_UPDATE_INTERVAL, true);
}

void Gameplay::handleUpdateCrosshairs(RakNet::Packet* a_p_Packet)
{
	// Firstly read the information.
	RakNet::BitStream bs(a_p_Packet->data, a_p_Packet->length, false);
	bs.IgnoreBytes(sizeof(RakNet::MessageID));
	int iNumOfCrosshairs;
	bs.Read(iNumOfCrosshairs);

	// Go through all the crosshairs
	for (int i = 0; i < iNumOfCrosshairs; i++)
	{
		// Read in each crosshair.
		unsigned int uiID;
		float fPositionX;
		float fPositionY;
		float fVelocityX;
		float fVelocityY;

		bs.Read(uiID);
		bs.Read(fPositionX);
		bs.Read(fPositionY);
		bs.Read(fVelocityX);
		bs.Read(fVelocityY);

		// If this crosshair is the client's own crosshair, ignore and move on
		// to the next crosshair.
		if (uiID == ClientInfo::getClientID())
		{
			continue;
		}

		// Find the crosshair with this Client ID.
		for (int i = 0; i < m_vCrosshairs.size(); i++)
		{
			if (m_vCrosshairs[i]->m_uiNetworkID == uiID)
			{
				// Update the movement info
				m_vCrosshairs[i]->m_Position = glm::vec3(fPositionX, fPositionY, 1);
				m_vCrosshairs[i]->m_Velocity = glm::vec3(fVelocityX, fVelocityY, 1);

				break;
			}
		}
	}
}

void Gameplay::handleTargetDestroyed(RakNet::Packet* a_p_Packet)
{
	// Firstly read the information.
	RakNet::BitStream bs(a_p_Packet->data, a_p_Packet->length, false);
	bs.IgnoreBytes(sizeof(RakNet::MessageID));
	unsigned int ui_TargetID;
	unsigned int ui_ClientID;
	bs.Read(ui_TargetID);
	bs.Read(ui_ClientID);

	// Find the index of the target.
	int iTargetIndex = -1;
	for (int i = 0; i < m_vTargets.size(); i++)
	{
		if (m_vTargets[i]->m_uiNetworkID == ui_TargetID)
		{
			iTargetIndex = i;
			break;
		}
	}

	if (iTargetIndex != -1)
	{
		// If the client is the player, add the  score to the player's score.
		if (ui_ClientID == ClientInfo::getClientID())
		{
			ClientInfo::addToScore(m_vTargets[iTargetIndex]->m_iScore);
		}

		// Add the target destroyed information to the list of display info.
		addDisplayTargetDestroyedInfo(ui_TargetID, ui_ClientID,
			m_vTargets[iTargetIndex]->m_iScore,
			m_vTargets[iTargetIndex]->m_p_Circle->m_centre);

		// Remove the target.
		delete m_vTargets[iTargetIndex];
		m_vTargets.erase(m_vTargets.begin() + iTargetIndex);
	}
}

void Gameplay::handleTargetRemoved(RakNet::Packet* a_p_Packet)
{
	// Firstly read the information.
	RakNet::BitStream bs(a_p_Packet->data, a_p_Packet->length, false);
	bs.IgnoreBytes(sizeof(RakNet::MessageID));
	unsigned int ui_TargetID;
	bs.Read(ui_TargetID);

	// Find the index of the target.
	int iTargetIndex = 0;
	for (int i = 0; i < m_vTargets.size(); i++)
	{
		if (m_vTargets[i]->m_uiNetworkID == ui_TargetID)
		{
			iTargetIndex = i;
			break;
		}
	}

	// Remove the target.
	delete m_vTargets[iTargetIndex];
	m_vTargets.erase(m_vTargets.begin() + iTargetIndex);
}

void Gameplay::handleNewTargets(RakNet::Packet* a_p_Packet)
{
	// First read the information.
	RakNet::BitStream bs(a_p_Packet->data, a_p_Packet->length, false);
	bs.IgnoreBytes(sizeof(RakNet::MessageID));
	int iNumAdded;
	bs.Read(iNumAdded);

	// Add all added targets.
	for (int i = 0; i < iNumAdded; i++)
	{
		readTargetFromMessage(bs);
	}
}

void Gameplay::handleRemovePlayer(RakNet::Packet* a_p_Packet)
{
	// First read the information.
	RakNet::BitStream bs(a_p_Packet->data, a_p_Packet->length, false);
	bs.IgnoreBytes(sizeof(RakNet::MessageID));
	unsigned int iPlayerLeft;
	bs.Read(iPlayerLeft);

	// Find the player and remove them.
	int iCrossIndex = -1;
	for (int i = 0; i < m_vCrosshairs.size(); i++)
	{
		if (m_vCrosshairs[i]->m_uiNetworkID == iPlayerLeft)
		{
			iCrossIndex = i;
			break;
		}
	}
	if (iCrossIndex != -1)
	{
		delete m_vCrosshairs[iCrossIndex];
		m_vCrosshairs.erase(m_vCrosshairs.begin() + iCrossIndex);
	}

	// Reset the crosshair index for the player.
	// Find the crosshair that is this players and save that info.
	int i = 0;
	for (auto crosshair = m_vCrosshairs.begin(); crosshair != m_vCrosshairs.end();
		 crosshair++)
	{
		if ((*crosshair)->m_uiNetworkID == ClientInfo::getClientID())
		{
			m_iCrosshairIndex = i;
			break;
		}
		else
		{
			i++;
		}
	}
}

void Gameplay::handleRoundEnd()
{
	// Clear all entities.
	destroyEntities();

	// Set the gameplay status to end.
	m_eGameplayStatus = GameplayStatus::EndRound;

	// Send the got to the round end message.
	sendAtEndRound();	
}

void Gameplay::handleRoundRestart()
{
	// Clear all entities.
	destroyEntities();

	// Set the gameplay status to waiting.
	m_eGameplayStatus = GameplayStatus::Waiting;

	// Send the ready message.
	sendReadyToStart();
}

void Gameplay::handleGameEnd()
{
	// Game finished. Dont worry about clearing entities here. There will
	// be a change to the EndScene, and the deactivate function here
	// clears the entities.
	m_bGameEnded = true;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//					Network Messages sent to server

void Gameplay::sendReadyToStart()
{
	// Send the ID_SERVER_READY_TO_START message.
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_SERVER_READY_TO_START));
	m_p_PeerInter->Send(&bs, IMMEDIATE_PRIORITY, RELIABLE, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void Gameplay::sendDataLoaded()
{
	// Send the ID_SERVER_READY_TO_START message.
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_SERVER_INITIAL_DATA_LOADED));
	m_p_PeerInter->Send(&bs, IMMEDIATE_PRIORITY, RELIABLE, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void Gameplay::sendMovementInfo()
{
	// Send the crosshair movement info.
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_SERVER_MOVEMENT_INFO));

	// Get the player's crosshair movement info (position and velocity).
	bs.Write(m_vCrosshairs[m_iCrosshairIndex]->m_uiNetworkID);
	bs.Write(m_vCrosshairs[m_iCrosshairIndex]->m_Position.x);
	bs.Write(m_vCrosshairs[m_iCrosshairIndex]->m_Position.y);
	bs.Write(m_vCrosshairs[m_iCrosshairIndex]->m_Velocity.x);
	bs.Write(m_vCrosshairs[m_iCrosshairIndex]->m_Velocity.y);

	// Set priority to low, as the movement isn't important.
	m_p_PeerInter->Send(&bs, LOW_PRIORITY, RELIABLE, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void Gameplay::sendCheckDestroyTarget(Target* a_p_Target)
{
	// Send the crosshair movement info.
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_SERVER_CHECK_DESTROY_TARGET));

	// Send the ID of the target.
	bs.Write(a_p_Target->m_uiNetworkID);
	// Send the client's ID.
	bs.Write(ClientInfo::getClientID());

	// Send as Reliable ordered so that the message (as best as possible)
	// arrives in the order of click when two players are going for the same
	// target.
	m_p_PeerInter->Send(&bs, IMMEDIATE_PRIORITY, RELIABLE_ORDERED, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

void Gameplay::sendAtEndRound()
{
	// Send the ID_SERVER_AT_END_ROUND message.
	RakNet::BitStream bs;
	bs.Write(RakNet::MessageID(GameMessages::ID_SERVER_AT_END_ROUND));
	m_p_PeerInter->Send(&bs, IMMEDIATE_PRIORITY, RELIABLE, 0,
		RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
}

//#########################################################################
//								Time events

void Gameplay::removeDisplayInfo(int a_iTargetID)
{
	// Find the display info.
	int iTargetIndex = -1;
	for (int i = 0; i < m_vTargets.size(); i++)
	{
		if (m_vDisplayInfo[i].m_uiTargetID == a_iTargetID)
		{
			iTargetIndex = i;
			break;
		}
	}

	// Remove the display info.
	if (iTargetIndex != -1)
	{
		m_vDisplayInfo.erase(m_vDisplayInfo.begin() + iTargetIndex);
	}
}
