/*
file:				IScene.h
author:				Brad Cook.
description:		This file contains the declaration of the IScene class: An
					interface class for a scene in a game.
date last modified:	13/01/2017
*/

#pragma once

#include <string>
#include <RakNetTypes.h>

namespace RakNet {
class RakPeerInterface;
}

/// <summary>
/// Interface for a scene in a game. Scenes contain the objects of the game for
/// one specific section. A game is usually made up of multiple scenes, being
/// able to switch between scenes, depending on gameplay.
/// This interface class must be inherited when creating a scene.
/// </summary>
class IScene {
public:

	/// <summary>
	/// Default Constructor. As this is an interface, this constructor does
	/// nothing.
	/// </summary>
	IScene() {}

	/// <summary>
	/// Sets up all the elements required by the scene. This function is called
	/// when the game is first loaded.
	/// <param name="a_p_PeerInter">
	/// @param a_p_PeerInter: The RakNet::RakPeerInterface object in use for
	/// the server.
	/// </param>
	/// </summary>
	virtual bool init(RakNet::RakPeerInterface* a_p_PeerInter) = 0;

	/// <summary>
	/// Deletes and frees up up all the elements required by the scene. This
	/// function is called when the game is last closed.
	/// </summary>
	virtual void destroy() = 0;

	/// <summary>
	/// Updates all the relevant information (elements) in the
	/// scene per frame.
	/// <param name="dt">
	/// @param dt: Delta time, the time (in seconds) between frames executing.
	/// </param>
	/// <returns>
	/// Returns the name of the scene to change to.
	/// </returns>
	/// <remarks>
	/// The implementation in this class immediately returns "".
	/// </remarks>
	/// </summary>
	virtual std::string update(double dt) {
		return "";
	};

	/// <summary>
	/// Draws all the relevant information (elements) in the
	/// scene (i.e. sends the graphics to GPU).
	/// <remarks>
	/// The implementation in this class immediately returns.
	/// </remarks>
	/// </summary>
	virtual void draw() {
		return;
	}

	/// <summary>
	/// Sets up all the elements in the scene. This function is called everytime
	/// the scene is set as the active scene (i.e. When
	/// Application.setActiveScene is called). It is not called when the game is
	/// initially loaded.
	/// </summary>
	virtual void activate() {}

	/// <summary>
	/// Cleans up all the elements in the scene. This function is called
	/// everytime the active scene is changed (i.e. When
	/// Application.setActiveScene is called). It is not called when the game is
	/// exited.
	/// </summary>
	virtual void deactivate() {}

	/// <summary>
	/// Handles network messages. Receives the network messages and sends the
	/// information to the relevant function.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The data sent with a network message.
	/// </param>
	/// <returns>
	/// Resturns: the name of the scene to change to if any. Returns "" if
	/// no scene is required.
	/// </returns>
	/// </summary>
	virtual std::string handleNetworkMessages(RakNet::Packet* a_p_Packet) {
		return "";
	}

	/// <summary>
	/// Called when a player has disconnected from the server. All logic
	/// relating to handling a player disconnecting is done here.
	/// <param name="a_SystemAddr">
	/// @param a_SystemAddr: The system address of the player that has
	/// disconnected.
	/// </param>
	/// </summary>
	virtual void handlePlayerDisconnected(
		const RakNet::SystemAddress& a_SystemAddr) { return; }
};

