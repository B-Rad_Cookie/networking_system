/*
file:				Circle.cpp
author:				Brad Cook.
description:		This file contains the declaration of the Circle struct:
					A struct representing a circle geometry.
date last modified:	21/12/2016
*/

#include "Circle.h"

Circle::Circle()
{
	m_centre = glm::vec3(0);
	m_fRadius = 1.0f;
}

Circle::Circle(const glm::vec3 &a_Centre, float a_fRadius)
{
	m_centre = a_Centre;
	m_fRadius = a_fRadius;
}
