/*
file:				main.cpp
author:				Conan Burke, Brad Cook.
description:		This file is the entry point of the client-side application.
date last modified:	13/12/2016
*/

#include "BasicNetworkingApplication.h"

int main() {
	
	BasicNetworkingApplication* app = new BasicNetworkingApplication();
	app->run("ClientApplication", 1280, 720, false);
	delete app;

	return 0;
}