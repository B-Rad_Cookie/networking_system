/*
file:				ColDetect.cpp
author:				Brad Cook.
description:		This file contains the definition of the ColDetect class:
					The utility class which handles collision detection of
					geometry objects.
date last modified:	12/01/2017
*/

#include "ColDetect.h"
#include "Circle.h"
#include <glm\ext.hpp>

// Determines whether two circles are intersecting.
bool ColDetect::AreColliding(const Circle* a_p_Circle1,
	const Circle* a_p_Circle2)
{
	/*
	To determine whether 2 circles are intersecting.
	The way to determine whether circles intersect is to find the distance
	between the two centre points and see whether that distance is greater than
	the sum of radius. If the distance is less than the sum, they intersect.
	*/

	// Get the distance between the centres.
	float fCentreDistance = glm::distance(
		a_p_Circle2->m_centre, a_p_Circle1->m_centre);

	// Get the sums of the radii.
	float fRadiusSum = a_p_Circle1->m_fRadius + a_p_Circle2->m_fRadius;

	// Return the result.
	if (fCentreDistance <= fRadiusSum)
		return true;
	else
		return false;
}

bool ColDetect::AreColliding(const glm::vec3 &a_Point,
	const Circle* a_p_Circle)
{
	/*
	To determine whether a point is within a circle, find the distance between
	the point and the centre circle. Then see whether that distance is greater
	than the radius. If the distance is less than the radius, they intersect.
	*/

	// Get the distance between the point and centre.
	float distance = glm::distance(a_Point, a_p_Circle->m_centre);

	// Return the result.
	if (distance <= a_p_Circle->m_fRadius)
		return true;
	else
		return false;
}
