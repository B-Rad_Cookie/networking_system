/*
file:				Gameplay.h
author:				Brad Cook.
description:		This file contains the declaration of the Gameplay class:
					The main gameplay scene within the game.
date last modified:	15/01/2017
*/

#pragma once

#include "IScene.h"
#include <Enums.h>
#include <Font.h>
#include <vector>
#include "TimeManager.h"
#include <glm\glm.hpp>

struct Target;
struct Crosshair;

/// <summary>
/// When a player destroys a target, a message showing what player destroyed
/// the target (by the colour of the text) and the points they scored is going
/// to be displayed. This struct is used to contain all the information
/// required to do that.
/// </summary>
struct DisplayTargetDestroy
{
	/// <summary>
	/// The target that was destroyed
	/// </summary>
	unsigned int	m_uiTargetID;

	/// <summary>
	/// The player that destroyed the target.
	/// </summary>
	unsigned int	m_uiPlayerID;

	/// <summary>
	/// The points score that was scored  as a result of destroying the target.
	/// </summary>
	int				m_iPointsScored;

	/// <summary>
	/// Where the target was destroyed
	/// </summary>
	glm::vec3		m_Position;
};

/// <summary>
/// The scene used to control the main gameplay component of the game. This is
/// the scene containing the targets and handling the player input to destroy
/// the targets.  It also keeps track of the player's score.
/// </summary>
class Gameplay : public IScene
{
public:

	/// <summary>
	/// Default Constructor. Returns a new Gameplay instance. To set up the
	/// scene, call the init() function.
	/// <returns>
	/// returns Gameplay instance.
	/// </returns>
	/// </summary>
	Gameplay() {}

	/*-------------------------------------------------------------------------
							PUBLIC METHODS
	-------------------------------------------------------------------------*/

	/// <summary>
	/// Sets up all the elements required by the scene. This function is called
	/// when the game is first loaded.
	/// <param name="a_p_PeerInter">
	/// @param a_p_PeerInter: The RakNet::RakPeerInterface object in use for
	/// the server.
	/// </param>
	/// </summary>
	bool init(RakNet::RakPeerInterface* a_p_PeerInter);

	/// <summary>
	/// Deletes and frees up up all the elements required by the scene. This
	/// function is called when the game is last closed.
	/// </summary>
	void destroy();

	/// <summary>
	/// Updates all the relevant information (elements) in the
	/// scene per frame.
	/// <param name="dt">
	/// @param dt: Delta time, the time (in seconds) between frames executing.
	/// </param>
	/// <returns>
	/// Returns the name of the scene to change to.
	/// </returns>
	/// </summary>
	std::string update(double dt);

	/// <summary>
	/// The update function that is run when the gameplay status is in the
	/// Waiting gameplay state.
	/// <param name="dt">
	/// @param dt: Delta time, the time (in seconds) between frames executing.
	/// </param>
	/// <returns>
	/// Returns the name of the scene to change to.
	/// </returns>
	/// </summary>
	std::string updateWaiting(double dt);

	/// <summary>
	/// The update function that is run when the gameplay status is in the
	/// Setup gameplay state.
	/// <param name="dt">
	/// @param dt: Delta time, the time (in seconds) between frames executing.
	/// </param>
	/// <returns>
	/// Returns the name of the scene to change to.
	/// </returns>
	/// </summary>
	std::string updateSetup(double dt);

	/// <summary>
	/// The update function that is run when the gameplay status is in the
	/// Starting gameplay state.
	/// <param name="dt">
	/// @param dt: Delta time, the time (in seconds) between frames executing.
	/// </param>
	/// <returns>
	/// Returns the name of the scene to change to.
	/// </returns>
	/// </summary>
	std::string updateStarting(double dt);

	/// <summary>
	/// The update function that is run when the gameplay status is in the
	/// Playing gameplay state.
	/// <param name="dt">
	/// @param dt: Delta time, the time (in seconds) between frames executing.
	/// </param>
	/// <returns>
	/// Returns the name of the scene to change to.
	/// </returns>
	/// </summary>
	std::string updatePlaying(double dt);

	/// <summary>
	/// Draws the entities and information in the scene (i.e. sends the
	/// graphics to GPU).
	/// </summary>
	void draw();

	/// <summary>
	/// Sets up all the elements in the scene. This function is called everytime
	/// the scene is set as the active scene (i.e. When
	/// Application.setActiveScene is called). It is not called when the game is
	/// initially loaded.
	/// </summary>
	void activate();

	/// <summary>
	/// Cleans up all the elements in the scene. This function is called
	/// everytime the active scene is changed (i.e. When
	/// Application.setActiveScene is called). It is not called when the game is
	/// exited.
	/// </summary>
	void deactivate();

	/// <summary>
	/// Handles network messages. Receives the network messages and sends the
	/// information to the relevant function.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The data sent with a network message.
	/// </param>
	/// <returns>
	/// Resturns: the name of the scene to change to if any. Returns "" if
	/// no scene is required.
	/// </returns>
	/// </summary>
	std::string handleNetworkMessages(RakNet::Packet* a_p_Packet);

	//#########################################################################
	//								Entities


private:

	/*-------------------------------------------------------------------------
							PRIVATE MEMBER VARIABLES
	-------------------------------------------------------------------------*/

	/// <summary>
	/// The <see cref="aie::Font"/> object font used to display text on the
	/// screen.
	/// </summary>
	aie::Font*							m_p_DisplayFont;

	/// <summary>
	/// The targets that are currently in play.
	/// </summary>
	std::vector<Target*>				m_vTargets;

	/// <summary>
	/// The crosshairs of all the players. 
	/// </summary>
	std::vector<Crosshair*>				m_vCrosshairs;

	/// <summary>
	/// List of all targets destroyed display info.
	/// </summary>
	std::vector<DisplayTargetDestroy>	m_vDisplayInfo;

	/// <summary>
	/// The state of gameplay.
	/// </summary>
	GameplayStatus						m_eGameplayStatus;

	/// <summary>
	/// The current gameplay round number
	/// </summary>
	unsigned int						m_uiRoundNumber;

	/// <summary>
	/// The index of the crosshair that the client controls.
	/// </summary>
	int									m_iCrosshairIndex;

	/// <summary>
	/// The time (in seconds) between sending movement update information to
	/// the server.
	/// </summary>
	const float							m_fMOVEMENT_UPDATE_INTERVAL = 0.25f;

	/// <summary>
	/// Determines whether the game has ended or not.
	/// </summary>
	bool								m_bGameEnded;

	/// <summary>
	/// The <see cref="TimeManager"/> to manage <see cref="Timers"/>  used for
	/// time-based events.
	/// </summary>
	target::TimeManager*				m_p_TimeManager;

	/// <summary>
	/// Reference to the RakPeerInterface object;
	/// </summary>
	RakNet::RakPeerInterface*			m_p_PeerInter;

	/*-------------------------------------------------------------------------
								PRIVATE METHODS
	-------------------------------------------------------------------------*/

	/// <summary>
	/// Handles input and interaction from the player.
	/// <param name="dt">
	/// @param dt: delta time: the time in between frames.
	/// </param>
	/// </summary>
	void handleInput(double dt);

	/// <summary>
	/// Removes all the entities in the scene.
	/// </summary>
	void destroyEntities();

	/// <summary>
	/// Adds new target destroyed display info.
	/// <param name="a_uiTargetID">
	/// @param a_uiTargetID: The ID of the target that was destroyed.
	/// </param>
	/// <param name="a_uiPlayerID">
	/// @param a_uiPlayerID: The player who destroyed the target.
	/// </param>
	/// <param name="a_iPointsScored">
	/// @param a_iPointsScored: Points scored for destroying the target.
	/// </param>
	/// <param name="a_Position">
	/// @param a_Position: Position where the target was destroyed.
	/// </param>
	/// </summary>
	void addDisplayTargetDestroyedInfo(unsigned int a_uiTargetID,
		unsigned int a_uiPlayerID, int a_iPointsScored,
		const glm::vec3& a_Position);

	//#########################################################################
	//							Entities

	/// <summary>
	/// Updates the position off all the other player's crosshairs.
	/// <param name="dt">
	/// @param dt: delta time: the time in between frames.
	/// </param>
	/// </summary>
	void updateCrosshairs(double dt);

	//#########################################################################
	//						Network message handling

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//					Network Messages received from server

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_CLIENT_SETUP_GAME'. The message is sent from the server
	/// informing that the game is being set up.
	/// <remarks>
	/// The message is sent from ServerGameplayScene::sendGameSetup().
	/// </remarks>
	/// </summary>
	void handleGameSetup();

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_CLIENT_INITIAL_TARGETS'. The message is sent from the server
	/// sending all the initial targets at the start of gameplay.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The packet of data associated with the message.
	/// </param>
	/// <remarks>
	/// The message is sent from ServerGameplayScene::sendInitialTargets().
	/// </remarks>
	/// </summary>
	void handleInitialTargets(RakNet::Packet* a_p_Packet);

	/// <summary>
	/// Reads a <see cref="ServerTarget"/> message from a RakNet Bitstream and
	/// converts and saves the message as a <see cref="Target"/> object.
	/// <param name="a_bs>
	/// @param a_bs: The RakNet::Bitstream object to write the values to.
	/// </param>
	/// <remarks>
	/// It reads the Information sent from
	/// ServerGameplayScene::writeTargetToMessage()
	/// </remarks>
	/// </summary>
	void readTargetFromMessage(RakNet::BitStream& a_bs);

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_CLIENT_CROSSHAIRS'. The message is sent from the server
	/// sending all the crosshairs in the game.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The packet of data associated with the message.
	/// </param>
	/// <remarks>
	/// The message is sent from ServerGameplayScene::sendCrosshairs().
	/// </remarks>
	/// </summary>
	void handleCrosshairs(RakNet::Packet* a_p_Packet);

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_CLIENT_ROUND_START'. The message is sent from the server
	/// informing that a round of gameplay has started.
	/// <remarks>
	/// The message is sent from ServerGameplayScene::sendRoundStart().
	/// </remarks>
	/// </summary>
	void handleRoundStart();

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_CLIENT_UPDATE_CROSSHAIRS'. The message is sent from the server
	/// sending updates on movement info for all the crosshairs in the game.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The packet of data associated with the message.
	/// </param>
	/// <remarks>
	/// The message is sent from ServerGameplayScene::sendUpdateCrosshairs().
	/// </remarks>
	/// </summary>
	void handleUpdateCrosshairs(RakNet::Packet* a_p_Packet);

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_CLIENT_TARGET_DESTROYED'. The message is sent from the server
	/// informing of a target being destroyed.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The packet of data associated with the message.
	/// </param>
	/// <remarks>
	/// The message is sent from ServerGameplayScene::sendTargetDestroyed().
	/// </remarks>
	/// </summary>
	void handleTargetDestroyed(RakNet::Packet* a_p_Packet);

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_CLIENT_TARGET_REMOVED'. The message is sent from the server
	/// informing of a target being removed due to time running out.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The packet of data associated with the message.
	/// </param>
	/// <remarks>
	/// The message is sent from ServerGameplayScene::sendTargetRemoved().
	/// </remarks>
	/// </summary>
	void handleTargetRemoved(RakNet::Packet* a_p_Packet);

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_CLIENT_NEW_TARGETS'. The message is sent from the server
	/// informing of new targets being added.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The packet of data associated with the message.
	/// </param>
	/// <remarks>
	/// The message is sent from ServerGameplayScene::sendNewTargets().
	/// </remarks>
	/// </summary>
	void handleNewTargets(RakNet::Packet* a_p_Packet);

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_CLIENT_REMOVE_PLAYER'. The message is sent from the server
	/// informing of a player who has lef the game.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The packet of data associated with the message.
	/// </param>
	/// <remarks>
	/// The message is sent from ServerGameplayScene::sendRemovePlayer().
	/// </remarks>
	/// </summary>
	void handleRemovePlayer(RakNet::Packet* a_p_Packet);

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_CLIENT_END_ROUND'. The message is sent from the server
	/// informing that a round of gameplay has ended.
	/// <remarks>
	/// The message is sent from ServerGameplayScene::sendRoundEnd().
	/// </remarks>
	/// </summary>
	void handleRoundEnd();

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_CLIENT_RESTART_ROUND'. The message is sent from the server
	/// informing that a new round of gameplay will be played.
	/// <remarks>
	/// The message is sent from ServerGameplayScene::sendRoundRestart().
	/// </remarks>
	/// </summary>
	void handleRoundRestart();

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_CLIENT_END_GAME'. The message is sent from the server
	/// informing that the game has ended.
	/// <remarks>
	/// The message is sent from ServerGameplayScene::sendGameEnd().
	/// </remarks>
	/// </summary>
	void handleGameEnd();

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//					Network Messages sent to server

	/// <summary>
	/// Sends the ID_SERVER_READY_TO_START message to the server, which informs
	/// the server that this client is ready  to start a gameplay round.
	/// <remarks>
	/// This message is handled by Server::handleReadyToStart().
	/// </remarks>
	/// </summary>
	void sendReadyToStart();

	/// <summary>
	/// Sends the ID_SERVER_INITIAL_DATA_LOADED message to the server, which
	/// informs the server that this client has loaded the inital game data.
	/// <remarks>
	/// This message is handled by ServerGameplayScene::handledataLoaded().
	/// </remarks>
	/// </summary>
	void sendDataLoaded();

	/// <summary>
	/// Sends the ID_SERVER_MOVEMENT_INFO message to the server, which
	/// sends the client's crosshair movement info to the server.
	/// <remarks>
	/// This message is handled by ServerGameplayScene::handleMovementInfo().
	/// </remarks>
	/// </summary>
	void sendMovementInfo();

	/// <summary>
	/// Sends the ID_SERVER_CHECK_DESTROY_TARGET message to the server, which
	/// sends an inquiry to the server about whether or not they have
	/// destroyed a target.
	/// <param name="a_p_Target">
	/// @param a_p_Target: The target to check.
	/// </param>
	/// <remarks>
	/// This message is handled by
	/// ServerGameplayScene::handleCheckDestroyTarget().
	/// </remarks>
	/// </summary>
	void sendCheckDestroyTarget(Target* a_p_Target);

	/// <summary>
	/// Sends the ID_SERVER_AT_END_ROUND message to the server, which
	/// informs the server that this client is at the end of round stage.
	/// <remarks>
	/// This message is handled by ServerGameplayScene::handledataLoaded().
	/// </remarks>
	/// </summary>
	void sendAtEndRound();

	//#########################################################################
	//								Time events

	/// <summary>
	/// Removes a target destroyed display info message.
	/// <param name="a_iTargetID">
	/// @param a_iTargetID: The ID of the target.
	/// </param>
	/// </summary>
	void removeDisplayInfo(int a_iTargetID);

	/// <summary>
	/// A function that can be used to bind to when you don't want anything to
	/// happen when a <see cref="Timer"/> runs out <see cref="TimeManager"/>
	/// </summary>
	void dummy() { return; }
};
