/*
file:				Timer.h
author:				Brad Cook.
description:		This file contains the definition for the Timer class:
					which handles and event based on time elapsing.
date last modified:	15/01/2017
*/

#include "Timer.h"

target::Timer::Timer()
{
	m_p_VoidFuncBind = nullptr;
	m_p_IntFuncBind = nullptr;
	m_fTimeDuration = 0.0f;
	m_bToRepeat = false;
}

target::Timer::Timer(const std::function<void(void)>& a_FunctionBinding,
	float a_fCurrentTime, float a_fTimerDuration, bool a_bRepeat)
{
	m_fTimeDuration = a_fTimerDuration;
	m_bToRepeat = a_bRepeat;
	m_p_VoidFuncBind = a_FunctionBinding;

	// Set the void func(int) info to null.
	m_p_IntFuncBind = nullptr;	

	setTimer(a_fCurrentTime);
}

target::Timer::Timer(const std::function<void(int)>& a_FunctionBinding,
	int a_iArgumentValue,
	float a_fCurrentTime, float a_fTimerDuration, bool a_bRepeat)
{
	m_fTimeDuration = a_fTimerDuration;
	m_bToRepeat = a_bRepeat;
	m_p_IntFuncBind = a_FunctionBinding;
	m_iArgumentValue = a_iArgumentValue;

	// Set the void func() info to null.
	m_p_VoidFuncBind = nullptr;

	setTimer(a_fCurrentTime);
}

target::Timer::~Timer()
{
	// As there is no memory created in this class, don't do anything.
}

bool target::Timer::hasTimeElapsed(float a_fCurrentTime)
{
	return a_fCurrentTime >= m_fEndTime;
}

void target::Timer::invokeFunction()
{
	if (m_p_IntFuncBind == nullptr)  // call void func()
	{
		m_p_VoidFuncBind();
	}
	else  // call void func(int)
	{
		m_p_IntFuncBind(m_iArgumentValue);
	}
}

bool target::Timer::isRepeat()
{
	return m_bToRepeat;
}

void target::Timer::restart(float a_fCurrentTime)
{
	setTimer(a_fCurrentTime);
}

void target::Timer::setTimer(float a_fCurrentTime)
{
	// Calculate and set the end time.
	m_fEndTime = a_fCurrentTime + m_fTimeDuration;
}

