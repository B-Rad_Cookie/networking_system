/*
file:				TimeManager.h
author:				Brad Cook.
description:		This file contains the declaration for the class that
					handles timers in the game.
date last modified:	15/01/2017
*/

#pragma once

#include <map>
#include <string>
#include "Timer.h"

class IScene;

namespace target {

/// <summary>
/// Handles the management of time in an interactive application. It tracks the
/// passing of time in an application within an application's update loop. It
/// can create and handle multiple <see cref="Timer"/>s that are used to
/// trigger events based on time.
/// </summary>
class TimeManager
{
public:
	/// <summary>
	/// Default Constructor. Creates a new TimeManager.
	/// <returns>
	/// Returns: <see cref="TimeManager"/> object.
	/// </returns>
	/// </summary>
	TimeManager();

	/// <summary>
	/// Destructor. Performs necessary clean up functionality on destruction of
	/// an instance.
	/// </summary>
	~TimeManager();

	/*-------------------------------------------------------------------------
								PUBLIC METHODS
	-------------------------------------------------------------------------*/

	/// <summary>
	/// Reset the Time manager.
	/// </summary>
	void			reset();

	/// <summary>
	/// Runs the update logic. This function should be called within an
	/// update loop and the passed in value should be delta time.
	/// <param name="a_fDeltaTime">@param a_fDeltaTime: The time to add on.</param>
	/// </summary>
	void			update(double dt);

	/// <summary>
	/// void func() Constructor. Creates a new Timer instance with a
	/// function of the form "void function()".
	/// <param name="a_sTimerName">
	/// @param a_sTimerName: The name of the timer.
	/// </param>
	/// <param name="a_FunctionBinding">
	/// @param a_FunctionBinding: The std::function binding to call when the
	/// timer finishes. The function must be of the kind void func(). To create
	/// this, you will need to call the std::bind function. For more
	/// information on how to do this, see the examples section in the link 
	/// <see href="http://en.cppreference.com/w/cpp/utility/functional/function">
	/// HERE</see>
	/// </param>
	/// <param name="a_fTimerDuration">
	/// @param a_fTimerDuration: The time (in seconds) to elapse before the
	/// function is called.
	/// </param>
	/// <param name="a_bRepeat">
	/// @param a_bRepeat: Whether the timer should be repeated when time runs
	/// out.
	/// </param>
	/// <returns>
	/// Returns: <see cref="Timer"/> object.
	/// </returns>
	/// </summary>
	const Timer*	addTimer(std::string a_sTimerName,
		const std::function<void(void)>& a_FunctionBinding,
		float a_fTimerDuration, bool a_bRepeat);

	/// <summary>
	/// void func(int) Constructor. Creates a new Timer instance with a
	/// function of the form "void function(int)".
	/// <param name="a_sTimerName">
	/// @param a_sTimerName: The name of the timer.
	/// </param>
	/// <param name="a_FunctionBinding">
	/// @param a_FunctionBinding: The std::function binding to call when the
	/// timer finishes. The function must be of the kind void func(int). To create
	/// this, you will need to call the std::bind function. For more
	/// information on how to do this, see the examples section in the link 
	/// <see href="http://en.cppreference.com/w/cpp/utility/functional/function">
	/// HERE</see>
	/// </param>
	/// <param name="a_iArgumentValue">
	/// @param a_iArgumentValue: The value of the int argument to pass in when
	/// calling the function.
	/// </param>
	/// <param name="a_fTimerDuration">
	/// @param a_fTimerDuration: The time (in seconds) to elapse before the
	/// function is called.
	/// </param>
	/// <param name="a_bRepeat">
	/// @param a_bRepeat: Whether the timer should be repeated when time runs
	/// out.
	/// </param>
	/// <returns>
	/// Returns: <see cref="Timer"/> object.
	/// </returns>
	/// </summary>
	const Timer*	addTimer(std::string a_sTimerName,
		const std::function<void(int)>& a_FunctionBinding, int a_iArgumentValue,
		float a_fTimerDuration, bool a_bRepeat);

	/// <summary>
	/// Stops the timer of the given name.
	/// <param name="a_sTimerName">
	/// @param a_sTimerName: The timer to stop. The name passed in
	/// corresponds to the given when addTimer() was called.
	/// </param>
	/// <returns>
	/// Returns true if the timer was successfully stopped, returns false otherwise.
	/// </returns>
	/// <remarks>
	/// Note that stopping the timer actually removes it. So if you want to
	/// restart the timer at a later time, you will need to re-add it through
	/// addTimer().
	/// </remarks>
	/// </summary>
	bool			stopTimer(const std::string& a_sTimerName);

	/// <summary>
	/// Returns the amount of time remaining before the timer has finished.
	/// <param name="a_sTimerName">
	/// @param a_sTimerName: The timer to get the time remaining for. The name
	/// passed in corresponds to the given when addTimer() was called.
	/// </param>
	/// <returns>
	/// Returns amount of time remaining (in seconds). Returns -1 if the timer
	/// can not be found.
	/// </returns>
	/// </summary>
	float			getTimeRemaining(const std::string& a_sTimerName);

	/// <summary>
	/// Returns the amount of time remaining before the timer has finished in a
	/// print friendly format.
	/// <param name="a_sTimerName">
	/// @param a_sTimerName: The timer to get the time remaining for. The name
	/// passed in corresponds to the given when addTimer() was called.
	/// </param>
	/// <returns>
	/// Amount of time remaining as a std::string in the form of "HH:MM:SS"
	/// (if hours and minutes are not 0). Returns "" if the timer can not be
	/// found.
	/// </returns>
	/// </summary>
	std::string		getPrintTimeRemaining(const std::string& a_sTimerName);

	/// <summary>
	/// Removes all the timers.
	/// </summary>
	void			clearTimers();

private:

	/*-------------------------------------------------------------------------
							PRIVATE MEMBER VARIABLES
	-------------------------------------------------------------------------*/

	/// <summary>
	/// The current elapsed time.
	/// </summary>
	double									m_dElapsedTime;

	/// <summary>
	/// The collection of active <see cref="Timer"/>s. The first element is a
	/// std::string refereing to the name of the timer. The second element is
	/// the associated <see cref="Timer"/>.
	/// </summary>
	std::map<std::string, target::Timer*>	m_mTimers;
};

}  //namespace target
