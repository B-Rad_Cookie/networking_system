/*
file:				ClientInfo.cpp
author:				Brad Cook.
description:		This file contains the definition of the ClientInfo class:
					A class which contains all the information required by the
					client for the game.
date last modified:	07/01/2017
*/

#include "ClientInfo.h"

ClientInfo* ClientInfo::p_Singleton = nullptr;

ClientInfo::ClientInfo()
{
	m_uiClientID = 0;
	m_eConnectionStatus = ConnectionStatus::AttemptingConnection;
	m_p_2dRenderer = new aie::Renderer2D();
	m_PlayerColour = glm::vec4(1);
}

ClientInfo::~ClientInfo()
{
	delete m_p_2dRenderer;
}

void ClientInfo::create()
{
	if (p_Singleton == nullptr)
	{
		p_Singleton = new ClientInfo();
	}
}

void ClientInfo::destroy()
{
	if (p_Singleton != nullptr)
	{
		delete p_Singleton;
		p_Singleton = nullptr;
	}
}

void ClientInfo::setRenderer(aie::Renderer2D* a_p_Renderer)
{
	// Check if there's a renderer object already and delete it if there is.
	if (p_Singleton->m_p_2dRenderer != nullptr)
	{
		delete p_Singleton->m_p_2dRenderer;
		p_Singleton->m_p_2dRenderer = nullptr;
	}
	p_Singleton->m_p_2dRenderer = a_p_Renderer;
}
