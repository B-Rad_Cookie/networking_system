/*
file:				EndScene.h
author:				Brad Cook.
description:		This file contains the declaration of the EndScene class:
					The scene that is shown at the end of the game.
date last modified:	15/01/2017
*/

#pragma once

#include "IScene.h"
#include <Font.h>
#include <ServerEndScene.h>

#include <vector>

/// <summary>
/// The scene that is used at the end of the game. This scene is shown when the
/// main gameplay finishes. It shows the player's scores.
/// </summary>
class EndScene : public IScene
{
public:
	/// <summary>
	/// Default Constructor. Returns a new EndScene instance. To set up the
	/// scene, call the init() function.
	/// <returns>
	/// returns EndScene instance.
	/// </returns>
	/// </summary>
	EndScene() {}

	/*-------------------------------------------------------------------------
									PUBLIC METHODS
	-------------------------------------------------------------------------*/

	/// <summary>
	/// Sets up all the elements required by the scene. This function is called
	/// when the game is first loaded.
	/// <param name="a_p_PeerInter">
	/// @param a_p_PeerInter: The RakNet::RakPeerInterface object in use for
	/// the server.
	/// </param>
	/// </summary>
	bool init(RakNet::RakPeerInterface* a_p_PeerInter);

	/// <summary>
	/// Deletes and frees up up all the elements required by the scene. This
	/// function is called when the game is last closed.
	/// </summary>
	void destroy();

	/// <summary>
	/// Updates all the relevant information (elements) in the
	/// scene per frame.
	/// <param name="dt">
	/// @param dt: Delta time, the time (in seconds) between frames executing.
	/// </param>
	/// <returns>
	/// Returns the name of the scene to change to.
	/// </returns>
	/// </summary>
	std::string update(double dt);

	/// <summary>
	/// Draws the entities and information in the scene (i.e. sends the
	/// graphics to GPU).
	/// </summary>
	void draw();

	/// <summary>
	/// Sets up all the elements in the scene. This function is called everytime
	/// the scene is set as the active scene (i.e. When
	/// Application.setActiveScene is called). It is not called when the game is
	/// initially loaded.
	/// </summary>
	void activate();

	/// <summary>
	/// Cleans up all the elements in the scene. This function is called
	/// everytime the active scene is changed (i.e. When
	/// Application.setActiveScene is called). It is not called when the game is
	/// exited.
	/// </summary>
	void deactivate();

	/// <summary>
	/// Handles network messages. Receives the network messages and sends the
	/// information to the relevant function.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The data sent with a network message.
	/// </param>
	/// <returns>
	/// Resturns: the name of the scene to change to if any. Returns "" if
	/// no scene is required.
	/// </returns>
	/// </summary>
	std::string handleNetworkMessages(RakNet::Packet* a_p_Packet);

private:

	/*-------------------------------------------------------------------------
							PRIVATE MEMBER VARIABLES
	-------------------------------------------------------------------------*/

	/// <summary>
	/// The player scores for the game.
	/// </summary>
	std::vector<PlayerScores>		m_vPlayerScores;

	/// <summary>
	/// The <see cref="aie::Font"/> object font used to display text on the
	/// screen.
	/// </summary>
	aie::Font*						m_p_DisplayFont;

	/// <summary>
	/// Determines whether to start a new game.
	/// </summary>
	bool							m_bStartNewGame;

	/// <summary>
	/// Reference to the RakPeerInterface object;
	/// </summary>
	RakNet::RakPeerInterface*		m_p_PeerInter;

	/*-------------------------------------------------------------------------
								PRIVATE METHODS
	-------------------------------------------------------------------------*/

	//#########################################################################
	//						Network message handling

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//					Network Messages received from server

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_CLIENT_ALL_PLAYER_SCORES'. The message is sent from the server
	/// sending all player's scores.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The packet of data associated with the message.
	/// </param>
	/// <remarks>
	/// The message is sent from ServerEndScene::sendAllPlayerScores().
	/// </remarks>
	/// </summary>
	void handleAllPlayerScores(RakNet::Packet* a_p_Packet);

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_CLIENT_NEW_GAME'. The message is sent from the server
	/// informing a new game is going to begin.
	/// <remarks>
	/// The message is sent from ServerEndScene::sendNewGame().
	/// </remarks>
	/// </summary>
	void handleNewGame();

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//					Network Messages sent to server

	/// <summary>
	/// Sends the ID_SERVER_SEND_SCORE message, which informs sends the server
	/// the score the client got.
	/// <remarks>
	/// This message is handled in ServerEndScene::handlePlayerScore().
	/// </remarks>
	/// </summary>
	void sendPlayerScore();
};

/// <summary>
/// A functor class used to sort a vector of <see cref="PlayerScores"/>. It
/// sorts based on the player's score, then number. It will sort in descending
/// order
/// </summary>
class ScoreSorter
{
public:

	/// <summary>
	/// Constructor.
	/// </summary>
	ScoreSorter() {}

	/// <summary>
	/// The overload of the '()' function (to use as a functor).
	/// <param name="a_First">
	/// @param a_First: The first value to compare.
	/// </param>
	/// <param name="a_Second">
	/// @param a_Second: The first value to compare.
	/// </param>
	/// <returns>
	/// Returns true if the score of a_First is greater than a_Second, or if
	/// equal, returns true if the player number is greater.
	/// </returns>
	/// </summary>
	bool operator() (const PlayerScores& a_First,
		const PlayerScores& a_Second) const
	{
		// Check player scores.
		if (a_First.m_uiPlayerScore > a_Second.m_uiPlayerScore)
		{
			return true;
		}
		if (a_First.m_uiPlayerScore < a_Second.m_uiPlayerScore)
		{
			return false;
		}

		// Check player numbers.
		if (a_First.m_uiPlayerNumber > a_Second.m_uiPlayerNumber)
		{
			return true;
		}
		if (a_First.m_uiPlayerNumber < a_Second.m_uiPlayerNumber)
		{
			return false;
		}

		return false;
	}
};
