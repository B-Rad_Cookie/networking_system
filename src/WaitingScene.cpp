/*
file:				WaitingScene.cpp
author:				Brad Cook.
description:		This file contains the definition of the WaitingScene class:
					The scene where players are made to wait for the next game.
date last modified:	15/01/2017
*/

#include "WaitingScene.h"
#include "ClientInfo.h"
#include <CustomGameMessages.h>

#include <Renderer2D.h>

#include <RakPeerInterface.h>
#include <MessageIdentifiers.h>
#include <BitStream.h>


bool WaitingScene::init(RakNet::RakPeerInterface* a_p_PeerInter)
{
	// Setup the font.
	m_p_DisplayFont = new aie::Font("./font/consolas.ttf", 24);

	// Get the peer interface reference.
	m_p_PeerInter = a_p_PeerInter;

	return true;
}

void WaitingScene::destroy()
{
	// Remove the font.
	delete m_p_DisplayFont;
}

std::string WaitingScene::update(double dt)
{
	// If new game started, switch to start scene.
	if (m_bNewGameStarted == true)
	{
		return "StartScene";
	}

	return "";
}

void WaitingScene::draw()
{
	// Get the renderer and window widths and height.
	aie::Renderer2D* p_Renderer = ClientInfo::getRenderer();
	int iWidth = ClientInfo::getWindowWidth();
	int iHeight = ClientInfo::getWindowHeight();

	p_Renderer->drawText(m_p_DisplayFont,
		"Game currently in progress.",
		((float)iWidth / 2.0f) - 170.0f,
		((float)iHeight / 2.0f) + 50.0f);

	p_Renderer->drawText(m_p_DisplayFont,
		"Please wait till next game starts.",
		((float)iWidth / 2.0f) - 200.0f,
		((float)iHeight / 2.0f) - 50.0f);
}

void WaitingScene::activate()
{
	// Set new game started to false;
	m_bNewGameStarted = false;
}

void WaitingScene::deactivate()
{

}

std::string WaitingScene::handleNetworkMessages(RakNet::Packet* a_p_Packet)
{
	switch (a_p_Packet->data[0])
	{
	case ID_CLIENT_NEW_GAME:
	{
		// start new game
		handleNewGame();
		break;
	}
	}

	// There is no change of scene driven by a network message, so return ""
	return "";
}

void WaitingScene::handleNewGame()
{
	m_bNewGameStarted = true;
}
