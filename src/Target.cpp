/*
file:				Target.cpp
author:				Brad Cook.
description:		This file contains the definition of the Target
					struct: The representation of the targets to destroy in the
					game on the client side.
date last modified:	15/01/2017
*/

#include "Target.h"
#include "Circle.h"
#include <Consts.h>
#include <Renderer2D.h>
#include "ClientInfo.h"

aie::Texture* Target::s_p_RedTargetTexture = nullptr;
aie::Texture* Target::s_p_BlueTargetTexture = nullptr;
aie::Texture* Target::s_p_GreenTargetTexture = nullptr;

Target::Target() : NetworkObject()
{
	m_p_Circle = new Circle();
	m_eTargetType = TargetType::Blue;
	m_iScore = 0;
	m_p_Texture = nullptr;
}

Target::Target(unsigned int a_ID, TargetType a_eTargetType,
	const glm::vec3 &a_Centre) : NetworkObject(a_ID)
{
	// Set the information based on the target type.
	m_eTargetType = a_eTargetType;

	switch (m_eTargetType)
	{
	case TargetType::Blue:
	{
		m_p_Circle = new Circle(a_Centre, Consts::TYPE_TO_SIZE[m_eTargetType]);
		m_iScore = 10;
		m_p_Texture = Target::s_p_BlueTargetTexture;
		break;
	}
	case TargetType::Green:
	{
		m_p_Circle = new Circle(a_Centre, Consts::TYPE_TO_SIZE[m_eTargetType]);
		m_iScore = 20;
		m_p_Texture = Target::s_p_GreenTargetTexture;
		break;
	}
	case TargetType::Red:
	{
		m_p_Circle = new Circle(a_Centre, Consts::TYPE_TO_SIZE[m_eTargetType]);
		m_iScore = 40;
		m_p_Texture = Target::s_p_RedTargetTexture;
		break;
	}
	default:  // Return a blank Circle, similar to Target().
		m_p_Circle = new Circle();
		m_iScore = 0;
		m_p_Texture = nullptr;
		break;
	}
}

Target::~Target()
{
	// remove the circle.
	delete m_p_Circle;
	m_p_Circle = nullptr;
}

void Target::draw()
{
	aie::Renderer2D* p_Renderer = ClientInfo::getRenderer();
	p_Renderer->drawSprite(m_p_Texture, m_p_Circle->m_centre.x,
		m_p_Circle->m_centre.y);
}

void Target::setupTextures()
{
	s_p_RedTargetTexture = new aie::Texture("./images/redtarget.png");
	s_p_BlueTargetTexture = new aie::Texture("./images/bluetarget.png");
	s_p_GreenTargetTexture = new aie::Texture("./images/greentarget.png");
}

void Target::destroyTextures()
{
	delete s_p_RedTargetTexture;
	delete s_p_BlueTargetTexture;
	delete s_p_GreenTargetTexture;
}
