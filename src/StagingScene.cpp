/*
file:				StagingScene.cpp
author:				Brad Cook.
description:		This file contains the definition of the Staging class:
					The scene that is shown when the game is first opened.
date last modified:	18/01/2017
*/

#include "StagingScene.h"
#include "ClientInfo.h"
#include <CustomGameMessages.h>

#include <Renderer2D.h>

bool StagingScene::init(RakNet::RakPeerInterface* a_p_PeerInter)
{
	// Setup the font.
	m_p_DisplayFont = new aie::Font("./font/consolas.ttf", 24);

	// Get the peer interface reference.
	m_p_PeerInter = a_p_PeerInter;

	return true;
}

void StagingScene::destroy()
{
	// delete the font.
	delete m_p_DisplayFont;

	m_p_PeerInter = nullptr;
}

std::string StagingScene::update(double dt)
{
	

	return "";
}

void StagingScene::draw()
{
	// Get the renderer and window widths and height.
	aie::Renderer2D* p_Renderer = ClientInfo::getRenderer();
	int iWidth = ClientInfo::getWindowWidth();
	int iHeight = ClientInfo::getWindowHeight();

	p_Renderer->drawText(m_p_DisplayFont,
		"Welcome to Target Blast!",
		((float)iWidth / 2.0f) - 170.0f,
		((float)iHeight / 2.0f) + 150.0f);

	// Print information based on the connection status of the client.
	switch (ClientInfo::getConnectStatus())
	{
	case ConnectionStatus::AttemptingConnection:
	{
		// Print information that you are trying to connect.
		p_Renderer->drawText(m_p_DisplayFont,
			"Connecting to Server....",
			((float)iWidth / 2.0f) - 170.0f,
			((float)iHeight / 2.0f) + 50.0f);

		break;
	}
	case ConnectionStatus::ConnectionFailed:
	{
		// Print information that connection failed.
		p_Renderer->drawText(m_p_DisplayFont,
			"You have failed to connect to the Server",
			((float)iWidth / 2.0f) - 270.0f,
			((float)iHeight / 2.0f) + 50.0f);
		p_Renderer->drawText(m_p_DisplayFont,
			"Please close the game and try again",
			((float)iWidth / 2.0f) - 240.0f,
			((float)iHeight / 2.0f) - 50.0f);
		break;
	}
	case ConnectionStatus::ServerFull:
	{
		// Print information that connection failed.
		p_Renderer->drawText(m_p_DisplayFont,
			"Sorry, but the current game is full",
			((float)iWidth / 2.0f) - 245.0f,
			((float)iHeight / 2.0f) + 50.0f);
		p_Renderer->drawText(m_p_DisplayFont,
			"Please close the game and try again later",
			((float)iWidth / 2.0f) - 275.0f,
			((float)iHeight / 2.0f) - 50.0f);
		break;
	}
	default:
		break;
	}
}

void StagingScene::activate()
{
	
}

void StagingScene::deactivate()
{
}

std::string StagingScene::handleNetworkMessages(RakNet::Packet* a_p_Packet)
{
	// This scene doesn't handle any network messages, so return ""
	return "";
}
