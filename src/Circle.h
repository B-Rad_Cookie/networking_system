/*
file:				Circle.h
author:				Brad Cook.
description:		This file contains the declaration of the Circle struct:
					A struct representing a circle geometry.
date last modified:	21/12/2016
*/

#pragma once

#include <glm\glm.hpp>

struct Circle
{
	/// <summary>
	/// Default Constructor. Creates a new Circle object, where the centre of
	/// the circle is set to the origin (0, 0, 0), and the radius is 1 unit.
	/// <returns>
	/// A Circle instance.
	/// </returns>
	/// </summary>
	Circle();

	/// <summary>
	/// Constrcutor. Creates a new Circle object, where the centre of the
	/// circle is set to the a_Centre parameter, with a radius of the
	/// a_fRadius parameter.
	/// <param name="a_Centre">
	/// @param a_Centre: The centre of the circle.
	/// </param>
	/// <param name="a_fRadius">
	/// @param a_fRadius: The radius of the circle.
	/// </param>
	/// </summary>
	Circle(const glm::vec3 &a_Centre, float a_fRadius);

	/// <summary>
	/// Destructor. Performs any necessary clean actions.
	/// </summary>
	~Circle() {}

	/*-------------------------------------------------------------------------
							PUBLIC MEMBER VARIABLES
	-------------------------------------------------------------------------*/

	/// <summary>
	/// The centre point of the circle.
	/// </summary>
	glm::vec3 m_centre;

	/// <summary>
	/// The radius of the circle.
	/// </summary>
	float m_fRadius;
};
