/*
file:				BasicNetworkingApplication.h
author:				Conan Burke, Brad Cook.
description:		This file contains the declaration of the client
					application for the game. It handles the logic of the
					client-side of the game system.					
date last modified:	07/01/2017
*/

#pragma once

#include "Application.h"

#include <RakPeerInterface.h>
#include <MessageIdentifiers.h>
#include <BitStream.h>

#include <string>
#include <map>

namespace RakNet {
	class RakPeerInterface;
}
class Camera;
class IScene;

/// <summary>
/// The client-side	application for the game. It handles the logic of the
/// client-side of the game system. It also handles the messages received from
/// the server and sends messages to the server.
/// <remarks>
/// This class has been designed so that many of it's functions can be extended
/// or overloaded.
/// </remarks>
/// <remarks>
/// This class inherits from aie::Application.
/// </remarks>
/// </summary>
class BasicNetworkingApplication : public aie::Application {
public:

	/// <summary>
	/// Default Constructor.
	/// <returns>
	/// returns BaseNetworkingApplication instance.
	/// </returns>
	/// </summary>
	BasicNetworkingApplication();

	/// <summary>
	/// Destructor. Inheriting classes should implement this if there are 
	/// things to clean up.
	/// </summary>
	virtual ~BasicNetworkingApplication();

	/*-------------------------------------------------------------------------
								PUBLIC METHODS
	-------------------------------------------------------------------------*/

	/// <summary>
	/// Setups up the application. This function is called before the update
	/// and draw loops begin. 
	/// </summary>
	virtual bool startup();

	/// <summary>
	/// Tears down the application. This function is called after the update
	/// and draw loops have finished. It closes and safely destorys the OpenGL
	/// window.
	/// </summary>
	virtual void shutdown();

	/// <summary>
	/// Update function. This function is called once every frame.It handles
	/// the game logic required to be processed or checked every frame in the
	/// game.
	/// <param name="deltaTime">@param deltaTime: the time (in seconds) between
	/// frames.</param>
	/// </summary>
	virtual void update(double deltaTime);

	/// <summary>
	/// Draw function. This function is called once every frame. It
	/// draws the objects to destroy, the player's score, time remaining and
	/// the other player's mouses to the GUI.
	/// </summary>
	virtual void draw();

	/// <summary>
	/// Adds a new <see cref="IScene"/> to the collection of scenes used in the
	/// application. It also initialises the scene by calling its "init()"
	/// function.
	/// <param name="a_p_cName">
	/// @param a_p_cName: The name of the scene.
	/// </param>
	/// <param name="a_p_Scene">
	/// @param a_p_Scene: The <see cref="IScene"/> instance.
	/// </param>
	/// </summary>
	void addScene(const char* a_p_cName, IScene* a_p_Scene);

	/// <summary>
	/// Removes all the <see cref="IScene"/> scene objects stored. It also
	/// calls there "destroy()" function.
	/// </summary>
	void clearScenes();

	/// <summary>
	/// Changes the sctive scene to be the scene 'a_p_cSceneName'.
	/// 'a_p_cSceneName' should correspond to the name of a scene that was
	/// added through <see cref="Application.addScene(const char*, IScene*)/>.
	/// <param name="a_p_cSceneName">
	/// @param a_p_cSceneName: The name of the scene to set as active.
	/// </param>
	/// <returns>
	/// Returns true if the scene has been changed, returns false otherwise. 
	/// </returns>
	/// </summary>
	bool setActiveScene(const char* a_p_cSceneName);

protected:

	/*-------------------------------------------------------------------------
							PROTECTED MEMBER VARIABLES
	-------------------------------------------------------------------------*/

	/// <summary>
	/// The <see cref="IScene"/> scenes used on the application.
	/// <remarks>
	/// The first element of the std::map is a std::string representing the
	/// name of the scene. The second element of the std::map is the
	/// <see cref="IScene"/> object. 
	/// </remarks>
	/// </summary>
	std::map<std::string, IScene*> m_mScenes;

	/// <summary>
	/// The current active scene.
	/// </summary>
	IScene*							m_p_ActiveScene;

	/// <summary>
	/// Reference to the RakNet peer interfac used for messaging.
	/// </summary>
	RakNet::RakPeerInterface*		m_p_PeerInter;
	
	/// <summary>
	/// The port on which the server is going to run on.
	/// </summary>
	const unsigned short PORT = 5456;

	/*-------------------------------------------------------------------------
								PROTECTED METHODS
	-------------------------------------------------------------------------*/

	/// <summary>
	/// Creates and sets up the scenes used in the application.
	/// </summary>
	virtual void setupScenes();

	/// <summary>
	/// Quits the application.
	/// </summary>
	void quit();

	//#########################################################################
	//						Network message handling

	/// <summary>
	/// Handles network messages. Receives the network messages and sends the
	/// information to the relevant function.
	/// <returns>
	/// Resturns: the name of the scene to change to if any. Returns "" if
	/// no scene is required.
	/// </returns>
	/// </summary>
	std::string handleNetworkMessages();

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//		Network Messages handled at BaseNetworkingApplication level.

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_CONNECTION_REQUEST_ACCEPTED'. The message is sent from RakNet
	/// informing of a successful connection to the server.
	/// </summary>
	void handleConnectionAccepted();

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_CONNECTION_ATTEMPT_FAILED'. The message is sent from RakNet
	/// informing of a failed connection to the server.
	/// </summary>
	void handleConnectionFailed();

	/// <summary>
	/// Handles the logic for the received message
	/// 'ID_NO_FREE_INCOMING_CONNECTIONS'. The message is sent from RakNet
	/// informing of a failed connection to the server because it is full.
	/// </summary>
	void handleNoFreeConnections();

	/// <summary>
	/// Handles the ID_DISCONNECTION_NOTIFICATION message, which is the server
	/// shutting down.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The data sent with a network message.
	/// </param>
	/// </summary>
	void handleDisconnection(RakNet::Packet* a_p_Packet);

	/// <summary>
	/// Handles the ID_CONNECTION_LOST message, which is an unexpected error
	/// and shutdown of the server.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The data sent with a network message.
	/// </param>
	/// </summary>
	void handleConnectionLost(RakNet::Packet* a_p_Packet);

	/// <summary>
	/// Handles the logic in dealing with the server shutting down.
	/// </summary>
	void serverShutdown();

	/// <summary>
	/// Handles the logic for the received message 'ID_CLIENT_CONNECT_INFO'.
	/// The message involves receiving the client's ID and whether or not the
	/// client can join a game or has to wait.
	/// <param name="a_p_Packet">
	/// @param a_p_Packet: The packet of data associated with the message.
	/// </param>
	/// <remarks>
	/// This message is sent from Server::sendConnectionInfoToClient().
	/// </remarks>
	/// <returns>
	/// Returns: the name of the scene to change to if any. Returns "" if
	/// no scene is required.
	/// </returns>
	/// </summary>
	std::string handleClientConnectInfo(RakNet::Packet* a_p_Packet);
};
