/*
file:				Target.h
author:				Brad Cook.
description:		This file contains the declaration of the Target
					struct: The representation of the targets to destroy in the
					game on the client side.
date last modified:	15/01/2017
*/

#pragma once

#include <NetworkObject.h>
#include <ServerTarget.h>
#include <Texture.h>

struct Circle;

struct Target : public NetworkObject
{
public:

	/// <summary>
	/// Default constructor. Returns a new Target instance with the member
	/// variables set to 0 or null.
	/// <returns>
	/// returns Target instance.
	/// </returns>
	/// </summary>
	Target();

	/// <summary>
	/// Set constructor. Returns a new Target instance with the member
	/// variables set to the passed in argument values. The colour, score,
	/// radius and duration of the Target will also be set based on the
	/// TargetType.
	/// <param name="a_ID">
	/// @param a_ID: The Network ID for the object.
	/// </param>
	/// <param name="a_eTargetType">
	/// @param a_eTargetType: The type of target.
	/// </param>
	/// <param name="a_Centre">
	/// @param a_Centre: The centre point of the target.
	/// </param>
	/// <returns>
	/// returns Target instance.
	/// </returns>
	/// </summary>
	Target(unsigned int a_ID, TargetType a_eTargetType,
		const glm::vec3 &a_Centre);

	/// <summary>
	/// Destructor. Cleans up used memory.
	/// </summary>
	~Target();

	/*-------------------------------------------------------------------------
							PUBLIC MEMBER VARIABLES
	-------------------------------------------------------------------------*/

	/// <summary>
	/// The circle geometry for the target. This is used for collision detection
	/// and other purposes.
	/// </summary>
	Circle*			m_p_Circle;

	/// <summary>
	/// The type of target the target is.
	/// </summary>
	TargetType		m_eTargetType;

	/// <summary>
	/// The score the target is worth for destroying.
	/// </summary>
	int				m_iScore;

	/*-------------------------------------------------------------------------
								PUBLIC METHODS
	-------------------------------------------------------------------------*/
	/// <summary>
	/// Draws the target to the display (i.e. sends the graphics to GPU).
	/// </summary>
	void draw();
	 
	/// <summary>
	/// Sets up all the target textures  to be used for drawing.
	/// </summary>
	static void setupTextures();

	/// <summary>
	/// Removesall the target textures.
	/// </summary>
	static void destroyTextures();

protected:

	/*-------------------------------------------------------------------------
							PROTECTED MEMBER VARIABLES
	-------------------------------------------------------------------------*/

	/// <summary>
	/// The texture to draw the target with.
	/// </summary>
	aie::Texture*				m_p_Texture;

	/// <summary>
	/// The texture for the red target.
	/// </summary>
	static aie::Texture*		s_p_RedTargetTexture;

	/// <summary>
	/// The texture for the blue target.
	/// </summary>
	static aie::Texture*		s_p_BlueTargetTexture;

	/// <summary>
	/// The texture for the green target.
	/// </summary>
	static aie::Texture*		s_p_GreenTargetTexture;
};
